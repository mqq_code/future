{

 
  
  // 泛型接口
  type DeepArr<T> = T | DeepArr<T>[]
  // interface DeepArr<T> {
  //   [index: number]: T |  DeepArr<T>
  // }

  const arr: DeepArr<number>[] = [[1,[2,3],4],[5,[6,7],8,9]]
  const arr1: DeepArr<string>[] = [[['a'], ['b']],['c', 'd']]

  interface IPerson<T, U> {
    name: T;
    age: U;
    getName: () => T;
    setName: (n: T) => void
  }

  let obj: IPerson<number, string> = {
    name: 111,
    age: '10',
    setName(n: number) {
      this.name = n
    },
    getName(): number {
      return this.name
    },
  }

  // 泛型函数
  function flat<K>(arr: DeepArr<K>[]): K[] {
    return arr.reduce((prev: K[], val) => {
      return Array.isArray(val) ? prev.concat(flat(val)) :  prev.concat(val)
    }, [])
  }

  // flat<string>(arr1)
  // flat<number>(arr)


  // 泛型类
  class Person<T, U> {
    name: T;
    age: U;

    constructor (name: T, age: U) {
      this.name = name
      this.age = age
    }
  }

  let xm = new Person<string, number>('小明', 100)

  // 泛型约束
  // type Ia = { length: number }
  // T 必须有 length 属性
  // const logLen = <T extends Ia>(a: T): void => {
  //   console.log(a.length)
  // }
  // logLen({
  //   name: 'aaaa',
  //   length: 100
  // })

  // T 必须是 string 或者 number
  // const logLen = <T extends string | number>(a: T): void => {
  //   console.log(a)
  // }
  // logLen('1111')


}