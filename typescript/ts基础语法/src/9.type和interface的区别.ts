{
  // 类型别名和接口的区别
  // interface 是定义类型，type 是给现有的类型定义别名
  // 1. 接口重复定义会进行合并，type重名会报错
  // 2. 接口使用继承扩展类型，type使用交叉类型扩展
  // 3. interface 可以扩展全局数据类型


  // 1. 接口重复定义会进行合并，type重名会报错
/*
  interface IPerson {
    name: string;
    age: number;
    sex: '男' | '女'
  }
  interface IPerson {
    height: number;
    weight: number;
  }
  type IPerson = {
    name: string;
    age: number;
    sex: '男' | '女'
  }
  type IPerson = {
    height: number;
    weight: number;
  }

  let xm: IPerson = {
    name: '小明',
    age: 20,
    sex: '男',
    height: 180,
    weight: 180
  }
*/

  // 2. 接口使用继承扩展类型，type使用交叉类型扩展
  interface Base {
    name: string;
    age: number;
    sex: '男' | '女'
  }
  interface IDoctor extends Base {
    job: string;
    height: number;
    weight: number;
  }

  // type Base = {
  //   name: string;
  //   age: number;
  //   sex: '男' | '女'
  // }
  // type IDoctor = {
  //   job: string;
  //   height: number;
  //   weight: number;
  // }
  // 交叉类型
  // type newDoctor = Base & IDoctor
  // type newDoctor = Base & {
  //   job: string;
  //   height: number;
  //   weight: number;
  // }
  let xm: IDoctor = {
    name: '小明',
    age: 20,
    sex: '男',
    job: '外科医生',
    height: 180,
    weight: 180
  }








}