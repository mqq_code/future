{
  // 定义对象类型
  let person1: {
    name: string;
    age: number;
    sex: 0 | 1;
  } = {
    name: 'xiaoming',
    age: 100,
    sex: 0
  }



  // 定义类型别名
  type Iperson = {
    name: string;
    age: number;
    sex: 0 | 1;
  }

  // 定义对象类型
  let person: Iperson = {} as Iperson

  setTimeout(() => {
    person = {
      name: 'xiaoming',
      age: 100,
      sex: 0
    }
    console.log(getInfo(person))
  }, 1000)

  function getInfo (o: Iperson): string {
    const arr = ['男', '女']
    return `我的名字叫${o.name},今年${o.age}岁,性别：${arr[o.sex]}`
  }


  type IGoods = {
    title: string;
    price: number;
    count: number;
  }

  const data: IGoods[] = [
    {
      title: '苹果',
      price: 10,
      count: 1
    }
  ]

  
  // 使用接口定义对象类型
  interface Iobj {
    name: string;
    readonly age: number; // 只读属性
    hobby?: string[]; // 可选属性
    [prop: string]: any // 额外的属性
  }

  let obj: Iobj = {
    name: '111',
    age: 11,
    hobby: ['11111']
  }

  obj.a = 100
  obj[1] = 'asasss'


  interface IRouter {
    path: string;
    children?: IRouter[]
  }

  let router: IRouter[] = []

  router = [
    {
      path: '/home',
      children: [
        {
          path: '/home/movie',
          children: [
            {
              path: '/home/movie/hot'
            }
          ]
        },
        {
          path: '/home/cinema'
        }
      ]
    },
    {
      path: '/mine'
    }
  ]
  

}