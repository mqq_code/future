{
/*
  // 定义类
  class Person {
    // 实例属性
    name: string;
    age: number;
    // 原型方法
    getInfo() {
      console.log(`我叫${this.name},今年${this.age}岁`)
    }
    constructor(name: string, age: number) { // 构造函数
      console.log('实例化对象时立即执行此函数')
      this.name = name
      this.age = age
    }
  }
  // 实例化对象
  // let xm = new Person('晓明', 30)
  // console.log(xm.age)
  // xm.getInfo()


  // 继承
  // 1. 使用extends继承父类
  // 2. 使用super调用父类的构造函数
  class Doctor extends Person {
    job: string;
    constructor(name: string, age: number, job: string) {
      super(name, age) // 父类的构造函数
      this.job = job
    }
  }

  let xg = new Doctor('小刚', 40, '医生')
  console.log(xg)

*/

/*
  // 修饰符
  class Person {
    // readonly 只读属性
    public readonly name: string; // 公有属性，实例化对象，子类中都可以访问
    private age: number; // 私有属性，只能在此class内部使用，实例化对象和子类中不可以访问
    protected sex = '男' // 受保护的属性，在class和子类内部访问，实例化对象不可以访问
    public getInfo() {
      console.log(`我叫${this.name},今年${this.age}岁`)
    }
    constructor(name: string, age: number) {
      this.name = name
      this.age = age
    }
  }

  class Doctor extends Person {
    job: string;
    constructor(name: string, age: number, job: string) {
      super(name, age)
      this.job = job
    }
  }

  let xm = new Person('小明', 20)
  let xg = new Doctor('小刚', 40, '医生')

  // xm.name = '100'
  console.log(xm)
  console.log(xg)
*/




// 静态属性
/*
  class Person {
    // 实例属性和实例方法：实例化对象可以访问的属性和方法
    name: string;
    age: number;
    sex = '男'
    getInfo() {
      console.log(`我叫${this.name},今年${this.age}岁`)

    }
    constructor(name: string, age: number) {
      this.name = name
      this.age = age
    }

    // 定义静态属性和静态方法，类本身的方法，实例化对象不可以调用，通过类本身调用，例如：Person.type
    static type = '我是Person的静态属性'
    static isPerson(p: any) {
      return p instanceof Person
    }
  }
  let xm = new Person('小明', 20)
  console.log(xm)
*/


// 定义class接口
/*
interface IPerson {
  name: string;
  age: number;
  getInfo: () => string;
}
type Ip1 = {
  sex: '男' | '女';
  hobby: string[];
} 

// 约束class中必须实现接口中定义的属性和方法，可以使用多个接口同时约束 class
class Person implements IPerson,Ip1 {
  name: string;
  age: number;
  sex: Ip1['sex'];
  hobby: string[] = []
  setAge(age: number) {
    this.age = age
  }
  getInfo(): string {
    return `我叫${this.name},今年${this.age}岁`
  }
  constructor(name: string, age: number, hobby: string[], sex: Ip1['sex']) {
    this.name = name
    this.age = age
    this.hobby = hobby
    this.sex = sex
  }
}
let xm = new Person('小明', 20, ['吃饭'], '男')
*/

// 抽象类
// 1. 无法被实例化，只能被子类继承
// 2. 抽象类用来规范子类的属性和方法类型，子类必须实现父类中的抽象属性和方法
// 3. 抽象类也可以定义具体的属性和方法被子类继承
// 4. class可以同时实现多个接口，但是只能继承一个父类

interface IPerson {
  name: string;
  age: number;
  getInfo: () => string;
}
type Ip1 = {
  sex: '男' | '女';
  hobby: string[];
} 

abstract class Person  {
  // 抽象属性
  abstract name: string
  abstract age: number
  // 抽象方法
  abstract getAge(): number
  abstract setAge(age: number): void

  sex: Ip1['sex'] = '女'
  hobby: string[] = ['唱歌']
  getInfo (): string {
    return `我叫${this.name},今年${this.age}岁` 
  }
}

class Doctor extends Person implements IPerson,Ip1 {
  name: string
  age: number
  getAge () {
    return this.age
  }
  setAge(age: number): void {
    this.age = age
  }
  constructor (name: string, age: number) {
    super()
    this.name = name
    this.age = age
  }
}
let xm = new Doctor('小明', 30)
console.log(xm)



// function Swiper() {
//   this.a = '111'
// }

// // 静态方法
// Swiper.isSwiper = function () {
//   console.log(111111111);
// }
// // 调用静态方法
// Swiper.isSwiper()


// let s = new Swiper()
// console.log(s)


}