{


// 定义函数
function sum(a: number, b: number): number {
  return a + b
}

const sum1 = (a: number, b: number): number => {
  return a + b
}

const a: number = 100

const sum2: (msg: string) => string = function (msg) {
  return msg
}

let obj = {
  sum (a: number, b: number): number {
    return a + b
  }
}


// 可选参数和默认参数
const request = (url: string, method = 'get', data?: string) => {
  let xhr = new XMLHttpRequest()
  xhr.open(method, url)
  xhr.send(data)
}
// request('xxx.com/api')

// 剩余参数
const sum3 = (...r: number[]): number => {
  return r.reduce((pr, v) => pr + v)
}
console.log(sum3(1, 2, 3, 4, 5, 6))


// 当一个函数参数或者返回值有多种类型时可以使用函数重载定义类型
// function fn(n: number, b: string): string
// function fn(n: string, b: number): number
// function fn(n: number | string, b: string | number) {
//   if (typeof n === 'number' && typeof b === 'string') {
//     return n.toFixed(b.length)
//   } else if (typeof n === 'string' && typeof b === 'number') {
//     return n.length + b
//   }
// }

// let fnr1 = fn('100', 100)
// let fnr2 = fn('100')

function fn1(a: number, n: number): string
function fn1(a: any[]): number
function fn1 (a: number | any[], n?: number): string | number {
  if (typeof a === 'number') {
    return a.toFixed(n)
  } else {
    return a.length
  }
}

let r1 = fn1(100, 2)
let r2 = fn1([100, 2])



}