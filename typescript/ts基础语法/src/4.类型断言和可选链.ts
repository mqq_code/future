{

  // 类型断言, 开发者比ts更确定此变量的类型时使用
  let title = document.querySelector('h1') as HTMLHeadingElement
  // let title = <HTMLHeadingElement>document.querySelector('h1')

  // 非空断言
  // let title = document.querySelector('h1')!


  // 可选链，title为真往后执行，为假返回 undefined
  console.log(title?.style.color)
  // 可选链不可以做赋值操作
  // title?.style.color = 'red'

  // ! 非空断言，告诉ts此变量不是null和undefined
  // title!.style.color = 'red'
  title.style.color = 'red'


  let k: unknown
  title.addEventListener('click' , () => {
    k = title.innerHTML
    // 使用类型断言给k确定类型
    console.log((k as string).includes('aaa'))
    console.log((<string>k).includes('aaa'))
  })


  let btn: HTMLButtonElement = document.querySelector('button')!
  let box = document.querySelector('.box') as HTMLDivElement
  box.addEventListener('click', (e) => {
    console.log((e.target as HTMLDivElement).innerHTML)
  })
  
  let inp = document.querySelector('.inp') as HTMLInputElement

  inp.addEventListener('input', e => {
    console.log((e.target as HTMLInputElement).value);
    
  })

}