{
  // 条件类型
  // type IRes<T> = T extends string ? number : string
  // function fn<T extends string | number>(a: T): IRes<T> {
  //   if (typeof a === 'string') {
  //     return a.length as IRes<T>
  //   } else {
  //     return a + '' as IRes<T>
  //   }
  // }
  // let aa = fn(100)

  // ts 中常用的内置泛型
  // Partial
  // Required
  // Readonly
  // Pick
  // Omit
  // Record
  // Exclude
  // Extract
  // ReturnType

  interface IObj {
    name: string;
    age: number;
    sex: 0 | 1;
    hobby: string[];
    desc?: string;
  }

  let obj: IObj = {
    name: '小明',
    age: 30,
    sex: 0,
    hobby: ['吃饭']
  }

  type PartialObj = Partial<IObj> // 把 IObj 中的所有属性都变成可选属性
  type RequiredObj = Required<IObj> // 把 IObj 中的所有属性都变成必传属性
  type ReadonlyObj = Readonly<IObj> // 把 IObj 中的所有属性都变成只读属性

  type PickObj = Pick<IObj, 'name' | 'age' | 'desc'> // 从 IObj 中挑选几个属性组成一个新的类型
  type OmitObj = Omit<IObj, 'name' | 'age'> // 从 IObj 中排除几个不需要的属性组成一个新的类型

  type RecordObj1 = Record<'z' | 'y' | 'x', string> // 返回一个 z y x 组成的对象类型，所有属性都是 string
  type RecordObj2 = Record<keyof IObj, string> // 把 IObj 中的所有属性都改成 string

  type ExcludeObj = Exclude<'a' | 'd' | 'c', 'a' | 'b' | 'c'> // 返回第一个参数中不被第二个参数包含的类型，没找到返回never
  type ExtractObj = Extract<'a' | 'd' | 'c', 'a' | 'b' | 'c'> // 返回第一个参数中被第二个参数包含的类型，没找到返回never


  const sum = (a: number, b: number) => {
    return a + b
  }

  type IRes = ReturnType<typeof sum> // 返回函数返回值的类型


}