{

  const getVal = <T, K extends keyof T>(obj: T, k: K): T[K] => {
    return obj[k]
  }
  
  let a = 'AAAa'
  const sum = (a: number, b: number): number =>  {
    return a + b
  }

  let obj1 = {
    aa: 100,
    bb: 200,
    cc: 'CCC'
  }

  // 获取js变量的类型
  type IObj1 = typeof obj1

  // 获取对象类型中的所有key名，组成文字联合类型
  // type IKey = keyof typeof obj1
  type IKey = keyof IObj1 // 'aa' | 'bb' | 'cc'

  console.log(getVal(obj1, 'bb'))

  enum Sex {
    other = 0,
    male = 1,
    female = 2
  }

  type ISex = keyof typeof Sex

  function getSex(sex: keyof typeof Sex) {
  }
  getSex('male')












}