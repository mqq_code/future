{
// 类型推论，ts可以根据值反推变量的类型
let n: number = 100


function fn1(a: number, n: number): string
function fn1(a: any[]): number
function fn1 (a: number | any[], n?: number): string | number {
  if (typeof a === 'number') {
    return a.toFixed(n)
  } else {
    return a.length
  }
}

// 根据结果推断变量的类型
let r1: string = fn1(100, 2)
let r2 = fn1([100, 2])


// ts可以推论出函数返回值类型
const sum = (a: number, b: number) => {
  return a + b
}
const r3 = sum(1, 2)




}