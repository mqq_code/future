{
  // type： 定义类型别名
  
  // 定义联合类型
  type ITest = number[] | string

  function fn(p: ITest) {
    // 联合类型的变量只能使用联合类型中所有类型共有的属性和方法
    if (typeof p === 'string') {
       // 类型保护，确定此作域中变量的具体类型
      return p.split('')
    } else {
      return p.join('')
    }
  }
  
  fn([10])

  // 定义交叉类型, 合并多个类型
  type ITest1 = { name: string } & { age: number }
  let o: ITest1 = {
    name: 'aaa',
    age: 30
  }

  // 合并基础类型返回never
  type test = number & string


  // 定义二维数组
  type DeepArr = any | DeepArr[]

  let arr: DeepArr[] = [
    [[1, [2, 3]], [4, 5]],
    [4, [[5], 6]], 6, 8
  ]


  // 数组扁平化
  // function flat(array: DeepArr[]) {
  //   return array.reduce((prev, val) => {
  //     return Array.isArray(val) ? [...prev, ...flat(val)]  : [...prev, val]
  //   }, [])
  // }

  function flat(array: DeepArr[]) {
    while (array.some(v => Array.isArray(v))) {
      console.log(array)
      array = [].concat(...array)
    }
    return array
  }
  console.log(flat(arr))
}