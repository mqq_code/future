{
  
  const box = document.querySelector('.box') as HTMLDivElement
  let pos = {
    x: 0,
    y: 0
  }
  // 定义枚举，使用方便理解的单词描述一组数字或者字符串的含义，方便理解代码含义和后期维护
  enum Direction {
    left = 65,
    top = 87,
    right = 68,
    down = 83,
  }

  document.addEventListener('keydown', (e) => {
    console.log(e.keyCode)
    if (e.keyCode === Direction.left) {
      pos.x -= 10
    } else if (e.keyCode === Direction.top) {
      pos.y -= 10
    } else if (e.keyCode === Direction.right) {
      pos.x += 10
    } else if (e.keyCode === Direction.down) {
      pos.y += 10
    }
    box.style.transform = `translate(${pos.x}px, ${pos.y}px)`
  })



  enum Sex {
    other = 0,
    female = 1,
    male = 2
  }
  function getSex(sex: Sex) {
    if (sex === Sex.other) {
      return '未知'
    } else if (sex === Sex.female) {
      return '女生'
    } else {
      return '男生'
    }
  }
  console.log(getSex(1))

}

// 重绘 回流（重排）

// const a = Symbol(1)
// const b = Symbol(1)
// const c = 'name'
// const d = 'name'
// const obj = {
// }

// obj[a] = 'AAAAAA'
// // obj[b] = 'BBBBBB'
// // obj[c] = 'cccc'
// obj.name = 'dddd'

// console.log(obj)

