{


  // 使用接口定义对象类型
  interface Iobj {
    name: string;
    readonly age: number; // 只读属性
    hobby?: string[]; // 可选属性
    [prop: string]: any // 额外的属性
  }

  let obj: Iobj = {
    name: '111',
    age: 11,
    hobby: ['11111']
  }

  obj.a = 100
  obj[1] = 'asasss'


  console.log(obj)

  // 使用接口定义函数类型
  interface SearchFunc {
    (source: string, subString: string): boolean;
  }
  const fn: SearchFunc = (source, subString): boolean => {
    return true
  }
  fn('a', 'b')

  // 接口定义可索引的类型
  interface StringArray {
    [index: number]: string;
  }
  let arr: StringArray = ['a', 'a', '']


  // 接口继承
  interface IPerson {
    name: string;
    age: number;
    sex: '男' | '女'
  }

  let xm: IPerson = {
    name: '小明',
    age: 20,
    sex: '男'
  }

  // 继承接口
  interface IDoctor extends IPerson {
    job: string;
    address: string;
    hobby: string[];
  }
  // 接口重名会合并
  interface IDoctor {
    height: number;
    weight: number;
  }

  let xg: IDoctor = {
    name: '小刚',
    age: 20,
    sex: '男',
    job: '外科医生',
    address: '天坛医院',
    hobby: ['打羽毛球'],
    height: 180,
    weight: 180
  }



}