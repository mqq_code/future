{
  // 联合类型
  let a: number | string | any[] = '10'

  function fn(p: number[] | string) {
    // 联合类型的变量只能使用联合类型中所有类型共有的属性和方法
    if (typeof p === 'string') {
       // 类型保护，确定此作域中变量的具体类型
      return p.split('')
    } else {
      return p.join('')
    }
  }
  
  fn([10])


  // 文字类型
  let sex: '男' | '女' | '未知'
  sex = '女'

  function setSex(s: '男' | '女' | '未知') {
    sex = s
  }
  setSex('未知')



  function tip(type: 'success' | 'warn' | 'error' ) {
    if (type === 'success') {
      alert("绿色的框")
    } else if (type === 'warn') {
      alert("黄色的框")
    } else {
      alert("红色的框")
    }
  }


  tip('error')


}