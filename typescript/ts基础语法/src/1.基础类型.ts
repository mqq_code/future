// ts 基础类型：number、boolean、string、undefined、null、symbol、array、object、function、enum、any、unknown、never、void
{


// 添加类型注解
let n: number = 100
const flag: boolean = true
const s: string = 'abc'
const sy: symbol = Symbol(1)

// undefined和null类型的值只有他们自身
const un: undefined = undefined
const nu: null = null 

// 定义数组
const arr: number[] = [1, 2, 3, 4, 5, 6, 7]
const arr1: Array<string> = ['a', 'b', 'c', 'd']
// 元组：定义已知元素数量和类型的数组
const arr2: [string, number] = ['a', 100]

// 定义任意类型，相当于放弃了类型检查，可以赋值给任意类型的变量，尽量少用
let an: any = 100
an = 'str'
an = true
an = [100, 'a', 'c']
let an1: any[] = [1,2,3,4,'aaa', true]

// let str1: string
// str1 = an
// str1.toLocaleLowerCase()


// void: 没有值，在函数没有返回值时使用
const log = (a: any): void => {
  console.log(a)
}

// never: 不可能存在的值, 永远不会出现的值
// never类型是那些总是会抛出异常或根本就不会有返回值的函数表达式或箭头函数表达式的返回值类型
// function error(message: string): never {
//   throw new Error(message)
// }

// unknown: 暂时不确定类型，可以赋值为任何类型，但是不可以把 unknown 类型赋值给其他类型的变量
// 使用场景: 变量暂时不确定类型，但是也不想放弃校验类型时使用
let k: unknown
k = 100
k = 'st'
k = true

function toStr(a: unknown): string {
  if (typeof a === 'string') {
    // 缩小 unknown 类型的范围 
    return a
  }
  return String(a)
}

// 变量只能赋值为引用类型
let obj: object
obj = {}
obj = [1, 2, 3, 45]
obj = function () {}

// 变量不可以赋值为 null 和 undefined
let obj1: Object
obj1 = 1
obj1 = 'str'
let obj2: {}
obj2 = 1
obj2 = 'str'


}