import request from "./request";

// 轮播图返回值类型
export interface IBannerItem {
  imageUrl: string;
}
interface BannerRes {
  code: number;
  banners: IBannerItem[];
}

// 获取轮播图
export const getBanner = () => {
  return request.get<{}, BannerRes>('https://zyxcl.xyz/banner').then(res => {
    return {
      code: 200,
      banners: [{"imageUrl":"https://img12.360buyimg.com/pop/s1180x940_jfs/t1/223740/37/20796/100454/640984efF82ca5892/4bbd818c9992476e.jpg.avif"},{"imageUrl":"https://imgcps.jd.com/img-cubic/creative_server_cia_jdcloud/v2/2000366/100011222593/FocusFullshop/CkNqZnMvdDEvMTY1MTI5LzE5LzM0Mjk1LzIzMjg3LzYzZmJiMGRhRjA0YzYzZGQwLzBhYjhkZWZjMGY1NDFhYWEucG5nEgkzLXR5XzBfNTQwAjjui3pCGwoX5ouH5oyH55m95bCPVOeUt-Wjq1TmgaQQAUITCg_niannvo7ku7fmm7TkvJgQAkIQCgznq4vljbPmiqLotK0QBkIKCgbnsr7pgIkQB1jBzIjJ9AI/cr/s/q.jpg"},{"imageUrl":"https://img10.360buyimg.com/pop/s1180x940_jfs/t1/157458/34/30095/39713/640aa8b1Fde735915/60e2d6593364ddda.jpg.avif"},{"imageUrl":"https://imgcps.jd.com/ling4/100012043978/5Lqs6YCJ5aW96LSn/5L2g5YC85b6X5oul5pyJ/p-640dbabb61fd0b75722ca74c/57dd7308/cr/s/q.jpg"},{"imageUrl":"https://img12.360buyimg.com/pop/s1180x940_jfs/t1/177484/29/8372/69340/60c06387E40d7008f/5568d4689c3a33eb.jpg.avif"},{"imageUrl":"https://imgcps.jd.com/ling4/100032149194/5Lqs6YCJ5aW96LSn/5L2g5YC85b6X5oul5pyJ/p-5bd8253082acdd181d02fa3b/3101f6b4/cr/s/q.jpg"},{"imageUrl":"https://imgcps.jd.com/ling4/100038004391/5Lqs6YCJ5aW96LSn/5L2g5YC85b6X5oul5pyJ/p-640dbabb61fd0b75722ca74c/c0b3fc9b/cr/s/q.jpg"},{"imageUrl":"https://imgcps.jd.com/ling4/100023239577/5Lqs6YCJ5aW96LSn/5L2g5YC85b6X5oul5pyJ/p-5bd8253082acdd181d02fa33/d7daf177/cr/s/q.jpg"}]
    }
  })
}


// 搜索请求参数类型
interface ISearchParams {
  keywords: string
}
// 搜索返回值类型
interface ISearchRes {
  code: number;
  result: {
    songCount: number;
    songs: {
      id: number;
      name: string;
    }[]
  }
}

export const search = (keywords: string) => {
  return request.get<ISearchParams, ISearchRes>('https://zyxcl.xyz/search', {
    keywords
  })
}

// request<{}, BannerRes>({
//   url: 'https://zyxcl.xyz/banner',
//   method: 'POST'
// }).then(res => {
//   console.log(res.banners)
// })

// request<ISearchParams>({
//   url: 'https://zyxcl.xyz/search',
//   method: 'POST',
//   data: {
//     keywords: '刘得华'
//   }
// }).then(res => {
//   console.log(res)
// })


