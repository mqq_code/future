
interface IPrams {
  [k: string]: any
}

interface RequestParams<T extends IPrams> {
  url: string;
  method?: 'GET' | 'POST';
  data?: T
}
// 格式化get请求的参数，把对象转成字符串拼接
function formatQuery (params: IPrams) {
  let res = ''
  Object.keys(params).forEach(key => {
    res += `&${key}=${params[key]}`
  })
  if (res) {
    res = '?' + res.slice(1)
  }
  return res
}

function request<IData extends IPrams, IResponse>({
  url,
  method = 'GET',
  data = {} as IData
}: RequestParams<IData>): Promise<IResponse> {
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest()
    if (method === 'GET') {
      xhr.open(method, url + formatQuery(data))
      xhr.send()
    } else {
      xhr.open(method, url)
      xhr.setRequestHeader('content-type', 'application/json;charset=utf-8')
      xhr.send(JSON.stringify(data))
    }
    xhr.onreadystatechange = () => {
      if (xhr.readyState === 4) {
        if (xhr.status === 200 || xhr.status === 304) {
          const data = JSON.parse(xhr.responseText)
          resolve(data)
        } else {
          reject(xhr.statusText)
        }
      }
    }
    
  })
}

request.get = function<T extends IPrams, IResponse> (url: string, data = {} as T) {
  return request<T, IResponse>({ url, data })
}
request.post = function<T extends IPrams, IResponse> (url: string, data = {} as T) {
  return request<T, IResponse>({ url, data, method: 'POST' })
}

export default request