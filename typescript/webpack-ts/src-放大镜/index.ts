import './scss/style.scss'
import { getBanner, IBannerItem } from './services'
import { $ } from './utils'

const list = $('.list') as HTMLUListElement // ul
const img = $('.img img') as HTMLImageElement // 当前展示图片
const bigBox = $('.big') as HTMLDivElement
const bigImg = $('.big img') as HTMLImageElement // 大图
const mark = $('.mark') as HTMLDivElement // 蒙层
let banners: IBannerItem[] = [] // 所有图片列表
let curIndex = 0 // 当前选中的下标
let liIndex = 0 // 当前展示最左侧图片的下标

// 遍历li绑定事件
const addClickEvent = () => {
  const li = $.gets('li', list) as HTMLLIElement[]
  li.forEach((item, index) => {
    item.addEventListener('click', () => {
      curIndex = index
      img.src = banners[curIndex].imageUrl
      bigImg.src = banners[curIndex].imageUrl
      $('.active')?.classList.remove('active')
      $('img', item)?.classList.add('active')
    })
  })
} 
// 事件委托绑定事件
list.addEventListener('click', (e) => {
  const target = e.target as HTMLElement
  if (target.nodeName === 'IMG') {
    const index = Number(target.getAttribute('data-index'))
    curIndex = index
    img.src = banners[curIndex].imageUrl
    bigImg.src = banners[curIndex].imageUrl
    $('.active')?.classList.remove('active')
    target.classList.add('active')
  }
})

getBanner().then(res => {
  banners = res.banners
  img.src = banners[curIndex].imageUrl
  bigImg.src = banners[curIndex].imageUrl
  // 计算ul的宽度
  list.style.width = banners.length * 100 + 'px'
  list.innerHTML = banners.map((item, index) => {
    return `<li><img class="${curIndex === index ? 'active' : ''}" data-index="${index}" src="${item.imageUrl}" /></li>`
  }).join('')
  // 给li绑定点击事件
  // addClickEvent()
})

// 滚动
$('.prev')?.addEventListener('click', () => {
  if (liIndex === 0) return
  liIndex--
  list.style.transform = `translateX(${liIndex * -100}px)`
})
$('.next')?.addEventListener('click', () => {
  if (liIndex === banners.length - 4) return
  liIndex++
  list.style.transform = `translateX(${liIndex * -100}px)`
})

// 放大镜
$('.img')?.addEventListener('mouseenter', () => {
  mark.classList.add('show')
  bigBox.classList.add('show')
})
$('.img')?.addEventListener('mouseleave', () => {
  mark.classList.remove('show')
  bigBox.classList.remove('show')
})
const { top, left, width, height } = $('.img')!.getBoundingClientRect()
const scale = 500 / 150 // 蒙层和右侧宽度的比例
bigImg.style.width = scale * width  + 'px'
$('.img')?.addEventListener('mousemove', (e) => {
  const { pageX, pageY } = e as MouseEvent
  let x = pageX - left - 75 
  let y = pageY - top - 75
  if (x <= 0) x = 0
  if (y <= 0) y = 0
  if (x >= width - 150) x = width - 150
  if (y >= height - 150) y = height - 150
  mark.style.cssText = `top: ${y}px; left: ${x}px`
  
  bigImg.style.left = `${-x * scale}px`
  bigImg.style.top = `${-y * scale}px`
})