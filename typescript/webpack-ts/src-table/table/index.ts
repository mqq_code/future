import './table.scss'

export interface IColumns<T> {
  title: string;
  key?: keyof T;
  render?: (record?: T, index?: number) => string | number
}

interface IOptions<T> {
  el: string | HTMLElement; // 渲染表格的位置
  columns: IColumns<T>[];
  dataSource: T[]
}

class Table<T> {
  private el: IOptions<T>['el']
  private thead: HTMLTableSectionElement
  private tbody: HTMLTableSectionElement
  private columns: IOptions<T>['columns']
  private dataSource: IOptions<T>['dataSource']

  constructor({ el, columns, dataSource }: IOptions<T>) {
    this.columns = columns
    this.dataSource = dataSource
    this.el = typeof el === 'string' ? document.querySelector(el) as HTMLElement : el
    this.el.innerHTML = `<table border="1"><thead><tr></tr></thead><tbody></tbody></table>`
    this.thead = this.el.querySelector('thead tr')!
    this.tbody = this.el.querySelector('tbody')!
    this.renderHead()
    this.renderBody()
  }

  private renderHead() {
    this.thead.innerHTML = this.columns.map(item => {
      return `<th>${item.title}</th>`
    }).join('')
  }
  private renderBody() {
    this.tbody.innerHTML = this.dataSource.map((item, index) => {
      return `<tr>${this.columns.map(val => `<td>${val.render ? val.render(item, index) : item[val.key!]}</td>`).join('')}</tr>`
    }).join('')
  }
  update(dataSource: IOptions<T>['dataSource']) {
    this.dataSource = dataSource
    this.renderBody()
  }
}

export default Table