import './index.scss'

interface ILayout {
  total: string;
  sizes: string;
  prev: string;
  pager: string;
  next: string;
  jumper: string;
}

interface IOptions {
  el: string | HTMLElement;
  total: number;
  pagesize: number;
  pagenum: number;
  pagesizes?: number[];
  layout: (keyof ILayout)[];
  onChange?: (pagesize: number, pagenum: number) => void;
}

class Pagination {
  el: HTMLElement
  total: IOptions['total']
  pagenum: IOptions['pagenum']
  pagesize: IOptions['pagesize']
  layout: IOptions['layout']
  pagesizes: number[]
  totalpage: number
  btns!: HTMLDivElement
  prev!: HTMLButtonElement
  next!: HTMLButtonElement
  select!: HTMLSelectElement
  totalEl!: HTMLElement
  jumpEl!: HTMLInputElement
  onChange: IOptions['onChange']
  constructor({ el, total, pagenum, pagesize, pagesizes, layout, onChange }: IOptions) {
    this.el = typeof el === 'string' ? document.querySelector(el) as HTMLElement : el
    this.total = total
    this.layout = layout
    this.pagenum = pagenum
    this.pagesize = pagesize
    this.pagesizes = pagesizes || [10, 20, 50, 100]
    this.totalpage = Math.ceil(this.total / this.pagesize)
    this.onChange = onChange
    this.init()
    this.renderBtns()
    this.renderSelect()
    this.addEvent()
  }
  layoutEnum: ILayout = {
    total: '<div>共 <b class="total"></b> 条</div>',
    sizes: '<select></select>',
    prev: '<button class="prev">上一页</button>',
    pager: '<div class="btns"></div>',
    next: '<button class="next">下一页</button>',
    jumper: '<div class="go">跳至 <input type="number" style="width: 60px" /> 页</div>'
  }
  init() {
    this.el.classList.add('pagination-wrap')
    this.el.innerHTML = this.layout.map(key => this.layoutEnum[key]).join('')
    this.btns = this.el.querySelector('.btns')!
    this.prev = this.el.querySelector('.prev')!
    this.next = this.el.querySelector('.next')!
    this.select = this.el.querySelector('select')!
    this.totalEl = this.el.querySelector('.total')!
    this.jumpEl = this.el.querySelector('.go input')!
  }
  renderBtns() {
    let btnstr = ''
    for (let i = 1; i <= this.totalpage; i ++) {
      btnstr += `<button class="${this.pagenum === i ? 'active' : ''}" data-num="${i}">${i}</button>`
    }
    this.btns.innerHTML = btnstr
  }
  renderSelect() {
    this.select.innerHTML = this.pagesizes.map(val => `<option value="${val}">每页 ${val} 条</option>`).join('')
    this.select.value = String(this.pagesize)
  }
  change() {
    this.btns.querySelector('.active')?.classList.remove('active')
    this.btns.children[this.pagenum - 1].classList.add('active')
    this.onChange && this.onChange(this.pagesize, this.pagenum)
  }
  addEvent() {
    this.prev.addEventListener('click', () => {
      if (this.pagenum > 1) {
        this.pagenum--
        this.change()
      }
    })
    this.next.addEventListener('click', () => {
      if (this.pagenum < this.totalpage) {
        this.pagenum++
        this.change()
      }
    })
    this.btns.addEventListener('click', (e) => {
      if ((e.target as HTMLElement).nodeName === 'BUTTON') {
        const num = (e.target as HTMLButtonElement).getAttribute('data-num')
        this.pagenum = Number(num)
        this.change()
      }
    })
    this.select.addEventListener('change', () => {
      this.pagesize = Number(this.select.value)
      this.pagenum = 1
      this.onChange && this.onChange(this.pagesize, this.pagenum)
    })
    this.jumpEl.addEventListener('keydown', e => {
      if (e.keyCode === 13) {
        let pagenum = Number((e.target as HTMLInputElement).value)
        if (pagenum < 1) pagenum = 1
        if (pagenum > this.totalpage) pagenum = this.totalpage
        this.pagenum = pagenum
        ;(e.target as HTMLInputElement).value = String(pagenum)
        this.change()
      }
    })
  }
  setTotal(total: number) {
    this.total = total
    this.totalpage = Math.ceil(this.total / this.pagesize)
    this.totalEl.innerHTML = String(total)
    this.renderBtns()
  }
}
export default Pagination