import './scss/style.scss'
import Table, { IColumns } from './table'
import axios from 'axios'
import Pagination from './pagination'

interface ISongsItem {
  name: string;
  ar: {
    id: number;
    name: string;
  }[],
  al: {
    id: number;
    name: string;
  }
}
interface ISongsRes {
  code: number;
  songs: ISongsItem[];
  total: number;
}

let list: ISongsItem[] = []
let total = 0
let pagenum = 1
let pagesize = 20
let offset = (pagenum - 1) * pagesize

const columns: IColumns<ISongsItem>[] = [
  {
    title: '序号',
    render: (record, index) => index! + 1
  },
  {
    title: '歌曲',
    key: 'name'
  },
  {
    title: '歌手',
    key: 'ar',
    render: record => {
      return record!.ar.map(item => item.name).join('/')
    }
  },
  {
    title: '专辑',
    key: 'al',
    render: record => record!.al.name
  }
]

const table = new Table({
  el: document.querySelector('.songs') as HTMLDivElement,
  columns,
  dataSource: list
})


let pagination = new Pagination({
  el: '.pagination',
  total,
  pagesize,
  pagenum,
  pagesizes: [10, 20, 30, 50],
  layout: ['pager', 'prev', 'next', 'total', 'sizes', 'jumper'],
  onChange: (size, num) => {
    pagenum = num
    pagesize = size
    getList()
  }
})

const getList = async () => {
  offset = (pagenum - 1) * pagesize
  const res = await axios.get<ISongsRes>('https://zyxcl.xyz/artist/songs', {
    params: {
      id: 6452,
      offset,
      limit: pagesize
    }
  })
  list = res.data.songs
  total = res.data.total
  // 更新总数
  pagination.setTotal(total)
  // 更新table数据
  table.update(list)
}
getList()










/*

interface IData {
  name: string;
  age: number;
  sex: number;
}

const dataSource: IData[] = [
  {
    name: '王小明',
    age: 20,
    sex: 1
  },
  {
    name: '老王',
    age: 30,
    sex: 0
  },
  {
    name: '小李',
    age: 25,
    sex: 1
  }
]
const columns: IColumns<IData>[] = [
  {
    title: '姓名',
    key: 'name'
  },
  {
    title: '年龄',
    key: 'age'
  },
  {
    title: '性别',
    key: 'sex',
    render: (record) => {
      return record?.sex === 1 ? '男' : '女'
    }
  },
  {
    title: '操作',
    render: (record) => {
      return '<button>删除</button> <button>编辑</button>'
    }
  }
]

new Table<IData>({
  el: document.querySelector('.table') as HTMLDivElement,
  columns,
  dataSource
})

*/