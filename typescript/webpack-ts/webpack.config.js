const path = require('path')
const HtmlWebpackPlugin   = require('html-webpack-plugin')

module.exports = {
  mode: 'development',
  entry: './src/index.ts',
  output: {
    path: path.join(__dirname, 'build'),
    filename: 'index.js'
  },
  devServer: {
    open: true,
    hot: true
  },
  resolve: {
    // 引入文件可以忽略的后缀名，使用ts必须添加此配置
    extensions: ['.ts', '.js'],
  },
  module: {
    // 配置loader
    rules: [
      {
        test: /\.(css|scss|sass)$/i,
        use: ['style-loader', 'css-loader', 'sass-loader']
      },
      {
        test: /\.(png|svg|jpg|jpeg|gif)$/i,
        type: 'asset',
      },
      {
        test: /\.(js|ts)$/,
        exclude: /node_modules/,
        use: ['babel-loader', 'ts-loader']
      }
    ]
  },
  // 配置插件
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/index.html',
      filename: 'index.html'
    })
  ]
}