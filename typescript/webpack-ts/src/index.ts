import './scss/style.scss'
import Swiper from './swiper'

const btns = [...document.querySelectorAll('.btns button')]
const swiper = new Swiper({
  el: '.swiper',
  pagination: '.pagination',
  prev: '.prev',
  next: '.next',
  curIndex: 0,
  // autoplay: {
  //   speed: 600,
  //   delay: 2000
  // },
  onChange: curIndex => {
    document.querySelector('.btns .active')?.classList.remove('active')
    btns[curIndex].classList.add('active')
  }
})

btns[0].classList.add('active')
btns.forEach((button, index) => {
  button.addEventListener('click', () => {
    document.querySelector('.btns .active')?.classList.remove('active')
    btns[index].classList.add('active')
    swiper.slideTo(index)
  })
})
