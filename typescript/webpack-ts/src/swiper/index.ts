import './index.scss'

interface IAutoPlay {
  speed: number;
  delay: number;
}
interface IOptions {
  el: string | HTMLElement;
  pagination?: string | HTMLElement;
  prev?: string | HTMLElement;
  next?: string | HTMLElement;
  curIndex?: number;
  autoplay?: boolean | IAutoPlay;
  onChange?: (curIndex: number) => void;
}

class Swiper {
  private el: HTMLElement
  private pagination?: HTMLElement
  private prev?: HTMLElement
  private next?: HTMLElement
  private container: HTMLElement
  private autoplay: IAutoPlay = {
    speed: 500,
    delay: 1000
  }
  curIndex: number
  timer?: NodeJS.Timer
  onChange: IOptions['onChange']
  slideLen: number
  constructor ({ el, pagination, prev, next, curIndex, autoplay, onChange }: IOptions) {
    this.el = typeof el === 'string' ? document.querySelector(el)! : el
    this.container = this.el.querySelector('.swiper-container')!
    this.pagination = typeof pagination === 'string' ? document.querySelector(pagination)! : pagination
    this.prev = typeof prev === 'string' ? document.querySelector(prev)! : prev
    this.next = typeof next === 'string' ? document.querySelector(next)! : next
    this.curIndex = curIndex || 0 // 默认展示图片下标
    this.onChange = onChange
    // 存轮播原本的数量
    this.slideLen = this.container.children.length

    this.init()
    // 判断是否添加自动轮播
    this.addAutoPlay(autoplay)
  }
  private init () {
    // 渲染分页器
    this.pagination && this.renderPagination()
    // 绑定事件
    this.addEvent()

    // 添加衔接滑动
    this.container.appendChild(this.container.children[0].cloneNode(true))
    this.container.insertBefore(this.container.children[this.slideLen - 1].cloneNode(true), this.container.children[0])
    this.curIndex += 1

    // 动态设置元素的宽度
    this.container.style.width = (this.container.children.length * 100) + '%'
    // 设置默认展示的图片
    this.container.style.transform = `translateX(${-this.el.offsetWidth * this.curIndex}px)`
  }
  private startAutoPlay () {
    this.timer = setInterval(() => {
      this.change(this.curIndex + 1)
    }, this.autoplay.delay)
  }
  private addAutoPlay (autoplay: IOptions['autoplay']) {
    if (autoplay) {
      if (typeof autoplay !== 'boolean') {
        this.autoplay = autoplay
      }
      this.startAutoPlay()
      this.el.addEventListener('mouseenter', () => {
        if (this.timer) {
          clearInterval(this.timer)
        }
      })
      this.el.addEventListener('mouseleave', () => {
        this.startAutoPlay()
      })
    }
  }
  private renderPagination () {
    let str = ''
    for (let i = 0; i < this.slideLen; i ++) {
      str += `<span class="${this.curIndex === i ? 'active' : ''}"></span>`
    }
    this.pagination!.innerHTML = str
  }
  // 衔接滑动
  private change (index: number) {
    this.curIndex = index
    this.container.style.transform = `translateX(${-this.el.offsetWidth * this.curIndex}px)`
    this.container.style.transition = `transform ${this.autoplay.speed}ms linear`
    if (this.curIndex > this.slideLen) {
      this.curIndex = 1
      setTimeout(() => {
        this.container.style.transition = `none`
        this.container.style.transform = `translateX(${-this.el.offsetWidth * this.curIndex}px)`
      }, this.autoplay.speed)
    }
    if (this.curIndex === 0) {
      this.curIndex = this.slideLen
      setTimeout(() => {
        this.container.style.transition = `none`
        this.container.style.transform = `translateX(${-this.el.offsetWidth * this.curIndex}px)`
      }, this.autoplay.speed)
    }
    this.pagination?.querySelector('.active')?.classList.remove('active')
    this.pagination?.children[this.curIndex - 1].classList.add('active')
    this.onChange && this.onChange(this.curIndex - 1)
  }
  private addEvent () {
    this.prev?.addEventListener('click', () => {
      this.change(this.curIndex - 1)
    })
    this.next?.addEventListener('click', () => {
      this.change(this.curIndex + 1)
    })
    this.pagination && [...this.pagination.children].forEach((span, index) => {
      span.addEventListener('click', () => {
        this.change(index + 1)
      })
    })
  }
  slideTo (index: number) {
    this.change(index + 1)
  }
}

export default Swiper