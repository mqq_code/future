export const sum: (a: number, b: number) => number
export const utilName: string

export interface BBB {
  age: number
}

// .d.ts 中如果使用了 export 和 import 此声明文件就是局部声明文件，需要引入才能生效
// 无法直接定义全局变量，想要定义全局变量需要在 declare global 中定义
declare global {
  // 定义全局类型
  declare interface Window {
    _UTIL_: string
  }
  declare var aa: number
}
