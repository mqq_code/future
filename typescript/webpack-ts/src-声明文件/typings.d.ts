
// 定义全局变量类型
declare const _VERSION_: string 
// declare var aa: number
declare let token: boolean
declare function logColor(msg: string, color: string): void
declare interface Window {
  tip(msg: string): void
}

interface AAA {
  name: string
}

// 定义模块类型
declare module '*.svg'
declare module '*.png'
// declare module 'mockjs' {
//   const mockAA: string
//   function fn(): string
// }