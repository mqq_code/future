// .ts文件：可以编写类型和逻辑
// .d.ts文件：类型声明文件，只能定义类型

// 自带声明文件
import axios from 'axios'
// 下载第三方插件如果没有声明文件，尝试 `npm i --save-dev @types/包名` 下载声明文件，如果没有就自己定义声明文件
import Mock from 'mockjs'
// 引入自定义的js文件，需要自定义声明文件
import { sum, utilName, BBB } from './utils/util'
import './scss/style.scss'
import { formart } from './utils/format'
import logo from './imgs/logo.svg'
import img from './imgs/img.png'

// let imgbox = new Image()
// imgbox.src = img
// document.body.appendChild(imgbox)

// 使用全局声明文件中定义的接口
let aaa: AAA = {
  name: '100'
}
// 使用模块中定义的接口
let bbb: BBB = {
  age: 100
}


console.log(window._UTIL_)

let s = sum(1, 2)
console.log(utilName)

interface IObj {
  name: string;
  age: number;
}
let obj: IObj = {
  name: 'aa',
  age: 100
}


window.tip('111')
console.log(_VERSION_)
console.log(aa)
console.log(token)
logColor(_VERSION_, 'red')


// .ts文件：可以编写类型和逻辑
// .d.ts文件：类型声明文件，只能定义类型
// 第三方插件自带声明文件直接用，例如：axios
// 第三方插件没有声明文件，先尝试 `npm i --save-dev @types/包名` 下载声明文件，如果没有就使用 declare module 自己定义声明文件
// ts 中引入自定义的js模块，需要自定义声明文件，在该js同级目录定义同名的.d.ts文件声明类型
// .d.ts 中没有export和import就是全局的，有任意一个就是局部声明文件
// 定义全局变量
//    1. 在全局的 .d.ts 中使用 declare const、declare let ...... 直接声明全局变量类型
//    2. 在局部的 .d.ts 中需要在 declare global 中声明全局变量类型