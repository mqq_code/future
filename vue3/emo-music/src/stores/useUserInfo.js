import { defineStore } from 'pinia'
import { ref } from 'vue'
import { loginStatusApi } from '../service/login'
import { userDetailApi } from '../service/user'
import { logoutApi } from '@/service/login'


const useUserInfoStore = defineStore('userInfo', () => {
  // 显示登陆框
  const showLogin = ref(false)
  // 个人信息
  const userinfo = ref(null)
  // 获取用户信息接口
  const getUserInfo = async (id) => {
    const info = await userDetailApi(id)
    userinfo.value = info.data
  }
  // 查询登陆状态
  const getLoginStatus = async () => {
    try {
      const res = await loginStatusApi()
      // 如果已经登陆，调用个人信息接口
      if (res.data.data.profile) {
        getUserInfo(res.data.data.account.id)
      }
    } catch (e) {
      console.log(e)
    }
  }
  // 退出登陆
  const logout = async () => {
    await logoutApi()
    userinfo.value = null
  }

  return {
    showLogin,
    userinfo,
    getLoginStatus,
    logout,
    getUserInfo
  }
})

export default useUserInfoStore
