import { defineStore } from 'pinia'
import { ref, reactive, computed } from 'vue'
import { getSongDetailApi, getSongUrlApi } from '../service/playlist'

const usePlayer = defineStore('player', () => {
  const curIndex = ref(0) // 当前播放的下标
  const songs = reactive([]) // 歌曲详情
  const curSong = computed(() => {
    return songs[curIndex.value]
  })
  const addSongs = async (id) => { // 添加歌曲
    const index = songs.findIndex(v => v.id === id)
    if (index > -1) {
      curIndex.value = index
    } else {
      const res = await getSongDetailApi(id) // 获取歌曲详情
      const urlRes = await getSongUrlApi(id) // 获取歌曲url
      songs.push({
        ...res.data.songs[0],
        url: urlRes.data.data[0].url
      })
      curIndex.value = songs.length - 1
    }
  }
  return {
    songs,
    curIndex,
    curSong,
    addSongs
  }
})

export default usePlayer
