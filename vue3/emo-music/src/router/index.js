import { createRouter, createWebHistory } from 'vue-router'
import My from '../views/my/My.vue'
import Friend from '../views/friend/Friend.vue'
import Download from '../views/download/Download.vue'
import Home from '../views/discover/home/Home.vue'
import Toplist from '../views/discover/toplist/Toplist.vue'
import Playlist from '../views/discover/playlist/Playlist.vue'
import Djradio from '../views/discover/djradio/Djradio.vue'
import Artist from '../views/discover/artist/Artist.vue'
import Album from '../views/discover/album/Album.vue'
import PlaylistDetail from '../views/discover/playlistDetail/PlaylistDetail.vue'


const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/discover',
      name: 'discover',
      component: Home
    },
    {
      path: '/discover/toplist',
      name: 'toplist',
      component: Toplist
    },
    {
      path: '/discover/playlistDetail/:id',
      name: 'playlistDetail',
      component: PlaylistDetail
    },
    {
      path: '/discover/playlist',
      name: 'playlist',
      component: Playlist
    },
    {
      path: '/discover/djradio',
      name: 'djradio',
      component: Djradio
    },
    {
      path: '/discover/artist',
      name: 'artist',
      component: Artist
    },
    {
      path: '/discover/album',
      name: 'album',
      component: Album
    },
    {
      path: '/my',
      name: 'my',
      component: My
    },
    {
      path: '/friend',
      name: 'friend',
      component: Friend
    },
    {
      path: '/download',
      name: 'download',
      component: Download
    },
    {
      path: '/',
      redirect: '/discover'
    }
  ]
})

export default router
