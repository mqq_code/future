import request from './request'

// 获取用户详情
export const userDetailApi = (uid) => {
  return request.get('/user/detail', { params: { uid } })
}