import request from './request'

// 获取歌单详情
export const playlistDetailApi = (id) => {
  return request.get('/playlist/detail', { params: { id } })
}
// 获取歌单评论
export const commentPlaylistApi = (id) => {
  return request.get('/comment/playlist', { params: { id } })
}
// 获取音乐播放地址
export const getSongUrlApi = (id) => {
  return request.get('/song/url', { params: { id } })
}
// 获取音乐详情
export const getSongDetailApi = (ids) => {
  return request.get('/song/detail', { params: { ids } })
}