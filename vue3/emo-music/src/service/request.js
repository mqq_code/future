import axios from 'axios'

// 生产环境域名：https://zyxcl-music-api.vercel.app
// 开发环境域名：http://121.89.213.194:3000/

// 创建新的axios实例对象
const instance = axios.create({
  baseURL: '/mqq'
})

export default instance
