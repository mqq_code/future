import request from './request'

// 登陆状态
export const loginStatusApi = () => {
  return request.get('/login/status', { params: { now: Date.now() } })
}
// 二维码 key 生成接口
export const getQrKeyApi = () => {
  return request.get('/login/qr/key', { params: { now: Date.now() } })
}
// 二维码 key 生成接口
export const getQrApi = (key = '') => {
  return request.get('/login/qr/create', { params: { key, now: Date.now() } })
}
// 查询二维码状态是否可用
export const qrCheckApi = (key = '') => {
  return request.get('/login/qr/check', { params: { key, now: Date.now() } })
}

// 退出登陆
export const logoutApi = () => {
  return request.get('/logout')
}
