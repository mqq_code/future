import request from './request'

// 首页轮播图
export const bannerApi = () => {
  return request.get('/banner')
}
// 推荐歌单
export const personalizedApi = (limit = 10) => {
  return request.get('/personalized', {
    params: {
      limit
    }
  })
}
// 新碟上架
export const newAlbumApi = (limit = 10, offset = 0) => {
  return request.get('/top/album', {
    params: {
      limit,
      offset
    }
  })
}
// 歌单详情
export const playlistDetailApi = (id) => {
  return request.get('/playlist/detail', { params: { id } })
}

// 排行榜
export const toplistApi = (id) => {
  return request.get('/toplist')
}
