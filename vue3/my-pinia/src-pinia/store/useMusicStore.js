import { defineStore } from 'pinia'
import { ref, reactive, computed } from 'vue'
import useInfoStore from './useInfoStore'
import axios from 'axios'

// 创建仓库
const useMusicStore = defineStore('music', () => {
  // 使用其他仓库数据
  const user = useInfoStore()

  const count = ref(0)
  const total = computed(() => {
    return `总价：¥${count.value * 10}`
  })
  const add = (n) => {
    count.value += n
    user.changeAge(n)
  }

  const list = ref([])
  const getList = async () => {
    const res = await axios.get('https://zyxcl-music-api.vercel.app/playlist/hot')
    list.value = res.data.tags
  }

  return { count, total, list, getList, add }
})

export default useMusicStore
