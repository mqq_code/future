import { defineStore } from 'pinia'

// 创建仓库
const useInfoStore = defineStore('user', {
  state: () => ({
    name: '王小明',
    age: 20
  }),
  getters: {
    fullInfo: state => {
      return `我叫${state.name},今年${state.age}岁`
    }
  },
  actions: {
    changeAge (n) {
      this.age += n
    }
  }
})

export default useInfoStore
