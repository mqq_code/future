import { defineStore } from 'pinia'
import { ref } from 'vue'

const getResult = (arr) => {
  const [a, b, c] = arr
  if (a === b & a === c) {
    return '豹子'
  }
  const total = a + b + c
  if (total >= 10) return '大'
  return '小'
}

const useDice = defineStore('dice', () => {
  const balance = ref(10000)
  const diceArr = ref([1, 2, 3])
  const chipMoney = ref(0)
  let loading = false

  const changeDice = () => {
    return new Promise((resolve) => {
      loading = true
      const timer = setInterval(() => {
        diceArr.value = diceArr.value.map(() => {
          return Math.floor(Math.random() * 6) + 1
        })
      }, 60)
      setTimeout(() => {
        clearInterval(timer)
        loading = false
        resolve(diceArr.value)
      }, 3000)
    })
  }
  const start = async (type) => {
    if (loading) return
    const res = await changeDice()
    if (getResult(res) === type.title) {
      console.log('赢了', chipMoney.value * type.scale)
      balance.value += chipMoney.value * type.scale
    } else {
      console.log('输了', chipMoney.value)
      balance.value -= chipMoney.value
    }
    chipMoney.value = 0
  }
  const addChip = (n) => {
    if (chipMoney.value + n > balance.value) {
      chipMoney.value = balance.value
    } else {
      chipMoney.value += n
    }
  }

  return {
    balance,
    chipMoney,
    diceArr,
    start,
    addChip
  }
})

export default useDice
