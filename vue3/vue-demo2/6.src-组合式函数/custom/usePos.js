import { ref, reactive, onMounted, onBeforeUnmount } from 'vue'

const usePos = () => {
  const pos = reactive({ x: 0, y: 0 })

  const mousemove = (e) => {
    pos.x = e.pageX
    pos.y = e.pageY
  }
  onMounted(() => {
    document.addEventListener('mousemove', mousemove)
  })
  
  onBeforeUnmount(() => {
    document.removeEventListener('mousemove', mousemove)
  })

  return pos
}

export default usePos
