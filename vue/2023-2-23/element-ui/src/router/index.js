import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/Home'
import GoHome from '@/views/gohome/GoHome'
import Cart from '@/views/cart/Cart'
import Mine from '@/views/mine/Mine'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    redirect: '/gohome',
    children: [
      {
        path: '/gohome',
        name: 'gohome',
        component: GoHome
      },
      {
        path: '/cart',
        name: 'cart',
        component: Cart
      },
      {
        path: '/mine',
        name: 'mine',
        component: Mine,
        meta: {
          isAuth: true
        }
      }
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
