const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    setupMiddlewares (middlewares, devserver) {
      const zh = require('./data/zh.json')
      const xl = require('./data/xl.json')
      const sx = require('./data/sx.json')
      const obj = { zh, xl, sx }
      devserver.app.get('/api/list', (req, res) => {
        const { type } = req.query
        if (obj[type]) {
          res.send({
            code: 0,
            msg: '成功',
            data: obj[type]
          })
        } else {
          res.send({
            code: -1,
            msg: '查找的数据不存在'
          })
        }
      })
      return middlewares
    }
  }
})
