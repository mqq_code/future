import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/Home'
import Detail from '@/views/Detail'
import Search from '@/views/Search'
import List from '@/views/List'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'homme',
    component: Home,
    redirect: '/list/zh',
    children: [
      {
        path: '/list/:id',
        name: 'list',
        component: List
      }
    ]
  },
  {
    path: '/detail',
    name: 'detail',
    component: Detail
  },
  {
    path: '/search',
    name: 'search',
    component: Search
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
