import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: { // 存公共数据
    num: 10,
    list: [],
    username: '王晓明',
    age: 22,
    sex: '男',
    goods: {
      title: '苹果',
      price: 20
    }
  },
  mutations: { // 改变state的唯一方式，mutations 中的函数必须是同步函数
    changeNum (state, payload) {
      console.log(state, payload)
      state.num += payload
    },
    addList (state, payload) {
      state.list.push({
        id: Date.now(),
        text: payload
      })
    },
    remove (state, id) {
      const index = state.list.findIndex(v => v.id === id)
      state.list.splice(index, 1)
    },
    addCount (state, num) {
      if (typeof state.goods.count !== 'number') {
        // 给对象添加新的响应式属性
        Vue.set(state.goods, 'count', num)
      } else {
        state.goods.count += num
      }
      console.log(state.goods)
    },
    addAge (state) {
      state.age++
    }
  },
  getters: { // 类似组件中计算属性，函数中的依赖项改变自动重新计算
    userInfo (state) {
      return `我叫${state.username},性别${state.sex},今年${state.age}岁`
    }
  }
})

export default store
