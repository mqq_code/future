## Vue（响应式数据，数据驱动视图更新，组件化开发）

### 声明式渲染
- 只能渲染表达式，不可以渲染语句（if、for、switch...）
```jsx
<div>{{ title }}</div>
```

### 指令：（以 v- 开头的特殊属性）
1. v-text: 设置元素的 innerText，不可以解析标签
2. v-html: 设置元素的 innerHTML，可以解析标签
3. v-show: 显示隐藏dom元素，添加删除元素的 display: none 样式
4. v-if、v-else-if、v-else: 添加删除dom元素
5. v-for: 遍历数据渲染元素，可以遍历数组、数字、字符串、对象
- 注意：需要在同一元素上使用 v-if 和 v-for 时改成计算属性
6. v-on: 简写 @ ，绑定事件，表达式可以是一个方法的名字或一个内联语句，内联语句中可以使用 $event 获取事件对象
7. v-bind: 简写 : , 让标签的属性可以解析 vue 变量
8. v-model: 表单双向绑定
```jsx
<input type="text" v-model="变量">
{/* 以下写法的语法糖 */}
<input type="text" :value="变量" @input="变量 = $event.target.value">
```

### 计算属性（当一个变量时由一个或者多个变量生成时使用计算属性）
1. 是一个函数，必须有返回值，页面使用时不需要调用函数，vue会自动执行，返回一个缓存的值，不可以手动给计算属性赋值
2. 函数内使用的 vue 变量改变时会自动重新执行函数，计算返回结果，内部使用的 vue 变量没有改变不会执行函数

### watch 侦听器
1. 监听变量改变执行对应的函数（例如：监听分页变量改变调用接口获取数据）
2. 监听引用类型的属性改变需要深度监听

### 生命周期

1. 创建阶段：vue 实例被创建 
  - beforeCreate: 创建前，此时 data 和 methods 中的数据都还没有初始化 ，不可以使用 data 中的数据
  - created： 创建完毕，data 中有值，可以发起服务端请求，获取数据 
2. 挂载阶段： vue 实例被挂载到真实DOM节点 
  - beforeMount：组件挂载之前，可以发起服务端请求，获取数据 
  - mounted: 组件挂载完成，此时可以操作 Dom
3. 更新阶段：当 vue 实例里面的 data 数据变化时，触发组件的重新渲染 
  - beforeUpdate：数据修改后，页面更新之前
  - updated：页面更新完成，可以获取最新的 dom
4. 销毁阶段：vue 实例被销毁 
  - beforeDestroy：实例被销毁前，此时可以手动销毁一些异步操作（定时器，事件监听，请求）
  - destroyed：实例销毁后

### 常用 api
1. $nextTick
```js
this.$nextTick(() => {
  // 本次页面更新后立即执行，可以获取到最新的 dom
})
```
2. $set
```js
// 给对象添加或修改响应式属性，可以通过下标修改数组
this.$set(对象/数组, key/index, 值)
// this.obj.新属性 = val // 不会触发页面更新
```
- ***注意：不能通过下标修改数组内容，也不能通过 length 修改数组，必须使用数组的方法修改或者使用 $set *** 
3. $watch: 可以监听数据变化
```js
this.$watch('变量名', () => {
  // 变量更新执行此函数
})
```
4. $refs: 获取dom元素和组件实例对象
```js
// <div ref="box">box</div>
this.$refs.box // 获取 box dom元素
```