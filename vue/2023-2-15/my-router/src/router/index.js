import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/home/Home.vue'
// import Detail from '../views/detail/Detail.vue'
import Login from '../views/login/Login.vue'
import City from '../views/city/City.vue'
import Notfound from '../views/404.vue'
import Movie from '../views/home/movie/Movie.vue'
import Cinema from '../views/home/cinema/Cinema.vue'
import News from '../views/home/news/News.vue'
import Mine from '../views/home/mine/Mine.vue'
import Hot from '../views/home/movie/hot/Hot.vue'
import Coming from '../views/home/movie/coming/Coming.vue'

// 安装路由插件
Vue.use(VueRouter)

// 创建路由实例对象
const router = new VueRouter({
  // 路由模式
  // 1: hash 地址栏有 #，原理：使用 hashchange 事件监听 # 后的内容改变，渲染对应的组件
  // 2: history 地址栏没#，页面刷新时会找不到文件，需要服务器配置如果找不到文件直接返回 index.html, 原理：h5的history对象中的 pushState和replaceState
  mode: 'history',
  routes: [ // 配置路由
    {
      path: '/home',
      name: 'home',
      component: Home,
      redirect: '/home/movie',
      children: [ // 配置子路由
        {
          path: '/home/movie',
          name: 'movie',
          component: Movie,
          redirect: '/home/movie/hot',
          children: [
            {
              path: '/home/movie/hot',
              name: 'hot',
              component: Hot
            },
            {
              path: '/home/movie/coming',
              name: 'coming',
              component: Coming
            }
          ]
        },
        {
          path: '/home/cinema',
          name: 'cinema',
          component: Cinema
        },
        {
          path: '/home/news',
          name: 'news',
          component: News
        },
        {
          path: '/home/mine',
          name: 'mine',
          component: Mine
        }
      ],
      meta: {
        isAuth: true
      }
    },
    {
      // 配置动态路由
      path: '/detail/:filmId',
      name: 'detail',
      component: () => import(/* webpackChunkName: 'detail' */'../views/detail/Detail.vue'),
      meta: {
        isAuth: true
      }
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/city',
      name: 'city',
      component: City,
      // 路由独享守卫，只对当前路由生效
      beforeEnter: (to, from, next) => {
        // console.log('city')
        next()
      }
    },
    {
      path: '/404',
      name: 'notfound',
      component: Notfound
    },
    {
      path: '/',
      redirect: '/home' // 重定向
    },
    {
      path: '*',
      redirect: '/404' // 重定向
    }
  ]
})

// 路由全局前置守卫
// router.beforeEach((to, from, next) => {
//   // console.log('to', to) // 跳转的目标路由
//   // console.log('from', from) // 跳转之前的路由
//   // 根据路由元信息判断当前路由是否需要拦截
//   // to.matched 当前路由层级
//   if (to.matched.some(item => item.meta.isAuth)) {
//     // 判断是否登陆
//     const token = localStorage.getItem('token')
//     if (token) {
//       next()
//     } else {
//       next({
//         path: '/login',
//         query: {
//           redirect: to.fullPath
//         }
//       })
//     }
//   } else {
//     // 允许跳转页面
//     next()
//   }
// })

// // 全局解析守卫
// router.beforeResolve((to, from, next) => {
//   next()
// })
// // 全局后置钩子
// router.afterEach((to, from) => {
//   // console.log(to)
//   // console.log(from)
// })

export default router
