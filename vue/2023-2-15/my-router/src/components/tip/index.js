import Tip from './Tip'

export default function (Vue) {
  // 获取组件构造函数
  const TipConstructor = Vue.extend(Tip)
  // 存组件实例对象
  let tipInstance = null
  let timer = null
  Vue.prototype.$tip = function (msg) {
    if (!tipInstance) {
      // 创建组件实例对象
      tipInstance = new TipConstructor()
      // 创建元素
      const tipWrap = document.createElement('div')
      document.body.appendChild(tipWrap)
      // 把组件实例挂载到元素上
      tipInstance.$mount(tipWrap)
    }
    // 给组件传参数
    tipInstance.msg = msg
    if (timer) clearTimeout(timer)
    timer = setTimeout(() => {
      tipInstance.msg = ''
      timer = null
    }, 2000)
  }
}
