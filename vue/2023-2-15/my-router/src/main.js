import Vue from 'vue'
import App from './App.vue'
import router from './router'
import { Button, Cell, Toast } from 'vant'
import Tip from './components/tip'
// 全部引入
// import Vant from 'vant'
// import 'vant/lib/index.css'
// Vue.use(Vant)

// 按需引入
Vue.use(Button)
Vue.use(Cell)
Vue.use(Toast)
Vue.use(Tip)
Vue.prototype.$bus = new Vue()
Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
