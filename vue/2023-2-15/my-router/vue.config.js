const { defineConfig } = require('@vue/cli-service')
const express = require('express')
const md5 = require('md5')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    setupMiddlewares (middleware, devServer) {
      devServer.app.use(express.json())
      const data = require('./data/data.json')
      devServer.app.get('/api/list', (req, res) => {
        const { pagenum, pagesize } = req.query
        // 1 10 data.slice(0, 10)
        // 2 10 data.slice(10, 20)
        // 3 10 data.slice(20, 30)
        res.send({
          code: 0,
          msg: '成功',
          total: data.length,
          data: data.slice(pagenum * pagesize - pagesize, pagenum * pagesize),
          pagenum: Number(pagenum),
          pagesize: Number(pagesize)
        })
      })
      devServer.app.get('/api/detail', (req, res) => {
        const { id } = req.query
        const movie = data.find(item => item.filmId === id * 1)
        if (movie) {
          res.send({
            code: 0,
            msg: '成功',
            data: movie
          })
        } else {
          res.send({
            code: -1,
            msg: '此电影不存在'
          })
        }
      })
      devServer.app.post('/api/login', (req, res) => {
        const { username, pwd } = req.body
        if (!username || !pwd) {
          res.send({
            code: -1,
            msg: '参数错误'
          })
        } else {
          res.send({
            code: 0,
            msg: '成功',
            token: md5(username + pwd)
          })
        }
      })
      devServer.app.get('/api/city', (req, res) => {
        const city = require('./data/city.json')
        res.send({
          code: 0,
          msg: '成功',
          data: city
        })
      })
      return middleware
    },
    proxy: {
      '/mapapi': {
        target: 'http://api.map.baidu.com',
        pathRewrite: { '^/mapapi': '' }
      }
    }
  }
})
