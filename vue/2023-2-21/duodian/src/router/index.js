import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/home/Home'
import GoHome from '@/views/home/gohome/GoHome'
import Cart from '@/views/home/cart/Cart'
import Mine from '@/views/home/mine/Mine'
import Edit from '@/views/edit/Edit'
import Address from '@/views/address/Address'
import Login from '@/views/login/Login'
import Detail from '@/views/detail/Detail'
import Pay from '@/views/pay/Pay'
import Notfound from '@/views/404'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    redirect: '/gohome',
    children: [
      {
        path: '/gohome',
        name: 'gohome',
        component: GoHome
      },
      {
        path: '/cart',
        name: 'cart',
        component: Cart
      },
      {
        path: '/mine',
        name: 'mine',
        component: Mine,
        meta: {
          isAuth: true
        }
      }
    ]
  },
  {
    path: '/address',
    name: 'address',
    component: Address
  },
  {
    path: '/edit',
    name: 'edit',
    component: Edit
  },
  {
    path: '/detail/:id',
    name: 'detail',
    component: Detail
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/pay',
    name: 'pay',
    component: Pay,
    meta: {
      isAuth: true
    }
  },
  {
    path: '/404',
    name: '404',
    component: Notfound
  },
  {
    path: '*',
    redirect: '/404'
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  console.log(to)
  if (to.matched.some(v => v.meta.isAuth)) {
    const token = localStorage.getItem('token')
    if (token) {
      next()
    } else {
      next({
        path: '/login',
        query: {
          redirect: to.fullPath
        }
      })
    }
  } else {
    next()
  }
})

export default router
