import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Vant, { Lazyload } from 'vant'
import 'vant/lib/index.css'
import VConsole from 'vconsole'

const vConsole = new VConsole()
console.log(vConsole)
console.log(navigator.userAgent)
const isiPhone = navigator.userAgent.indexOf('iPhone') > -1
const isAndroid = navigator.userAgent.indexOf('Android') > -1
console.log('是否为iphone', isiPhone)
console.log('是否为Android', isAndroid)
Vue.use(Vant)
Vue.use(Lazyload)
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
