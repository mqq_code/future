export default {
  namespaced: true,
  state: () => ({
    curAddress: {}, // 当前选择地址
    addressList: [] // 地址列表
  }),
  mutations: {
    changeAddress (state, payload) {
      state.curAddress = payload
    },
    add (state, payload) {
      state.addressList.push({
        id: Date.now(),
        ...payload
      })
    },
    edit (state, payload) {
      const index = state.addressList.findIndex(v => v.id === payload.id)
      state.addressList.splice(index, 1, payload)
      if (state.curAddress.id === payload.id) {
        state.curAddress = payload
      }
    },
    remove (state, id) {
      const index = state.addressList.findIndex(v => v.id === id)
      state.addressList.splice(index, 1)
      if (state.curAddress.id === id) {
        state.curAddress = {}
      }
    }
  }
}
