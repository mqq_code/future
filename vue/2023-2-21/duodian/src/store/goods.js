import axios from 'axios'

export default {
  namespaced: true,
  state: () => ({
    list: [],
    cart: []
  }),
  mutations: {
    setList (state, payload) {
      state.list = payload
    },
    addCart (state, id) {
      // 判断购物车中是否存在此商品
      const goods = state.cart.find(v => v.id === id)
      if (goods) {
        goods.count++
      } else {
        // 根据 id 从 list 中查找添加的商品
        const item = state.list.find(v => v.id === id)
        state.cart.push({
          count: 1,
          checked: true,
          ...item
        })
      }
    },
    changeCount (state, { id, num }) {
      const index = state.cart.findIndex(v => v.id === id)
      state.cart[index].count += num
      if (state.cart[index].count <= 0) {
        state.cart.splice(index, 1)
      }
    },
    changeChecked (state, id) {
      const item = state.cart.find(v => v.id === id)
      item.checked = !item.checked
    },
    changeAll (state, checked) {
      state.cart.forEach(item => {
        item.checked = checked
      })
    }
  },
  actions: {
    async getList ({ commit }) {
      const res = await axios.get('/api/goods')
      commit('setList', res.data)
    }
  },
  getters: {
    total (state) {
      return state.cart.reduce((prev, val) => {
        return {
          price: prev.price + (val.checked ? val.count * val.price : 0),
          count: prev.count + (val.checked ? val.count : 0)
        }
      }, {
        price: 0,
        count: 0
      })
    }
  }
}
