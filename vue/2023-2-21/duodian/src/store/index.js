import Vue from 'vue'
import Vuex from 'vuex'
import address from './address'
import goods from './goods'
import { createVuexPersistedState } from 'vue-persistedstate'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    address,
    goods
  },
  plugins: [
    // 持久化存储数据
    createVuexPersistedState({
      key: 'vuex',
      storage: window.localStorage
    })
  ]
})

export default store
