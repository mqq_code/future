const { defineConfig } = require('@vue/cli-service')
const Mock = require('mockjs')

const data = Mock.mock({
  'list|200': [
    {
      "id": "@id",
      "img": "@image(100x100, @color, @name)",
      "title": "@cname",
      "price|1-10": 1
    }
  ]
})

module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    setupMiddlewares (middlewares, devserver) {
      devserver.app.get('/api/goods', (req, res) => {
        res.send(data.list)
      })
      return middlewares
    },
    proxy: {
      '/map': {
        target: 'https://api.map.baidu.com',
        pathRewrite: { '^/map': '' },
        changeOrange: true
      }
    }
  }
})
