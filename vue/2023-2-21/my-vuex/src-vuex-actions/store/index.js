import Vue from 'vue'
import Vuex from 'vuex'
import Logger from 'vuex/dist/logger' // vuex插件，触发 action 或者 mutations 自动打印 log
import axios from 'axios'
Vue.use(Vuex)

export default new Vuex.Store({
  state: { // 存数据
    list: []
  },
  mutations: { // 修改数据的唯一方式，必须是同步函数
    setList (state, payload) {
      state.list = payload
    }
  },
  actions: { // 处理异步
    async getList (context, payload) {
      // context: 类似 this.$store 的对象，可以触发 mutations 或者 actions 函数执行
      const res = await axios.get('/api/list111', { params: payload })
      // commit mutations 函数修改 list
      context.commit('setList', res.data)
      return res.data
    },
    async getList1 ({ commit }, payload) {
      const res = await axios.get('/api/list', { params: payload })
      commit('setList', res.data)
    }
  },
  plugins: [new Logger()]
})
