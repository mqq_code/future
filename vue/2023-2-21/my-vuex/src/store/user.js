
export default {
  namespaced: true,
  state: () => ({
    title: '用户信息仓库',
    username: '王小明',
    email: '123@qq.com',
    sex: '男',
    age: 20,
    avatar: 'http//img.alicdn.com/bao/uploaded/i4/299244686/O1CN01jLQBiF1kUESGkLKYd_!!0-item_pic.jpg'
  }),
  mutations: {
    setList (state, payload) {
      console.log('user.js setList')
    }
  },
  actions: {},
  getters: {}
}
