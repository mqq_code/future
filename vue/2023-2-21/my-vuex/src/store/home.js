import axios from 'axios'

export default {
  namespaced: true,
  state: () => ({
    title: 'home仓库',
    list: []
  }),
  mutations: {
    setList (state, payload) {
      console.log('home.js setList')
      state.list = payload
    }
  },
  actions: {
    async getList (context, payload) {
      // context: 类似 this.$store 的对象，可以触发 mutations 或者 actions 函数执行
      const res = await axios.get('/api/list', { params: payload })
      // commit mutations 函数修改 list
      context.commit('setList', res.data)
      return res.data
    }
  },
  getters: {}
}
