import Vue from 'vue'
import Vuex from 'vuex'
import Logger from 'vuex/dist/logger' // vuex插件，触发 action 或者 mutations 自动打印 log
import home from './home'
import cart from './cart'
import user from './user'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    rootTitle: '根仓库标题'
  },
  plugins: [new Logger()],
  modules: { // 拆分子模块
    home,
    user,
    cart
  }
})
