
export default {
  namespaced: true, // 让此仓库内的 mutations 和 actions 函数只对当前仓库生效
  state: () => ({
    title: '购物车仓库',
    cartList: []
  }),
  mutations: {
    setList (state, payload) {
      console.log('cart.js setList')
    }
  },
  actions: {},
  getters: {
    total (state, getters, rootState, rootGetters) {
      console.log(state)
      return 100
    }
  }
}
