const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    setupMiddlewares (middlewares, devserver) {
      const zh = require('./data/zh.json')
      devserver.app.get('/api/list', (req, res) => {
        res.send(zh)
      })
      return middlewares
    }
  }
})
