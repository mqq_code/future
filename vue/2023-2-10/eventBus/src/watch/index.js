
class Bus {
  list = {}
  on (key, fn) {
    this.list[key] = fn
  }

  emit (key, ...rest) {
    this.list[key] && this.list[key](...rest)
  }
}

export default Bus
