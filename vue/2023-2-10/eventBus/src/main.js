import Vue from 'vue'
import App from './App.vue'
// import Bus from './watch' // 发布订阅模式

Vue.config.productionTip = false
Vue.prototype.$bus = new Vue()

new Vue({
  render: h => h(App)
}).$mount('#app')
