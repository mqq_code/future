const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    setupMiddlewares: (middlewares, devServer) => {
      // devServer.app === express()
      // 模拟后端接口
      const data = require('./data/data.json')
      devServer.app.get('/api/list', (req, res) => {
        res.send(data)
      })
      return middlewares
    }
  }
})
