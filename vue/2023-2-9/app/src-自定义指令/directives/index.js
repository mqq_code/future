import copy from './copy'
import test1 from './test1'
import abc from './abc'

const directives = {
  copy,
  test1,
  abc
}

export default function (Vue) {
  for (const key in directives) {
    Vue.directive(key, directives[key])
  }
}
