const copy = {
  bind (el, binding) { // 只调用一次，指令第一次绑定到元素时调用。在这里可以进行一次性的初始化设置。
    // console.log('指令绑定到元素', el)
    // console.log('指令信息', binding)
    el.$copyValue = binding.value
    el.$copyHandler = () => {
      const textarea = document.createElement('textarea')
      textarea.value = el.$copyValue
      document.body.appendChild(textarea)
      textarea.select()
      document.execCommand('copy')
      textarea.remove()
      console.log('复制成功')
    }
    el.addEventListener('click', el.$copyHandler)
  },
  inserted (el, binding) { // 被绑定元素插入父节点时调用，可以获取父级节点
    // console.log('inserted', el)
    // console.log('inserted', binding)
  },
  update (el, binding) { // 组件更新时调用
    // console.log('更新了')
    // console.log('update', el)
    // console.log('update', binding)
    if (binding.value !== binding.oldValue) {
      el.$copyValue = binding.value
    }
  },
  unbind (el) { // 指令解绑时调用，清除异步任务
    console.log('指令解绑')
    el.removeEventListener('click', el.$copyHandler)
  }
}

export default copy
