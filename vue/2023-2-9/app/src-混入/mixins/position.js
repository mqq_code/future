const position = {
  data () {
    return {
      mousePos: {
        x: 0,
        y: 0
      }
    }
  },
  methods: {
    mousemove (e) {
      this.mousePos = {
        x: e.pageX,
        y: e.pageY
      }
    }
  },
  mounted () {
    console.log('position')
    document.addEventListener('mousemove', this.mousemove)
  },
  beforeDestroy () {
    document.removeEventListener('mousemove', this.mousemove)
  }
}

export default position
