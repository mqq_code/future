const { defineConfig } = require('@vue/cli-service')
const fs = require('fs')
const path = require('path')
const express = require('express')

module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    setupMiddlewares (middlewares, devserver) {
      devserver.app.use(express.json())
      devserver.app.get('/api/list', (req, res) => {
        const data = JSON.parse(fs.readFileSync(path.join(__dirname, './data/data.json')))
        const { pagenum, pagesize, keywords } = req.query
        let data1 = data
        if (keywords) {
          data1 = data.filter(item => item.name.includes(keywords.trim()))
        }
        res.send({
          code: 0,
          msg: '成功',
          data: data1.slice(pagenum * pagesize - pagesize, pagenum * pagesize),
          pagenum: Number(pagenum),
          pagesize: Number(pagesize),
          total: data1.length
        })
      })
      devserver.app.delete('/api/del', (req, res) => {
        const { id } = req.query
        const data = JSON.parse(fs.readFileSync(path.join(__dirname, './data/data.json')))
        const index = data.findIndex(v => String(v.id) === String(id))
        if (index > -1) {
          data.splice(index, 1)
          fs.writeFileSync(path.join(__dirname, './data/data.json'), JSON.stringify(data))
          res.send({
            code: 0,
            msg: '成功'
          })
        } else {
          res.send({
            code: -1,
            msg: '参数错误'
          })
        }
      })
      devserver.app.post('/api/add', (req, res) => {
        const params = req.body
        const data = JSON.parse(fs.readFileSync(path.join(__dirname, './data/data.json')))
        data.push({
          id: Date.now() + '',
          index: data[data.length - 1].index + 1,
          ...params
        })
        fs.writeFileSync(path.join(__dirname, './data/data.json'), JSON.stringify(data))
        res.send({
          code: 0,
          msg: '成功'
        })
      })
      devserver.app.put('/api/edit', (req, res) => {
        const { id } = req.body
        const data = JSON.parse(fs.readFileSync(path.join(__dirname, './data/data.json')))
        const index = data.findIndex(v => String(v.id) === String(id))
        if (index > -1) {
          data.splice(index, 1, { ...data[index], ...req.body})
          fs.writeFileSync(path.join(__dirname, './data/data.json'), JSON.stringify(data))
          res.send({
            code: 0,
            msg: '成功'
          })
        } else {
          res.send({
            code: -1,
            msg: '参数错误'
          })
        }
      })
      return middlewares
    }
  }
})
