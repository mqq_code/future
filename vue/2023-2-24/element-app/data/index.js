const Mock = require('mockjs')
const fs = require('fs')
const path = require('path')
const arr = ['唱歌', '跳舞', '篮球', '下棋', 'rap', '画画']
const data = Mock.mock({
  "list|1000": [
    {
      "id": "@id",
      "index|+1": 1,
      "name": "@cname",
      "age|18-40": 20,
      "sex|0-1": 0,
      "address": "@county(true)",
      "desc": "@cword(10, 100)",
      "hobby": function () {
        const len = Math.floor(Math.random() * 6)
        const arr1 = arr.sort(() => Math.random() > 0.5)
        return arr1.slice(0, len)
      },
      "married": "@boolean",
      "time": "@datetime"
    }
  ]
})
fs.writeFileSync(path.join(__dirname, './data.json'), JSON.stringify(data.list), 'utf-8')