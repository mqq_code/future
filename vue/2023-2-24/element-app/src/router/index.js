import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/home/Home'
import Dashboard from '@/views/home/dashboard/Dashboard'
import List from '@/views/home/list/List'
import Info from '@/views/home/info/Info'
import Edit from '@/views/home/edit/Edit'
import Login from '@/views/login/Login'
import Notfound from '@/views/404'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    redirect: '/dashboard',
    children: [
      {
        path: '/dashboard',
        name: 'dashboard',
        component: Dashboard
      },
      {
        path: '/list',
        name: 'list',
        component: List
      },
      {
        path: '/edit',
        name: 'edit',
        component: Edit
      },
      {
        path: '/info',
        name: 'info',
        component: Info
      }
    ]
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/404',
    name: '404',
    component: Notfound
  },
  {
    path: '*',
    redirect: '/404'
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
