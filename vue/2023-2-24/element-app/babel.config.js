module.exports = {
  // 预设：插件的集合
  presets: [
    '@vue/cli-plugin-babel/preset'
  ]
  // 按需加载element-ui组件
  // plugins: [
  //   [
  //     "component",
  //     {
  //       "libraryName": "element-ui",
  //       "styleLibraryName": "theme-chalk"
  //     }
  //   ]
  // ]
}
