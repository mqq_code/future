import Vue from 'vue'
import App from './App.vue'
import VConsole from 'vconsole'

// 手机端调试
const vConsole = new VConsole()
console.log(vConsole)
Vue.config.productionTip = false

new Vue({
  render: h => h(App)
}).$mount('#app')
