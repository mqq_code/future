## Vue（响应式数据，数据驱动视图更新，组件化开发）

### 声明式渲染
- 只能渲染表达式，不可以渲染语句（if、for、switch...）
```jsx
<div>{{ title }}</div>
```

### 指令：（以 v- 开头的特殊属性）
1. v-text: 设置元素的 innerText，不可以解析标签
2. v-html: 设置元素的 innerHTML，可以解析标签
3. v-show: 显示隐藏dom元素，添加删除元素的 display: none 样式
4. v-if、v-else-if、v-else: 添加删除dom元素
5. v-for: 遍历数据渲染元素，可以遍历数组、数字、字符串、对象
6. v-on: 简写 @ ，绑定事件，表达式可以是一个方法的名字或一个内联语句，内联语句中可以使用 $event 获取事件对象
7. v-bind: 简写 : , 让标签的属性可以解析 vue 变量
8. v-model: 表单双向绑定
```jsx
<input type="text" v-model="变量">
{/* 以下写法的语法糖 */}
<input type="text" :value="变量" @input="变量 = $event.target.value">
```