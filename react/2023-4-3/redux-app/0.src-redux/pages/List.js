import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import store from '../store'

export default class List extends Component {
  
  state = {
    num: store.getState().num
  }


  subNum = () => {
    store.dispatch({
      type: 'SUB_NUM',
      payload: 2
    })
  }

  componentDidMount() {
    this.un = store.subscribe(() => {
      console.log('list页面数据更新了')
      this.setState({
        num: store.getState().num
      })
    })
  }
  componentWillUnmount() {
    this.un()
  }

  render() {
    return (
      <div>
        <h1>列表页面</h1>
        {this.state.num}
        <button onClick={this.subNum}>-</button>
        <hr />
        <Link to="/">跳转首页</Link>
      </div>
    )
  }
}
