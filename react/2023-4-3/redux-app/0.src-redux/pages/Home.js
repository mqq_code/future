import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import store from '../store'

export default class Home extends Component {

  state = {
    num: store.getState().num
  }


  addNum = () => {
    // 发送 action，执行 reducer 函数返回新数据
    // action: 有type属性的对象，描述本次修改内容
    store.dispatch({
      type: 'ADD_NUM',
      payload: 2
    })
  }

  componentDidMount() {
    // store.subscribe 返回清除监听的方法
    this.unListener =  store.subscribe(() => {
      // 监听仓库数据改变会执行此函数
      console.log('home页面获取新数据', store.getState())
      this.setState({
        num: store.getState().num
      })
    })
  }
  componentWillUnmount() {
    // 销毁监听
    this.unListener()
  }

  render() {
    return (
      <div>
        <h1>Home <Link to="/list">跳转列表</Link></h1>
        {this.state.num}
        <button onClick={this.addNum}>+</button>
      </div>
    )
  }
}
