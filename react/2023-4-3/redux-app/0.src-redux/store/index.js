import { createStore } from 'redux'

const initState = {
  num: 100
}

// reducer: 返回和修改state
const reducer = (state = initState, action) => {
  // state: 当前数据（上一次reducer函数的返回值）
  // action: 本次修改内容的描述信息
  console.log('reducer函数执行了', state, action)
  if (action.type === 'ADD_NUM') {
    return {...state, num: state.num + action.payload}
  } else if (action.type === 'SUB_NUM') {
    return {...state, num: state.num - action.payload}
  }

  return state
}

// 创建仓库，第一个参数必须是 reducer 函数
const store = createStore(reducer)

export default store