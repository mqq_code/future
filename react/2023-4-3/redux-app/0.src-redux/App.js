import React from 'react'
import routes from './router'
import RouterView from './router/RouterView'


const App = () => {
  return (
    <div className='app'>
      <RouterView routes={routes} />
    </div>
  )
}

export default App
