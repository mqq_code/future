import Home from '../pages/Home'
import List from '../pages/List'

const routes = [
  {
    path: '/',
    component: Home,
    exact: true
  },
  {
    path: '/list',
    component: List,
    exact: true
  },
  {
    to: '/'
  }
]

export default routes
