import React from 'react'
import { Link } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'



const Home = (props) => {
  // 获取 dispatch 函数
  const dispatch = useDispatch()
  // 获取仓库数据
  const num = useSelector(state => state.num)
  const obj = useSelector(state => {
    // state === store.getState() 仓库中的所有数据
    return {
      test: state.test,
      arr: state.arr
    }
  })
  const state = useSelector(s => s)

  const addNum = () => {
    dispatch({
      type: 'ADD_NUM',
      payload: 2
    })
  }

  return (
    <div>
      <h1>Home <Link to="/list">跳转列表</Link></h1>
      {num}
      <button onClick={addNum}>+</button>
    </div>
  )
}

export default Home
