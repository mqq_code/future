import React from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'

const List = (props) => {

  return (
    <div>
      <h1>列表页面</h1>
      {props.num}
      <button onClick={() => props.subNum(3)}>-</button>
      <hr />
      <Link to="/">跳转首页</Link>
    </div>
  )
}

const mapState = (state, props) => {
  return {
    num: state.num
  }
}
const mapDispatch = dispatch => {
  return {
    subNum: num => dispatch({ type: 'SUB_NUM', payload: num })
  }
}
export default connect(mapState, mapDispatch)(List)
