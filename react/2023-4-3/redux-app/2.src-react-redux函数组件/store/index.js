import { createStore, applyMiddleware } from 'redux'
import logger from 'redux-logger' // 自动打印log的中间件

const initState = {
  num: 10,
  test: '测试数据',
  arr: [1,2,3,45]
}

// reducer: 返回和修改state
const reducer = (state = initState, action) => {
  // state: 当前数据（上一次reducer函数的返回值）
  // action: 本次修改内容的描述信息
  // console.log('reducer函数执行了', state, action)
  if (action.type === 'ADD_NUM') {
    return {...state, num: state.num + action.payload}
  } else if (action.type === 'SUB_NUM') {
    return {...state, num: state.num - action.payload}
  }

  return state
}

// 创建仓库，第一个参数必须是 reducer 函数
// applyMiddleware: 添加中间件
// 中间件：增强dispatch，给dispatch添加额外的功能
const store = createStore(reducer, applyMiddleware(logger))

export default store