import React, { Suspense } from 'react';
import ReactDOM from 'react-dom/client';
import './index.scss';
import App from './App';
import { HashRouter } from 'react-router-dom'
import { Provider } from 'react-redux' // 链接组件和仓库
import store from './store';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <Suspense fallback={<div style={{ fontSize: 26 }}>loading...</div>}>
    {/* 把 store 传给所有的 children */}
    <Provider store={store}>
      <HashRouter>
        <App />
      </HashRouter>
    </Provider>
  </Suspense>
);
