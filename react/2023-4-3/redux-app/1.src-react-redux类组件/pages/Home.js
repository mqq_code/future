import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'

class Home extends Component {


  addNum = () => {
    this.props.dispatch({
      type: 'ADD_NUM',
      payload: 2
    })
  }

  render() {
    console.log(this.props)
    return (
      <div>
        <h1>Home <Link to="/list">跳转列表</Link></h1>
        {this.props.num}
        <button onClick={this.addNum}>+</button>
      </div>
    )
  }
}

// connect 高阶组件：连接组件和仓库，把仓库的数据传给组件的 props

// 把函数的返回值添加到组件的 props 中
const mapState = state => {
  // state === store.getState() 仓库所有数据
  return {
    a: 'AAAA',
    num: state.num
  }
}
export default connect(mapState)(Home)
