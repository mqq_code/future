import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'

class List extends Component {
  
 
  subNum = () => {
    this.props.dispatch({
      type: 'SUB_NUM',
      payload: 2
    })
  }

  render() {
    console.log(this.props)
    return (
      <div>
        <h1>列表页面</h1>
        {this.props.num}
        <button onClick={this.subNum}>-</button>
        <hr />
        <Link to="/">跳转首页</Link>
      </div>
    )
  }
}

const mapState = (state, props) => {
  console.log(props)
  return {
    num: state.num
  }
}
export default connect(mapState)(List)
