import React from 'react'
import List from '../components/List'
import Mark from '../components/Mark'
import { useStore } from '../store'

const Home = () => {
  const [state] = useStore()
  return (
    <div className="home">
      <List />
      {state.selected.length > 0 && <Mark />}
    </div>
  )
}

export default Home