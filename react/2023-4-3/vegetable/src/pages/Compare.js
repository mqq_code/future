import React, { useMemo, useState } from 'react'
import { useStore } from '../store'
import classNames from 'classnames'

const Compare = () => {
  const [state] = useStore()
  const tablist = useMemo(() => {
    return [
      {
        title: '建议少吃',
        desc: '以下食物升糖指数较高，可以建议少吃',
        color: 'red',
        list: state.selected.filter(v => v.glycemicIndex > 40)
      },
      {
        title: '适量食用',
        desc: '以下食物升糖指数适中，可以适量食用',
        color: 'orange',
        list: state.selected.filter(v => v.glycemicIndex <= 40 && v.glycemicIndex >= 20)
      },
      {
        title: '放心食用',
        desc: '以下食物升糖指数较低，可以放心食用',
        color: 'green',
        list: state.selected.filter(v => v.glycemicIndex < 20)
      }
    ]
  }, [state.selected])
  const [curIndex, setCurIndex] = useState(0)
  return (
    <div className="compare">
      <div className="nav">
        {tablist.map((item, index) =>
          <p key={item.title} className={classNames({ active: curIndex === index })} onClick={() => setCurIndex(index)}>
            <b>{item.list.length}</b>
            <b>{item.title}</b>
          </p>
        )}
      </div>
      <div className="compare_list">
        <h3 style={{ color: tablist[curIndex].color }}>{tablist[curIndex].desc}</h3>
        <ul>
          {tablist[curIndex].list.map(item =>
            <li key={item.name}>
              <img src={item.imgUrl} width={60} alt="" />
              <h4>{item.name}</h4>
              <p>{item.content}</p>
            </li>
          )}
        </ul>
      </div>
    </div>
  )
}

export default Compare