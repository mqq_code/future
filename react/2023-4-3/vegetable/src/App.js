import React from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
import Home from './pages/Home'
import Compare from './pages/Compare'
import { Provider } from './store'

const App = () => {
  return (
    <Provider>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/compare" component={Compare} />
        <Redirect to="/" />
      </Switch>
    </Provider>
  )
}

export default App