import React, { useEffect, useState } from 'react'
import { useStore } from '../store'
import axios from 'axios'
import classNames from 'classnames'

const List = () => {
  const [state, dispatch] = useStore()
  const [curIndex, setCurIndex] = useState(0)
  const change = name => {
    dispatch({
      type: 'CHANGE_SELECT',
      payload: name
    })
  }
  const getList = async () => {
    const res = await axios.get('/api/list')
    // 调用reducer函数
    dispatch({
      type: 'SET_LIST',
      payload: res.data
    })
  }
  useEffect(() => {
    if (state.list.length === 0) {
      getList()
    }
  }, [])

  return (
    <div className='list'>
      <div className="menu">
        {state.list.map((item, index) =>
          <p key={item.title} className={classNames({active: curIndex === index})} onClick={() => setCurIndex(index)}>
            {item.title}
          </p>
        )}
      </div>
      <div className="content">
        {state.list[curIndex] && state.list[curIndex].list.map(item =>
          <div key={item.name} className={classNames('food', { active: item.checked })} onClick={() => change(item.name)}>
            <img src={item.imgUrl} alt="" />
            <p>{item.name}</p>
          </div>
        )}
      </div>
    </div>
  )
}

export default List