import React from 'react'
import { useStore } from '../store'
import { Link } from 'react-router-dom'

const Mark = () => {
  const [state, dispatch] = useStore()
  return (
    <div className="mark">
      <div className="mark_header">
        <b>已选食物({state.selected.length})</b>
        <Link to="/compare">去测评</Link>
      </div>
      <div className="mark_list">
        {state.selected.map(item =>
          <div key={item.name} className="mark_item">
            {item.name}
            <span onClick={() => dispatch({ type: 'CHANGE_SELECT', payload: item.name })}>x</span>
          </div>
        )}
      </div>
    </div>
  )
}

export default Mark