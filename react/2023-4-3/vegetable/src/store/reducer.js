
// 初始值
export const initValue = {
  list: [],
  selected: [] // 选中的数据
}

// 组件中调用 dispatch 触发此函数修改 state
const reducer = (state, action) => {
  console.log(action)
  if (action.type === 'SET_LIST') {
    return {...state, list: action.payload}
  } else if (action.type === 'CHANGE_SELECT') {
    let selected = [...state.selected]
    const newlist = [...state.list]
    newlist.forEach(item => {
      item.list.forEach(val => {
        if (val.name === action.payload) {
          val.checked = !val.checked
          if (val.checked) {
            selected.unshift(val)
          } else {
            selected = selected.filter(v => v.name !== action.payload)
          }
        }
      })
    })
    return {list: newlist, selected}
  }
  return state
}

export default reducer
