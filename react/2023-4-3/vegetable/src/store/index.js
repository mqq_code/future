import { createContext, useContext, useReducer } from 'react'
import reducer,{ initValue } from './reducer'

// 创建 context 对象
export const storeCtx = createContext()

// 定义组件把 value 传给所有的 children
export const Provider = (props) => {
  const store = useReducer(reducer, initValue)
  return (
    <storeCtx.Provider value={store}>
      {props.children}
    </storeCtx.Provider>
  )
}

// 获取 Provider 组件 value 的值
export const useStore = () => useContext(storeCtx)

