import React, { useEffect, useState, useRef, createRef } from 'react'


const App = () => {
  const [title, setTitle] = useState('默认值')
  const ref1 = useRef(11111) // 可以设置默认值，ref对象始终只向同一个地址
  const ref2 = createRef(22222) // 无法设置默认值，每次更新都会重新创建
  
  useEffect(() => {
    setTimeout(() => {
      ref1.current = 'AAAAAAAA'
      ref2.current = 'BBBBBBBB'
      console.log('ref1 修改完成', ref1)
      console.log('ref2 修改完成', ref2)
    }, 2000)
  }, [])

  console.log('ref1', ref1)
  console.log('ref2', ref2)

  return (
    <div>
      <h1>useRef和createRef的区别</h1>
      <div>
        <input type="text" value={title} onChange={e => setTitle(e.target.value)} />
        {title}
      </div>
    </div>
  )
}

export default App

