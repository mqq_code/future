import React, { Component } from 'react'
import Count from './components/Count'

class App extends Component {

  state = {
    list: [1, 2, 3, 4, 5],
    num: 0
  }

  render() {
    return (
      <div>
        <h1>App</h1>
        <button onClick={() => this.setState({ num: this.state.num + 1 })}>{this.state.num}</button>
        <hr />
        <Count list={this.state.list} />
      </div>
    )
  }
}

export default App
