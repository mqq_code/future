import React, { useState } from 'react'

// v16.8 之前函数组件不能定义state，没有生命周期，只能做纯渲染
// v16.8 之后react新增hooks让函数组件可以拥有状态，可以完全代理class组件的功能
/*
  Hook 就是 JavaScript 函数，函数名称必须使用 use 开头，
  使用它们会有两个额外的规则：
    1. 只能在函数最外层调用 Hook。不要在循环、条件判断或者子函数中调用。(因为要保证每次调用hook的顺序是一致的)
    2. 只能在 React 的函数组件和自定义 hook 中调用 Hook。不要在其他 JavaScript 函数中调用。
*/

function Count (props) {
  // 函数组件定义数据，返回一个数组 ===》 [数据，更新数据的函数]
  const [num, setNum] = useState(0)
  const [show, setShow] = useState(true)
  const [arr, setArr] = useState([])
  const [title, setTitle] = useState('默认标题')
  const [obj, setObj] = useState({
    name: '孙帅',
    age: 22,
    sex: '男'
  })

  // if (num === 0) {
  //   const [aa, setaa] = useState('aa')
  // }

  let a = 0 // 不要此方式定义数据，每次组件更新数据都会清空
  const pushArr = () => {
    setArr([...arr, arr.length])
  }
  const changeObj = (key, e) => {
    setObj({ ...obj, [key]: e.target.value })
  }
  const add = () => {
    // 异步更新数据，连续多次调用会合并成一次更新
    // setNum(num + 1)
    // setNum(num + 1)
    // setNum(num + 1)

    // 传入函数可以获取最新的 state 数据，使用函数的返回值覆盖原数据
    setNum(n => n + 1)
    setNum(n => n + 1)
    setNum(n => n + 1)
    console.log(num)
  }

  console.log('函数组件更新了')
  return (
    <div>
      <h2>{title}</h2>
      <input type="text" value={title} onChange={e => setTitle(e.target.value)} />
      <button onClick={() => setTitle('默认标题')}>reset</button>
      <hr />
        {a}
        <button onClick={() => {
          a++
          console.log(a)
        }}>a+</button>
      <hr />
      <div>
        <button onClick={() => setNum(num - 1)}>-</button>
        {num}
        <button onClick={add}>+</button>
      </div>
      <hr />
      <button onClick={pushArr}>add arr</button>
      <ol>
        {arr.map(item => <li key={item}>{item}</li>)}
      </ol>
      <hr />
      <button onClick={() => setShow(!show)}>{show ? '隐藏' : '显示'}</button>
      {show &&
        <ul>
          {props.list.map(item => <li key={item}>{item}</li>)}
        </ul>
      }
      <hr />
      <div>
        <p>姓名: {obj.name} <input type="text" value={obj.name} onChange={e => changeObj('name', e)} /></p>
        <p>性别: {obj.sex} <input type="text" value={obj.sex} onChange={e => changeObj('sex', e)} /></p>
        <p>年龄: {obj.age} <input type="text" value={obj.age} onChange={e => changeObj('age', e)} /></p>
      </div>
    </div>
  )
}

export default Count