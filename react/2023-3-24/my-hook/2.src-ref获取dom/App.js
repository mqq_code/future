import React, { useEffect, useState, useRef } from 'react'
import axios from 'axios'


const App = () => {
  const [banners, setBanners] = useState([])
  // 使用ref获取dom
  const ulRef = useRef()

  // 正确示范：
  const getBanner = async () => {
    const res = await axios.get('http://zyxcl.xyz/banner')
    console.log(res.data.banners)
    setBanners(res.data.banners)
  }
  useEffect(() => {
    getBanner()
  }, [])

  useEffect(() => {
    // 获取dom
    console.log(ulRef.current)
  }, [banners])

  return (
    <div>
      <ul ref={ulRef}>
        {banners.map(item => <li key={item.targetId}><img width={300} src={item.imageUrl} /></li>)}
      </ul>
    </div>
  )
}

export default App

