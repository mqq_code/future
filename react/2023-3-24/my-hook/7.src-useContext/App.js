import React, { useState } from 'react'
import Child from './components/Child'
import { Provider } from './context/title'

const App = () => {
  const [title, setTitle] = useState('默认值')

  return (
    <Provider value={{ title, setTitle }}>
      <div>
        <h1>useContext</h1>
        <div>
          <input type="text" value={title} onChange={e => setTitle(e.target.value)} />
          {title}
        </div>
        <Child  />
      </div>
    </Provider>
  )
}

export default App

