import { createContext } from 'react'


const titleCtx = createContext()
export const Provider = titleCtx.Provider
export const Consumer = titleCtx.Consumer
export default titleCtx
