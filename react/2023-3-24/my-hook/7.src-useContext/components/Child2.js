import React, { useContext} from 'react'
import titleCtx from '../context/title'

const Child2 = (props) => {
  // 获取父级 Provider 传过来的数据
  const titleInfo = useContext(titleCtx)
  console.log(titleInfo)

  return (
    <div className="box">
      <h3>child2 - {titleInfo.title}</h3>
      name：<input type="text" value={titleInfo.title} onChange={e => titleInfo.setTitle(e.target.value)} />
    </div>
  )
}

export default Child2