import React from 'react'
import Child2 from './Child2'

const Child = () => {
  return (
    <div className="box">
      <h2>Child</h2>
      <Child2 />
    </div>
  )
}

export default Child