import React, { useEffect, useState, useRef } from 'react'


const App = () => {
  const [pos, setPos] = useState({ left: 0, top: 0})
  /*
    ref 存数据
      1. 类似 class 组件中的实例属性，数据改变不会引起组件渲染
      2. ref 保证储存的对象在组件中始终只向同一个地址
  */ 
  const isDown = useRef(false)
  const downPos = useRef({ x: 0, y: 0 })

  const mouseDown = e => {
    isDown.current = true
    downPos.current = {
      x: e.nativeEvent.offsetX,
      y: e.nativeEvent.offsetY
    }
  }
  
  useEffect(() => {
    document.addEventListener('mousemove', e => {
      if (isDown.current) {
        const { x, y } = downPos.current
        setPos({
          left: e.clientX - x,
          top: e.clientY - y
        })
      }
    })
    document.addEventListener('mouseup', e => {
      isDown.current = false
      downPos.current = {
        x: 0,
        y: 0
      }
    })
  }, [])

  return (
    <div>
      <h1>ref存数据</h1>
      <div className="box" style={pos} onMouseDown={mouseDown}></div>
    </div>
  )
}

export default App

