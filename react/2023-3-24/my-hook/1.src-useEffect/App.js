import React, { useEffect, useState } from 'react'
import axios from 'axios'

const Child = (props) => {
  const [count, setCount] = useState(0)

  useEffect(() => {
    const timer = setInterval(() => {
      // 必须使用函数的方式获取count最新值
      setCount(c => c + 1)
    }, 500)

    return () => {
      // 组件销毁时执行，类似 class 中的 componentWillUnmount
      clearInterval(timer)
      console.log('组件销毁时执行')
    }
  }, [])


  // 可以监听对象的属性变化
  // useEffect(() => {
  //   console.log('%cchild组件', 'color: red;font-size: 30px', props.title)
  // }, [props.title])


  return (
    <div>
      <h3>子组件</h3>
      <p>定时器：{count}</p>
    </div>
  )
}


const App = () => {
  const [num, setNum] = useState(0)
  const [title, setTitle] = useState('useEffect')

  // useEffect: 处理组件中的副作用（与组件渲染无关的操作，例如：定时器，调接口，操作dom），可以实现类似 class 组件中的生命周期的功能，但是和生命周期有本质的不同
  // useEffect(callback, [依赖的变量])
  
  // 第二个参数不传，只要有数据更新就会执行, 类似 class 中 componentDidUpdate
  // useEffect(() => {
  //   // 等待组件渲染完成执行，可以获取到最新的数据和dom
  //   console.log('useEffect', num, document.querySelector('button').innerHTML)
  // })
  // 第二个参数传入空数组，只执行一次，类似 class 中 componentDidMount
  // useEffect(() => { // 可以调用接口，操作dom，设置定时器
  //   console.log('1111111')
  // }, [])
  // 第二个参数传入具体变量,依赖项更新时执行
  // useEffect(() => {
  //   console.log('num', num)
  //   console.log('title', title)
  // }, [num, title])


  // 错误示范：useEffect的函数不能时async函数，因为async函数返回一个Promise对象，组件销毁时不能当函数调用
  // useEffect(async () => {
  //   const res = await axios.get('http://zyxcl.xyz/banner')
  //   console.log(res)
  // }, [])

  // 正确示范：
  const getBanner = async () => {
    const res = await axios.get('http://zyxcl.xyz/banner')
    console.log(res)
  }
  useEffect(() => {
    getBanner()
  }, [])



  return (
    <div>
      <h1>{title}</h1>
      <input type="text" value={title} onChange={e => setTitle(e.target.value)} />
      <div>
        <button onClick={() => setNum(num - 1)}>-</button>
        {num}
        <button onClick={() => setNum(num + 1)}>+</button>
      </div>
      
      <hr />
      {num > -1 && <Child title={title} />}
    </div>
  )
}

export default App

// const aa = async () => {}

// console.log(aa())



