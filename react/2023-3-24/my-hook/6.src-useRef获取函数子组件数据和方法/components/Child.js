import React, { useState, forwardRef, useImperativeHandle } from 'react'

const Child = (props, ref) => {

  // console.log(props)
  // console.log('父组件传过来的 ref 对象', ref)

  const [num, setNum] = useState(0)
  const addNum = n => {
    console.log('addNum')
    setNum(nu => nu + n)
  }

  useImperativeHandle(ref, () => {
    console.log('useImperativeHandle')
    // 把返回值赋值给父组件ref对象的current属性
    return {
      num,
      addNum
    }
  }, [num])

  return (
    <div>
      <h2>Child</h2>
      {num}
    </div>
  )
}

// 使用 forwardRef 接收父组件传过来的 ref 对象
export default forwardRef(Child)