import React, { forwardRef, useImperativeHandle, useRef } from 'react'

const Child2 = (props, ref) => {
  const inpRef = useRef(null)

  useImperativeHandle(ref, () => {
    return {
      inp: inpRef.current,
      focus: () => {
        inpRef.current.focus()
      }
    }
  }, [])


  return (
    <div>
      name：<input ref={inpRef} type="text" />
    </div>
  )
}

export default forwardRef(Child2)