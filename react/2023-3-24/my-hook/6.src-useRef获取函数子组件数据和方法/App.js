import React, { useEffect, useState, useRef } from 'react'
import Child from './components/Child'
import Child2 from './components/Child2'

const App = () => {
  const [title, setTitle] = useState('默认值')
  const childRef = useRef(123)
  const child2Ref = useRef({})

  useEffect(() => {
    console.log(childRef)
  }, [])

  const addChildNum = () => {
    // 父组件调用子组件数据和方法
    childRef.current.addNum(2)
    setTimeout(() => {
      console.log(childRef.current.num)
    })
  }

  return (
    <div>
      <h1>useRef获取函数子组件的数据和方法</h1>
      <div>
        <input type="text" value={title} onChange={e => setTitle(e.target.value)} />
        {title}
      </div>
      <button onClick={addChildNum}>修改Child组件数据</button>
      <button onClick={() => {
        console.log(child2Ref)
        child2Ref.current.inp.value = 123
        child2Ref.current.focus()
      }}>Child2组件input获取焦点</button>
      <hr />
      <Child ref={childRef} test="test" />
      <hr />
      <Child2 ref={child2Ref} />
    </div>
  )
}

export default App

