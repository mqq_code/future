import React, { useState, useEffect } from 'react'
import axios from 'axios'

const App = () => {

  useEffect(() => {
    axios.get('/api/list').then(res => {
      console.log(res.data)
    })
  }, [])

  return (
    <div>

    </div>
  )
}

export default App

