import Mock from 'mockjs'
import data from './data.json'

// 模拟接口
Mock.mock('/api/list', 'get', function () {
  return data
})
