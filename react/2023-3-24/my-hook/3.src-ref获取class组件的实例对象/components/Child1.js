import React, { Component } from 'react'

class Child1 extends Component {

  state = {
    num: 0
  }
  add = (n) => {
    this.setState({
      num: this.state.num + n
    })
  }

  render() {
    return (
      <div>
        <h1>class 组件</h1>
        {this.state.num}
      </div>
    )
  }
}

export default Child1
