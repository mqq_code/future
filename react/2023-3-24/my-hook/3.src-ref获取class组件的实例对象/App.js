import React, { useEffect, useState, useRef } from 'react'
import Child1 from './components/Child1'

const App = () => {
  const childRef = useRef()

  useEffect(() => {
    console.log(childRef.current)
  }, [])

  return (
    <div>
      <h1>ref获取class组件实例</h1>
      <button onClick={() => {
        // 调用子组件的方法和数据
        childRef.current.add(2)
      }}>+</button>
      <hr />
      {/* ref获取子组件实例对象 */}
      <Child1 ref={childRef} />
    </div>
  )
}

export default App

