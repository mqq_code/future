import React, { Component } from 'react'

class App extends Component {
  state = {
    num: 0
  }

  addNum = () => {
    // react 直接修改数据不会触发页面更新，需要调用 this.setState 手动触发更新
    // this.setState 会把传入的数据和原本的state进行合并
    // this.setState 是异步更新
    // 传入对象：连续调用多次 setState 无法获取最新的 state
    // 传入函数：函数的参数可以获取到最新的state
    this.setState({ num: this.state.num + 1 })
    this.setState({ num: this.state.num + 1 })
    this.setState({ num: this.state.num + 1 })

    console.log('无法获取修改之后的数据', this.state.num)
  }

  sub = () => {
    // react 回把本次调用的所有的 setState 合并成一次更新
    this.setState(s => {
      // s: 最新的state
      // 把返回的对象和原本的state进行合并
      console.log('第一次调用setState', s)
      return { num: s.num - 1 }
    })
    this.setState(s => {
      console.log('第二次调用setState', s)
      return { num: s.num - 1 }
    })
    this.setState(s => {
      console.log('第三次调用setState', s)
      return { num: s.num - 1 }
    }, () => {
      console.log('组件更新后执行此函数，可以获取到更新后的数据和dom元素', this.state)
    })
  }

  render() {
    const { num } = this.state
    console.log('渲染组件', num)
    return <div>
      <button onClick={this.sub}>-</button>
      {num}
      <button onClick={this.addNum}>+</button>
    </div>
  }
}

export default App

