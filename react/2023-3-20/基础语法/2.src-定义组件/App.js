import React, { Component } from 'react'

// 类组件
// 1. 必须继承 React.Component
// 2. 必须定义render函数，返回一个react元素
class App extends Component {
  // 存和组件渲染相关的数据
  state = {
    num: 0,
    list: [1, 2, 3, 4, 5, 6, 7, 8],
    title: '标题'
  }
  // 其他实例属性可以存和组件渲染无关的数据
  aaa = '大标题'

  addNum = (n) => {
    // vue 数据是响应式，可以直接修改数据
    // react 直接修改数据不会触发页面更新，需要调用 this.setState 手动触发更新
    // this.setState 会把传入的数据和原本的state进行合并
    // this.setState 是异步更新
    this.setState({
      num: this.state.num + n
    })

    // 无法获取到更新后的数据
    console.log('add num', this.state)
  }

  change = e => {
    if (e.keyCode === 13) {
      this.setState({
        title: e.target.value
      })
    }
  }

  render() {
    const { num, list, title } = this.state
    return <div>
      <h1>{title}</h1>
      {/* 小驼峰定义事件 */}
      <div>
        <input type="text" onKeyDown={this.change} />
      </div>
      {num > 0 &&
        <>
          <button onClick={() => this.addNum(-1)}>-</button>
          {num}
        </>
      }
      <button onClick={() => this.addNum(1)}>+</button>
      <ul>
        {list.map(item =>
          <li key={item}>{item}</li>
        )}
      </ul>
    </div>
  }
}

export default App


// 函数式组件，v16.8 之前不能定义状态，没有生命周期，只能做纯渲染
// v16.8 之后react新增 hook 可以完全代替 class 组件的功能
// import React from 'react'
// const App = () => {
//   return (
//     <div>App</div>
//   )
// }
// export default App
