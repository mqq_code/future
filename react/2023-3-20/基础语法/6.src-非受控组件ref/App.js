import React, { Component, createRef } from 'react'

class App extends Component {
  state = {
  }
  formName = null // 定义实例属性存dom元素
  formAge = createRef() // 创建ref对象
  reset = () => {
    this.formName.value = ''
    this.formAge.current.value = ''
  }
  submit = () => {
    console.log('name', this.formName.value)
    console.log('age', this.formAge.current.value)
  }
  getName = el => this.formName = el
  render() {
    return <div>
      {/* 非受控组件：获取dom元素获取value */}
      {/* 方式一：ref 传入一个函数，函数的参数就是dom元素 */}
      {/* 方式二：ref 传入一个ref对象，ref对象的current属性就是dom元素 */}
      <p>姓名: <input type="text" defaultValue="默认值小明" ref={this.getName}/></p>
      <p>年龄: <input type="text" defaultValue={20} ref={this.formAge} /></p>
      <p>记住账号: <input type="checkbox" defaultChecked /></p>
      <button onClick={this.submit}>提交</button>
      <button onClick={this.reset}>重置</button>
    </div>
  }
}

export default App

