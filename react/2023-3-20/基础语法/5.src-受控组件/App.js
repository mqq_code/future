import React, { Component } from 'react'

class App extends Component {
  state = {
    form: {
      name: '小明',
      age: 10
    }
  }
  reset = () => {
    this.setState({
      form: {
        name: '小明',
        age: 10
      }
    })
  }
  submit = () => {
    console.log(this.state.form)
  }
  change = (e, key) => {
    this.setState({
      form: {
        ...this.state.form,
        [key]: e.target.value
      }
    })
  }

  render() {
    const { form } = this.state
    return <div>
      {/* 受控组件：value受state变量控制，必须有onChange事件修改state数据 */}
      <p>姓名: <input type="text" value={form.name} onChange={e => this.change(e, 'name')} /></p>
      <p>年龄: <input type="text" value={form.age} onChange={e => this.change(e, 'age')} /></p>
      <button onClick={this.submit}>提交</button>
      <button onClick={this.reset}>重置</button>
      <hr />
      {JSON.stringify(form)}
    </div>
  }
}

export default App

