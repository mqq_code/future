import React, { Component } from 'react'

class App extends Component {
  state = {
    num: 0
  }

  addNum = () => {
    // this.setState({ num: this.state.num + 1 })
    // this.setState({ num: this.state.num + 1 })
    // this.setState({ num: this.state.num + 1 })
    // console.log('只渲染一次，异步执行', this.state.num)
    
    setTimeout(() => {
      // v18.0之前 this.setState 在react事件和生命周期中是异步执行，会合并更新
      // 在 setTimeout 和 原生事件中是同步执行，也不会合并更新
      this.setState({ num: this.state.num + 1 })
      this.setState({ num: this.state.num + 1 })
      this.setState({ num: this.state.num + 1 })
      console.log(this.state.num)
    })
  }

  sub = () => {
    this.setState({ num: this.state.num - 1 })
  }

  render() {
    const { num } = this.state
    console.log('render函数', num)
    return <div>
      <button onClick={this.sub}>-</button>
      {num}
      <button onClick={this.addNum}>+</button>
    </div>
  }
}

export default App

