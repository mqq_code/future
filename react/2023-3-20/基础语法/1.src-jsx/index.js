import React from 'react';
import ReactDOM from 'react-dom/client';

// const arr = ['a', 'b', 'c', 'd']
// const li = arr.map(item => React.createElement('li', null, [item]))
// // 创建react元素，虚拟dom
// const box1 = React.createElement('div', { className: 'box' }, [
//   React.createElement('h1', { id: 'title' }, [
//     React.createElement('span', null, ['开始学习']),
//     React.createElement('b', {
//       style: {
//         color: 'red'
//       }
//     }, ['react']),
//     '!!!!!!!!!'
//   ]),
//   React.createElement('ul', { className: 'list' }, li)
// ])
// console.log(box1)

// jsx语法：js + xml, (语法糖，通过 babel 转译成 React.createElement)
/*
  jsx 注意事项：
  1. class ==> className
  2. for ==> htmlFor (label标签中的for)
  3. style 必须写对象形式
  4. jsx 语法中使用变量需要放在 {} 内
  5. jsx 中的 {} 内只能使用表达式，不能使用语句
  6. jsx 标签内容部分 {} 中不能渲染对象，会报错
  7. 遍历数组使用map方法生成新数组渲染页面
  8. 条件判断：三元表达式 / 使用函数判断return不同的数据 / “&&”
*/

const title = "开始学习"
const objStyle = { color: 'red' }
const arr = ['a', 'b', 'c', 'd']
const arrLi = [<li>a</li>, <li>b</li>, <li>c</li>, <li>d</li>]
const testfn = () => {
  return '测试函数返回值'
}
const num = 1000
const renderNum = (n) => {
  if (n < 60) {
    return <div style={{ color: 'red' }}>不及格</div>
  } else if (n >= 60 && n < 90) {
    return <div style={{ color: 'yellow' }}>60 - 90 分</div>
  } else if (n >= 90 && n < 100) {
    return <div style={{ color: 'tomato' }}>90 - 100</div>
  } else {
    return <div style={{ color: 'green' }}>100以上</div>
  }
}
const box = <div className='box'>
  <h1 id='title'>
    <span>{title}</span>
    <b style={objStyle}>react</b>
    !!!!!!!
  </h1>
  <ul>
    {/* {arrLi} */}
    {arr.map(item => <li key={item}>{item}</li>)}
  </ul>
  <div>
    <input type="checkbox" id="men" />
    <label htmlFor="men">男</label>
  </div>
  <hr />
  {num > 100 ?
    <>
      <p>字符串: {'abc'}</p>
      <p>数字: {100}</p>
      <p>布尔值: {true}</p>
      <p>布尔值: {false}</p>
      <p>null: {null}</p>
      <p>undefined: {undefined}</p>
      <p>数组: {[1,2,3,4,5,6]}</p>
      {/* <p>对象: {objStyle}</p> */}
      <p>调用函数: {testfn()}</p>
    </>
  : <div>啥也没有</div>}
  <hr />
  {num > 100 && '这是真值'}
  <hr />
  {renderNum(num)}
  {/* {num < 60 ? '不及格' : (
    num >= 60 && num < 90 ? '60-90' : (
      num >= 90 && num < 100 ? '90-100' : '100以上'
    )
  )} */}
</div>




const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(box);



