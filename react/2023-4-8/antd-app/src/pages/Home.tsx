
import React from 'react'
import { Button, Table, Tag } from 'antd'
import style from './home.module.css'
import type { ColumnsType } from 'antd/es/table';

interface IData {
  id: string;
  name: string;
  age: number;
  address: string;
}

const columns: ColumnsType<IData> = [
  {
    key: 'name',
    title: '姓名',
    dataIndex: 'name'
  },
  {
    key: 'age',
    title: '年龄',
    dataIndex: 'age'
  },
  {
    key: 'address',
    title: '地址',
    dataIndex: 'address',
    render: (address, record) => {
      return <Tag>{address}</Tag>
    }
  }
];

const data: IData[] = [
  {
    id: '1',
    name: '老王',
    age: 32,
    address: '纽约'
  },
  {
    id: '2',
    name: '小李',
    age: 42,
    address: '伦敦'
  },
  {
    id: '3',
    name: '丽丽',
    age: 32,
    address: '山西'
  },
];

const Home = () => {
  return (
    <div className={style.home}>
      {/* <Button type="primary" className={style.btn}>按钮</Button> */}
      <Table columns={columns} dataSource={data} rowKey="id" />
    </div>
  )
}

export default Home