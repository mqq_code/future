import React from 'react'
import style from './titlebar.module.scss'
import { useHistory } from 'react-router-dom'

const TitleBar = (props) => {
  const history = useHistory()
  return (
    <div className={style.titlebar}>
      {props.showBack && <div className={style.back} onClick={() => history.goBack()}></div>}
      <h3>{props.title}</h3>
    </div>
  )
}

export default TitleBar