import React, { useState } from 'react'
import TitleBar from '../../components/titleBar/TitleBar'
import style from './detail.module.scss'
import { Button, Switch, Toast, Dialog } from 'antd-mobile'
import { useParams } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'

const ptObj = {
  pdd: '拼多多',
  tb: '淘宝',
  jd: '京东'
}

const Detail = (props) => {
  const params = useParams()
  const dispatch = useDispatch()
  const info = useSelector(state => {
    const index = state.findIndex(v => v.id === Number(params.id))
    return state[index] ? {
      ...state[index],
      num: index + 1
    } : {}
  })
  const [back, setBack] = useState({
    bj_back: info.bj_back,
    yj_back: info.yj_back
  })
  const save = () => {
    dispatch({
      type: 'SAVE',
      payload: {
        id: Number(params.id),
        ...back
      }
    })
    Toast.show('保存成功')
    props.history.goBack()
  }
  const remove = async () => {
    const res = await Dialog.confirm({
      content: '确定删除此任务吗？',
    })
    if (res) {
      dispatch({
        type: 'REMOVE',
        payload: Number(params.id)
      })
      Toast.show('删除成功')
      props.history.goBack()
    }
  }
  return (
    <div className={style.detail}>
      <TitleBar showBack title="详情页面" />
      <main>
        <div className={style.num}>任务编号：第 {info.num} 个任务</div>
        <div className={style.content}>
          <h2>{info.name} <span>{ptObj[info.pt]}</span></h2>
          <div className={style.row}>
            <p>支付本金 {info.bj} 元</p>
            <div className={style.switch}>
              <span>{back.bj_back ? '已返' : '未返'}</span>
              <Switch checked={back.bj_back} onChange={bj_back => setBack({...back, bj_back})} style={{ '--height': '26px', '--width': '40px' }}></Switch>
            </div>
          </div>
          <div className={style.row}>
            <p>支付佣金 {info.yj} 元</p>
            <div className={style.switch}>
            <span>{back.yj_back ? '已返' : '未返'}</span>
              <Switch checked={back.yj_back} onChange={yj_back => setBack({...back, yj_back})} style={{ '--height': '26px', '--width': '40px' }}></Switch>
            </div>
          </div>
          <div className={style.remark}>
            <span>备注：</span>
            <p>{info.remark}</p>
          </div>
        </div>
        <Button className={style.btn} block size="small" color='primary' onClick={save}>保存</Button>
        <Button className={style.btn} block size="small" color='danger' onClick={remove}>删除</Button>
      </main>
    </div>
  )
}

export default Detail