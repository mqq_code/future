import React, { useMemo } from 'react'
import style from './home.module.scss'
import { NavLink } from 'react-router-dom'
import TitleBar from '../../components/titleBar/TitleBar'

const navlist = [
  {
    path: '/',
    title: '备忘录'
  },
  {
    path: '/create',
    title: '新建'
  },
  {
    path: '/mine',
    title: '我的'
  }
]
const Home = (props) => {
  const title = useMemo(() => {
    return navlist.find(v => v.path === props.location.pathname)?.title
  }, [props.location])
  return (
    <div className={style.home}>
      <TitleBar title={title} />
      <main>{props.children}</main>
      <footer>
        {navlist.map(item =>
          <NavLink key={item.path} exact activeClassName={style.active} to={item.path}>{item.title}</NavLink>
        )}
      </footer>
    </div>
  )
}

export default Home