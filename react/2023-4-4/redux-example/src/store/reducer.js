

const initState = []

const reducer = (state = initState, action) => {
  if (action.type === 'CREATE_MEMO') {
    return [{
      ...action.payload,
      id: Date.now(),
      date: Date.now(),
    }, ...state]
  } else if (action.type === 'SAVE') {
    const newState = JSON.parse(JSON.stringify(state))
    newState.forEach(item => {
      if (item.id === action.payload.id) {
        item.bj_back = action.payload.bj_back
        item.yj_back = action.payload.yj_back
      }
    })
    return newState
  } else if (action.type === 'REMOVE') {
    return state.filter(v => v.id !== action.payload)
  }
  return state
}

export default reducer