import React from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
import Home from './pages/home/Home'
import Detail from './pages/detail/Detail'
import Memo from './pages/home/memo/Memo'
import Create from './pages/home/create/Create'
import Mine from './pages/home/mine/Mine'

const App = () => {
  return (
    <Switch>
      <Route exact path="/detail/:id" component={Detail} />
      <Route path="/" render={routeInfo => {
        return (
          <Home {...routeInfo}>
            <Switch>
              <Route exact path="/" component={Memo} />
              <Route exact path="/create" component={Create} />
              <Route exact path="/mine" component={Mine} />
              <Redirect to="/" />
            </Switch>
          </Home>
        )
      }} />
    </Switch>
  )
}

export default App