import { createContext, useContext } from 'react'
import useUserStore from './user'

export const storeCtx = createContext()

export const useStore = () => useContext(storeCtx)


export const Provider = (props) => {
  const userStore = useUserStore()
  return (
    <storeCtx.Provider value={userStore}>
      {props.children}
    </storeCtx.Provider>
  )
}
