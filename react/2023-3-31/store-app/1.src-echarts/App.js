import React from 'react'
import Home from './pages/Home'
import User from './pages/User'
import Test from './pages/Test'
import { Switch, Route, Redirect } from 'react-router-dom'

const App = () => {
  return (
    <Switch>
      <Route exact path="/" component={Home} />
      <Route exact path="/user" component={User} />
      <Route exact path="/test" component={Test} />
      <Redirect to="/" />
    </Switch>
  )
}

export default App