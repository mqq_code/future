import React from 'react'
import { Link } from 'react-router-dom'
import { useStore } from '../store'


const Home = () => {
  const [state, dispatch] = useStore()
  
  const addAge = () => {
    dispatch({
      type: 'ADD_AGE'
    })
  }
  const changeName = e => {
    dispatch({
      type: 'CHANGE_NAME',
      payload: e.target.value
    })
  }
  return (
    <div className="home">
      <h1>首页</h1>
      <p>姓名：{state.name} <input type="text" value={state.name} onChange={changeName} /></p>
      <p>年龄：{state.age} <button onClick={addAge}>+</button></p>
      <p>性别：{state.sex}</p>
      <p>地址：{state.address}</p>
      <Link to="/user">去编辑</Link>
    </div>
  )
}

export default Home