import React from 'react'
import Home from './pages/Home'
import User from './pages/User'
import { Switch, Route, Redirect } from 'react-router-dom'

const App = () => {
  return (
    <Switch>
      <Route exact path="/" component={Home} />
      <Route exact path="/user" component={User} />
      <Redirect to="/" />
    </Switch>
  )
}

export default App