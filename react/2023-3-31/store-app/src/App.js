import React from 'react'
import Home from './pages/Home'
import User from './pages/User'
import Test from './pages/Test'
import AntdPage from './pages/AntdPage'
import Login from './pages/Login'

import { Switch, Route, Redirect } from 'react-router-dom'

const App = () => {
  return (
    <Switch>
      <Route exact path="/" component={Home} />
      <Route exact path="/user" component={User} />
      <Route exact path="/test" component={Test} />
      <Route exact path="/antd" component={AntdPage} />
      <Route exact path="/login" component={Login} />
      <Redirect to="/" />
    </Switch>
  )
}

export default App