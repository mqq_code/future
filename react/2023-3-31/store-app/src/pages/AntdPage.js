import React, { useState, useEffect, useRef } from 'react'
import { Button, Space, Col, Row, Table, Modal, message } from 'antd';
import axios from 'axios'
import moment from 'moment';

const formatCount = count => {
  if (count >= 100000000) {
    return Math.round(count / 100000000) + '亿'
  } else if (count >= 10000) {
    return Math.round(count / 10000) + '万'
  } else {
    return count
  }
}

const AntdPage = () => {
  const [data, setData] = useState([])
  const [open, setOpen] = useState(false)
  const editRef = useRef()
  const columns = [
    {
      title: '榜单名称',
      dataIndex: 'name',
      key: 'name'
    },
    {
      title: '封面',
      dataIndex: 'coverImgUrl',
      key: 'coverImgUrl',
      render: img => <img src={img} style={{ width: '100px' }} />
    },
    {
      title: '创建时间',
      dataIndex: 'createTime',
      key: 'createTime',
      render: createTime => moment(createTime).format('YYYY-MM-DD kk:mm:ss')
    },
    {
      title: '播放量',
      dataIndex: 'playCount',
      key: 'playCount',
      render: formatCount
    },
    {
      title: '操作',
      key: 'actions',
      render: (row, record, index) => {
        return <Button danger type="primary" onClick={() => {
          setOpen(true)
          editRef.current = index
        }}>删除</Button>
      }
    }
  ];
  const ok = () => {
    const data1 = [...data]
    data1.splice(editRef.current, 1)
    setData(data1)
    setOpen(false)
    message.success('删除成功！')
  }

  useEffect(() => {
    axios.get('https://zyxcl.xyz/toplist/detail').then(res => {
      setData(res.data.list)
    })
  }, [])

  return (
    <div>
      <h2>AntdPage</h2>
      <Space wrap>
        <Button type="primary">Primary Button</Button>
        <Button>Default Button</Button>
        <Button type="dashed">Dashed Button</Button>
        <Button type="text">Text Button</Button>
        <Button type="link">Link Button</Button>
      </Space>
      {/* <Row gutter={20}>
        <Col span={8}><div className='gutter-row'>1111</div></Col>
        <Col span={8} offset={8}><div className='gutter-row'>2222</div></Col>
      </Row> */}
      <Table
        bordered
        dataSource={data}
        rowKey="id"
        columns={columns}
        pagination={{
          pageSize: 5,
          pageSizeOptions: [5, 10, 20, 30],
          showSizeChanger: true
        }}
      />
      <Modal
        title="警告"
        open={open}
        onOk={ok}
        onCancel={() => setOpen(false)}
        okText="确认删除"
        cancelText="取消"
      >
        确定要删除吗？
      </Modal>
    </div>
  )
}

export default AntdPage