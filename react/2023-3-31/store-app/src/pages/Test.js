import React, { useState, useEffect, useRef } from 'react'
import { Navigation, Pagination, Scrollbar, A11y } from 'swiper'
import { Swiper, SwiperSlide } from 'swiper/react'
import 'swiper/css'
import 'swiper/css/navigation'
import 'swiper/css/pagination'
import 'swiper/css/scrollbar'
import axios from 'axios'
// import * as echarts from 'echarts';
import Echarts from '../components/Echarts'

const Test = () => {
  const [banners, setBanners] = useState([])
  const [options, setOptions] = useState({
    xAxis: {
      type: 'category',
      data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
    },
    yAxis: {
      type: 'value'
    },
    series: [
      {
        data: [120, 200, 150, 80, 70, 110, 130],
        type: 'bar'
      }
    ]
  })
  // const boxRef = useRef()
  // const myChart = useRef()
  useEffect(() => {
    axios.get('http://zyxcl.xyz/banner').then(res => {
      setBanners(res.data.banners)
    })
    // 基于准备好的dom，初始化echarts实例
    // myChart.current = echarts.init(boxRef.current);
  }, [])

  const setEcharts = () => {
    const data = [];
    for (let i = 0; i <= 100; i++) {
      let theta = (i / 100) * 360;
      let r = 5 * (1 + Math.sin((theta / 180) * Math.PI));
      data.push([r, theta]);
    }
    const option = {
      title: {
        text: 'Two Value-Axes in Polar'
      },
      legend: {
        data: ['line']
      },
      polar: {},
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'cross'
        }
      },
      angleAxis: {
        type: 'value',
        startAngle: 0
      },
      radiusAxis: {},
      series: [
        {
          coordinateSystem: 'polar',
          name: 'line',
          type: 'line',
          data: data
        }
      ]
    };
    setOptions(option)
    // myChart.current.setOption(option)
  }

  return (
    <div>
      <Swiper
        modules={[Navigation, Pagination, Scrollbar, A11y]}
        navigation
        pagination={{ clickable: true }}
        scrollbar={{ draggable: true }}
        onSlideChange={() => console.log('slide change')}
        onSwiper={(swiper) => console.log(swiper)}
      >
        {banners.map(item =>
          <SwiperSlide key={item.targetId}>
            <img src={item.imageUrl} alt="" />
          </SwiperSlide>
        )}
      </Swiper>
      <button onClick={setEcharts}>获取echarts数据</button>
      {/* <div className="box" ref={boxRef}></div> */}
      <div className="box">
        <Echarts options={options} />
      </div>
    </div>
  )
}

export default Test