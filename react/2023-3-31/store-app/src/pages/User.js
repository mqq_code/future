import React, { useState } from 'react'
import { useStore } from '../store'

const User = (props) => {
  const [state, dispatch] = useStore()
  const [form, setForm] = useState({...state})
  const submit = () => {
    dispatch({
      type: 'SET_ALL',
      payload: {...form}
    })
    props.history.goBack()
  }
  return (
    <div>
      <h2>User</h2>
      <p>姓名：<input type="text" value={form.name} onChange={e => setForm({...form, name: e.target.value})} /></p>
      <p>年龄：<input type="text" value={form.age} onChange={e => setForm({...form, age: e.target.value})} /></p>
      <p>性别：<input type="text" value={form.sex} onChange={e => setForm({...form, sex: e.target.value})} /></p>
      <p>地址：<input type="text" value={form.address} onChange={e => setForm({...form, address: e.target.value})} /></p>
      <button onClick={submit}>确定修改</button>
      <button onClick={props.history.goBack}>取消</button>
    </div>
  )
}

export default User