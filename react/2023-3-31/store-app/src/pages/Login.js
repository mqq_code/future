import React from 'react'
import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { Button, Checkbox, Form, Input, Select } from 'antd';

const Login = () => {
  const onFinish = (values) => {
    console.log('表单的值 ', values);
  };
  const [form] = Form.useForm()
  return (
    <div className="login">
      <Form
        className="login_form"
        initialValues={{ remember: true }}
        onFinish={onFinish}
        form={form}
      >
        <Form.Item
          name="user"
          rules={[
            { required: true, message: '请输入用户名!' },
            { type: 'email', message: '邮箱格式错误!' }
          ]}
        >
          <Input prefix={<UserOutlined />} placeholder="用户名" />
        </Form.Item>
        <Form.Item
          name="tel"
          rules={[
            { required: true, message: '请输入手机号!' },
            { pattern: /^1[3-9]\d{9}$/, message: '手机号格式错误!' }
          ]}
        >
          <Input prefix={<UserOutlined />} placeholder="手机号" />
        </Form.Item>
        <Form.Item
          name="pwd"
          rules={[{ required: true, message: '请输入密码!' }]}
        >
          <Input.Password prefix={<LockOutlined />} placeholder="密码" />
        </Form.Item>
        <Form.Item>
          <Form.Item name="remember" valuePropName="checked" noStyle>
            <Checkbox>Remember me</Checkbox>
          </Form.Item>
          <a href="#">aaaaaa</a>
        </Form.Item>
        <Form.Item
          name="address"
          rules={[{ required: true, message: '请选择城市!' }]}
        >
          <Select
            style={{ width: 120 }}
            placeholder="请选择城市"
            options={[
              { value: '', label: '请选择', disabled: true },
              { value: 'beijing', label: '北京' },
              { value: 'shanghai', label: '上海' },
              { value: 'guangzhou', label: '广州' }
            ]}
          />
        </Form.Item>

        <Form.Item>
          <Button block type="primary" htmlType="submit">登陆</Button>
          <Button block style={{ marginTop: 20 }} onClick={() => {
            form.resetFields()
          }}>重置</Button>
        </Form.Item>
      </Form>
      <Button onClick={() => {
        form.submit() // 触发表单内的onFinish事件
      }}>提交表单</Button>
      <Button onClick={() => {
        form.validateFields(['user']).then(val => {
          console.log(val)
        })
      }}>校验表单</Button>
    </div>
  )
}

export default Login