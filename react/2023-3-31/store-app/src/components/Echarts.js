import React, { useState, useEffect, useRef } from 'react'
import * as echarts from 'echarts';

const Echarts = (props) => {
  const wrap = useRef()
  const myChart = useRef()
  useEffect(() => {
    myChart.current = echarts.init(wrap.current);
  }, [])

  useEffect(() => {
    myChart.current.setOption(props.options, true)
  }, [props.options])

  return (
    <div className='echarts-wrap' ref={wrap}>Echarts</div>
  )
}

export default Echarts