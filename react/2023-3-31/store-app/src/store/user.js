import { useReducer } from 'react'


const initState = {
  name: 'wang小明',
  age: 23,
  sex: '男',
  address: '上海'
}

// 修改数据的方法，组件中调用dispatch执行此函数，返回新数据
const reducer = (state, action) => {
  console.log(action)
  // state: 当前数据
  // action: 描述本次如何修改数据
  switch(action.type) {
    case 'ADD_AGE':
      return {...state, age: state.age + 1}
    case 'CHANGE_NAME':
      return {...state, name: action.payload}
    case 'SET_ALL':
      return action.payload
  }
  return state
  // if (action.type === 'ADD_AGE') {
  //   return {...state, age: state.age + 1}
  // } else if (action.type === 'CHANGE_NAME') {
  //   return {...state, name: action.payload}
  // } else if (action.type === 'SET_ALL') {
  //   return action.payload
  // }
  // return state
}

const useUserStore = () =>  useReducer(reducer, initState)
export default useUserStore