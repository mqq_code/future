import React from 'react'

const Header = (props) => {
  return (
    <div className="header">
      <ul>
        {props.topList.map(item =>
          <li key={item.title}>
            <img src={item.src} />
            <span onClick={() => props.change(item.title)}>x</span>
          </li>
        )}
        {props.topList.length < 3 && <li></li>}
      </ul>
      <button disabled={props.topList.length < 2} onClick={() => props.compare(true)}>compare</button>
    </div>
  )
}

export default Header