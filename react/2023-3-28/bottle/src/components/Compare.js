import React, { useState } from 'react'
import classNames from 'classnames'
const Compare = (props) => {
  const [isclose, setIsClose] = useState(false)
  const animateClassName = index => {
    if (isclose) {
      return index % 2 !== 0 ? 'animate__fadeOutUp' : 'animate__fadeOutDown'
    }
    return index % 2 !== 0 ? 'animate__fadeInDown' : 'animate__fadeInUp'
  }
  const onAnimationEnd = () => {
    if (isclose) {
      props.setVisible(false)
    }
  }
  return (
    <div className="compare">
      {props.topList.map((item, index) =>
        <div
          key={item.title}
          className={classNames('item', 'animate__animated', animateClassName(index))}
          onAnimationEnd={onAnimationEnd}
        >
          <img src={item.src} alt="" />
          <h3>{item.title}</h3>
          <p>{item.price}</p>
          <p>{item.alcohol}</p>
          <p>{item.year}</p>
          <p>{item.varietal}</p>
          <p>{item.region}</p>
        </div>
      )}
      {!isclose && <div className="close" onClick={() => setIsClose(true)}>x</div>}
    </div>
  )
}

export default Compare