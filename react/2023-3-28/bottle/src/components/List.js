import React from 'react'
import classNames from 'classnames'

const List = (props) => {
  return (
    <div className="list">
      {props.list.map(item =>
        <div className={classNames('bottle', { active: item.checked })} key={item.title}>
          <img src={item.src} alt="" />
          <h3>{item.title}</h3>
          <p>{item.price}</p>
          <button>add to cart</button>
          <span onClick={() => props.change(item.title)}>{item.checked ? '✅' : '+'}</span>
        </div>
      )}
    </div>
  )
}

export default List