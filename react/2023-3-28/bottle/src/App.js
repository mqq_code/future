import React, { useEffect, useState } from 'react'
import 'animate.css'
import './App.scss'
import axios from 'axios'
import Header from './components/Header'
import Compare from './components/Compare'
import List from './components/List'


const App = () => {
  const [list, setList] = useState([])
  const [topList, setTopList] = useState([])
  const [visible, setVisible] = useState(false)

  useEffect(() => {
    axios.get('/api/list').then(res => {
      setList(res.data)
    }).catch(e => {
      console.log(e)
    })
  }, [])

  const changeChecked = (title) => {
    const newList = [...list]
    const item = newList.find(item => item.title === title)
    if (topList.length >= 3 && !item.checked) return
    item.checked = !item.checked
    if (item.checked) {
      setTopList([...topList, item])
    } else {
      setTopList(topList.filter(v => v.title !== title))
    }
    setList(newList)
  }

  return (
    <div className="app">
      {topList.length > 0 && <Header topList={topList} change={changeChecked} compare={setVisible} />}
      <List list={list} change={changeChecked} />
      {visible && <Compare topList={topList} setVisible={setVisible} />}
    </div>
  )
}

export default App