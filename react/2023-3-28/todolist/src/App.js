import { useState, useMemo, useEffect } from  'react'
import './App.scss'
import Header from './components/header/Header'
import List from './components/list/List'
import Footer from './components/footer/Footer'

const App = () => {

  const [list, setList] = useState(JSON.parse(localStorage.getItem('todolist'))) // 所有数据
  const [type, setType] = useState(0) // 0: 全部，1:未完成，2:已完成
  const todo = useMemo(() => list.filter(v => !v.done), [list])
  const curList = useMemo(() => { // 根据选中的 type 筛选数据
    if (type === 0) {
      return list
    } else if (type === 1) {
      return list.filter(v => !v.done)
    }
    return list.filter(v => v.done)
  }, [list, type])

  useEffect(() => {
    if (curList.length === 0) {
      setType(0)
    }
  }, [curList])

  useEffect(() => {
    localStorage.setItem('todolist', JSON.stringify(list))
  }, [list])

  const add = text => {
    setList([...list, {
      id: Date.now(),
      done: false,
      text
    }])
  }

  const remove = id => {
    setList(list.filter(v => v.id !== id))
  }

  const change = id => {
    setList(list.map(item => {
      if (item.id === id) {
        return {...item, done: !item.done}
      }
      return item
    }))
  }

  const clearDone = () => {
    setList(list.filter(v => !v.done))
  }

  return (
    <div className="app">
      <Header add={add} />
      <List list={curList} remove={remove} change={change} />
      {list.length === 0 ?
        <div className="empty">还没有数据，快去事项吧</div>
      :
        <>
          <Footer
            type={type}
            setType={setType}
            todoLen={todo.length}
            allLen={list.length}
            clearDone={clearDone}
          />
          <div className="clear">
            <span onClick={() => setList([])}>clear</span>
          </div>
        </>
      }
    </div>
  )
}

export default App