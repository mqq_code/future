import React, { useRef } from 'react'
import style from './header.module.scss'

const Header = (props) => {
  const inp = useRef()
  const submit = () => {
    if (inp.current.value.trim()) {
      props.add(inp.current.value)
      inp.current.value = ''
    }
    inp.current.focus()
  }
  const keyDown = e => {
    if (e.keyCode === 13) {
      submit()
    }
  }

  return (
    <div className={style.header}>
      <h2>~ Today I need to ~</h2>
      <div className={style.form}>
        <input ref={inp} type="text" placeholder="add todo..." onKeyDown={keyDown} />
        <button onClick={submit}>submit</button>
      </div>
    </div>
  )
}

export default Header