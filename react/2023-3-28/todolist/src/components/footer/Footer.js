import React, { useState } from 'react'
import style from './footer.module.scss'
import classNames from 'classnames'

const nav = [
  {
    title: '全部',
    type: 0
  },
  {
    title: '未完成',
    type: 1
  },
  {
    title: '已完成',
    type: 2
  }
]

const Footer = ({ todoLen, allLen, type, setType, clearDone }) => {

  return (
    <div className={style.footer}>
      <div className={style.todo}>{todoLen} 项未完成</div>
      <div className={style.btns}>        
        {nav.map(item => {
          if (item.type == 0 || (allLen > todoLen && todoLen > 0)) {
            return <button
              key={item.type}
              className={classNames({[style.active]: type === item.type})}
              onClick={() => setType(item.type)}
            >{item.title}</button>  
          }
          return null
        })}
      </div>
      {allLen > todoLen && <button onClick={clearDone}>清空已完成</button>}
    </div>
  )
}

export default Footer