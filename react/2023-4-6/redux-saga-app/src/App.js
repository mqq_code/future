import React from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
import Home from './pages/home/Home'
import Movie from './pages/movie/Movie'
import MovieDetail from './pages/movie_detail/MovieDetail'
import User from './pages/user/User'

const App = () => {
  return (
    <Switch>
      <Route exact path="/movie" component={Movie} />
      <Route exact path="/movie/detail/:id" component={MovieDetail} />
      <Route exact path="/user" component={User} />
      <Route exact path="/" component={Home} />
      <Redirect to="/" />
    </Switch>
  )
}

export default App