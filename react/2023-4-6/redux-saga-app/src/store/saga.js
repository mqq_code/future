import { takeEvery, call, put } from 'redux-saga/effects'
import { getTopListApi, getBannerApi } from '../services/index'

function* getTopList(action) {
  // 通过 call 调用接口
  // call(返回 promise 对象的函数, 传给函数的参数)
  const res = yield call(getTopListApi, action.payload)
  // 发送 action 到 reducer 函数
  yield put({
    type: 'SET_TOPLIST',
    payload: res.data.list
  })
}

function* getBanner(action) {
  const res = yield call(getBannerApi)
  yield put({
    type: 'SET_BANNERS',
    payload: res.data.banners
  })
}



function* saga() {
  // 监听组件触发 GET_TOPLIST action 
  yield takeEvery('GET_TOPLIST', getTopList)
  yield takeEvery('GET_BANNER', getBanner)
  
}

export default saga