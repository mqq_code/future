
const init = {
  name: '小明',
  age: 20,
  sex: '男',
  banners: [],
  topList: []
}

const reducer = (state = init, action) => {
  switch(action.type) {
    case 'SET_NAME':
      return {...state, name: action.payload}
    case 'SET_TOPLIST':
      return {...state, topList: action.payload}
    case 'SET_BANNERS':
      return {...state, banners: action.payload}
  }
  return state
}
export default reducer