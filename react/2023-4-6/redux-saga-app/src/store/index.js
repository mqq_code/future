import { legacy_createStore as createStore, applyMiddleware } from 'redux'
import logger from 'redux-logger'
import reducer from './reducer'
import createSagaMiddleware from 'redux-saga'
import saga from './saga'

// 创建 saga 中间件
const sagaMiddleware = createSagaMiddleware()
const store = createStore(reducer, applyMiddleware(sagaMiddleware, logger))

// 运行 saga
sagaMiddleware.run(saga)

export default store
