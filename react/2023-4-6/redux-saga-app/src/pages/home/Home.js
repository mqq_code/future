import React from 'react'
import { Link } from 'react-router-dom'
import { useSelector } from 'react-redux'

const Home = () => {

  return (
    <div className="home">
      <h2>首页</h2>
      <p>
        <Link to="/movie">去电影</Link>
        <Link to="/user">个人信息</Link>
      </p>
    </div>
  )
}

export default Home