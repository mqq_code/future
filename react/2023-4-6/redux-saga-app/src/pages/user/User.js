import React, { useEffect, useRef } from 'react'
import { useDispatch, useSelector } from 'react-redux'

const User = () => {
  const dispatch = useDispatch()
  const info = useSelector(state => {
    return {
      name: state.name,
      age: state.age,
      sex: state.sex
    }
  })
  const banners = useSelector(state => state.banners)
  const inp = useRef()
  useEffect(() => {
    dispatch({ type: 'GET_BANNER' })
  }, [])
  return (
    <div>
      <h2>User</h2>
      <input type="text" ref={inp} />
      <button onClick={() => dispatch({
        type: 'SET_NAME',
        payload: inp.current.value
      })}>触发 SET_NAME</button>
      <p>{JSON.stringify(info)}</p>
      {JSON.stringify(banners)}
    </div>
  )
}

export default User