import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

const Movie = (props) => {
  const dispatch = useDispatch()
  const topList = useSelector(state => state.topList)

  useEffect(() => {
    dispatch({ type: 'GET_TOPLIST' })
  }, [])

  const goDetail = (id) => {
    props.history.push(`/movie/detail/${id}`)
  }
  return (
    <div>
      <ul>
        {topList.map(item =>
          <li key={item.id} onClick={() => goDetail(item.id)}>
            <img src={item.coverImgUrl} width="120" alt="" />
          </li>
        )}
      </ul>
    </div>
  )
}

export default Movie