import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'


const MovieDetail = (props) => {
  const dispatch = useDispatch()
  const info = useSelector(state => {
    return state.topList.find(v => v.id === Number(props.match.params.id)) || {}
  })

  useEffect(() => {
    dispatch({
      type: 'GET_TOPLIST',
      payload: 123
    })
  }, [])

  return (
    <div>
      <h2>{info.name}</h2>
      <img src={info.coverImgUrl} width="120" alt="" />
      <p>{info.description}</p>
      <p>播放量:{info.playCount}</p>
      <ul>
        {info.tracks?.map(item =>
          <li key={item.first}>{item.first} - {item.second}</li>
        )}
      </ul>
    </div>
  )
}

export default MovieDetail