import { getbanner } from '../services'

export default {

  namespace: 'home',

  state: {
    banners: [],
    toplist: [],
    num: 0
  },

  reducers: {
    setBanners(state, action) {
      return { ...state, banners: action.payload };
    },
    setToplist(state, action) {
      return { ...state, toplist: action.payload };
    },
    addNum(state, action) {
      return {...state, num: state.num + 1}
    }
  },

  effects: {
    *getBannerAction({ payload }, { call, put }) {  // eslint-disable-line
      const res = yield call(getbanner)
      yield put({
        type: 'setBanners',
        payload: res.data.banners
      })
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {  // eslint-disable-line
      console.log(history)
      history.listen(() => {
        console.log('路由跳转了')
        dispatch({
          type: 'addNum'
        })
      })
    },
  },

};
