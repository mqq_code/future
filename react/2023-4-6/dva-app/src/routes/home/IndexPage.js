import React, { useEffect, useState } from 'react';
import { connect } from 'dva';
import { Link } from 'react-router-dom'
import styles from './IndexPage.css';

function IndexPage(props) {

  return (
    <div className={styles.wrap}>
      <h1>dva - {props.num}</h1>
      <button onClick={() => {
        props.dispatch({
          type: 'home/getBannerAction',
          payload: '123'
        })
      }}>给banner赋值</button>
      <Link to="/detail">详情</Link>
      <hr />
      {JSON.stringify(props.banners)}
    </div>
  );
}

const mapState = state => {
  return {
    banners: state.home.banners,
    num: state.home.num
  }
}
export default connect(mapState)(IndexPage);
