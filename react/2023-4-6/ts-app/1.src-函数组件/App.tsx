import React, { useEffect, useState, useRef, useMemo } from 'react'
import Child, { IRef } from './components/Child';
interface IProps {
  title: string;
}

interface ITodoItem {
  id: number;
  text: string;
  done: boolean;
}

const App: React.FC<IProps> = (props) => {

  const [num, setNum] = useState(0)
  const [arr, setArr] = useState([] as ITodoItem[])
  const inpRef = useRef<HTMLInputElement>(null)
  const add = () => {
    setArr([...arr, {
      id: Date.now(),
      text: inpRef.current!.value,
      done: false
    }])
    inpRef.current!.value = ''
  }
  const changeDone = (id: number) => {
    setArr(arr.map(item => {
      if (item.id === id) {
        return {...item, done: !item.done}
      }
      return item
    }))
  }
  const doneList = useMemo(() => {
    return arr.filter(v => v.done)
  }, [arr])

  const ChildRef = useRef<IRef>(null)

  return (
    <div>
      <h1>{props.title}</h1>
      <input type="text" ref={inpRef} /> <button onClick={add}>add</button>
      <ul>
        {arr.map(item =>
          <li key={item.id}>
            {item.done ? <s>{item.text}</s> : item.text}
            <button onClick={() => changeDone(item.id)}>done</button>
          </li>
        )}
      </ul>
      <button onClick={() => {
        console.log(ChildRef.current?.num)
        ChildRef.current?.changeNum(1)
      }}>获取子组件方法和数据</button>
      <hr />
      <Child ref={ChildRef}>
        <p>121121</p>
      </Child>
    </div>
  )
}

export default App