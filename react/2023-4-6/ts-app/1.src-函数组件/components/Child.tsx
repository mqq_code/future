import React, { useState, forwardRef, useImperativeHandle } from 'react'

interface IProps {
  children?: React.ReactElement;
}

export interface IRef {
  num: number;
  changeNum: (n: number) => void;
  a: number;
}

const Child: React.ForwardRefRenderFunction<IRef, IProps> = (props, ref) => {
  const [num, setNum] = useState(0)
  const changeNum = (n: number) => {
    setNum(num + n)
  }
  useImperativeHandle(ref, () => {
    return {
      num,
      changeNum,
      a: 100
    }
  }, [num, changeNum])
  return (
    <div>
      <h2>Child</h2>
      <button onClick={() => changeNum(-1)}>-</button>
      {num}
      <button onClick={() => changeNum(1)}>+</button>
      <hr />
      {props.children}
    </div>
  )
}

export default forwardRef(Child)