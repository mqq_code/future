import React, { Component, createRef } from 'react'
import Child, { ITodoItem } from './components/Child'
import Child2 from './components/Child2'

interface IState {
  num: number;
  obj: { name: string; };
  list: ITodoItem[];
}

class App extends Component<{}, IState> {

  state = {
    num: 0,
    obj: {
      name: '小王'
    },
    list: [] as ITodoItem[]
  }
  inpRef = createRef<HTMLInputElement>()
  child2Ref = createRef<Child2>()

  changeNum = (num: number) => {
    this.setState({
      num: this.state.num + num
    })
  }
  changeName = (e: React.ChangeEvent<HTMLInputElement>) => {
    console.log(e.target.value)
    this.setState({
      obj: {...this.state.obj, name: e.target.value}
    })
  }
  handleClick = (e: React.MouseEvent<HTMLHeadingElement>) => {
    console.log((e.target as HTMLHeadingElement).innerHTML)
  }
  addTodo = () => {
    if (this.inpRef.current && this.inpRef.current.value.trim()) {
      this.setState({
        list: [...this.state.list, {
          id: Date.now(),
          text: this.inpRef.current.value,
          done: false
        }]
      })
      this.inpRef.current.value = ''
      this.inpRef.current.focus()
    }
  }
  keydown = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (e.keyCode === 13) {
      this.addTodo()
    }
  }
  changeDone = (id: number) => {
    this.setState({
      list: this.state.list.map(item => {
        if (item.id === id) {
          return {...item, done: !item.done}
        }
        return item
      })
    })
    // const list = [...this.state.list]
    // list.forEach(item => {
    //   if (item.id === id) {
    //     item.done = !item.done
    //   }
    // })
    // this.setState({ list })
  }
  remove = (id: number) => {
    this.setState({
      list: this.state.list.filter(v => v.id !== id)
    })
  }
  render() {
    const { num, obj, list } = this.state
    return (
      <div>
        <h1 onClick={this.handleClick}>App</h1>
        <button onClick={() => this.changeNum(-1)}>-</button>
        {num}
        <button onClick={() => this.changeNum(1)}>+</button>
        <hr />
        <input type="text" value={obj.name} onChange={this.changeName} />
        <hr />
        <input type="text" ref={this.inpRef} onKeyDown={this.keydown} />
        <button onClick={this.addTodo}>添加</button>
        <ul>
          {list.map(item =>
            <Child key={item.id} {...item} changeDone={this.changeDone} remove={this.remove} />
          )}
        </ul>
        <hr />
        <button onClick={() => {
          console.log(this.child2Ref.current)
          this.child2Ref.current?.changeNum(1)
        }}>获取child2实例对象</button>
        <Child2 ref={this.child2Ref} />
      </div>
    )
  }
}

export default App
