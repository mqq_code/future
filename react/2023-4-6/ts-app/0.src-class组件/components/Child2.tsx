import React, { Component } from 'react'

interface IState {
  num: number;
}

export default class Child2 extends Component<{}, IState> {

  state = {
    num: 0
  }

  changeNum = (num: number) => {
    this.setState({
      num: this.state.num + num
    })
  }

  render() {
    return (
      <div style={{ background: 'tomato' }}>
        <h2>Child2</h2>
        <button onClick={() => this.changeNum(-1)}>-</button>
        {this.state.num}
        <button onClick={() => this.changeNum(1)}>+</button>
      </div>
    )
  }
}
