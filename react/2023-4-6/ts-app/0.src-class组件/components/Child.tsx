import React, { Component } from 'react'

interface IProps {
  id: number;
  text: string;
  done: boolean;
  changeDone: (id: number) => void;
  remove: (id: number) => void;
}

export type ITodoItem = Pick<IProps, 'id' | 'done' | 'text'>
export type ITodoItem1 = Omit<IProps, 'changeDone' | 'remove'>


class Child extends Component<IProps> {
  render() {
    return (
      <li>
        {this.props.done ? <s>{this.props.text}</s> : this.props.text}
        <button onClick={() => this.props.changeDone(this.props.id)}>done</button>
        <button onClick={() => this.props.remove(this.props.id)}>del</button>
      </li>
    )
  }
}

export default Child
