import React, { useEffect, useState } from 'react'
import { RouteComponentProps, useHistory } from 'react-router-dom'
import { getBanner } from '../services'
import type { IBannerItem } from '../services'

interface IProps {
}

// RouteComponentProps<params类型, any, state参数类型>
const Home: React.FC<IProps & RouteComponentProps> = (props) => {
  const [list, setList] = useState<IBannerItem[]>([])
  const history = useHistory()

  const goDetial = () => {
    // props.history.push('/detail/1234')
    history.push('/detail/1234')
  }
  const goOther = () => {
    history.push({
      pathname: '/other',
      search: 'a=1&b=2',
      state: {
        'A': 'aaa',
        'B': 'bbb'
      }
    })
  }

  useEffect(() => {
    getBanner().then(res => {
      setList(res.data.banners)
    })
  }, [])

  return (
    <div>
      <h2>Home</h2>
      <button onClick={goDetial}>跳转详情页面</button>
      <button onClick={goOther}>跳转 other 页面</button>
      <ul>
        {list.map(item =>
          <li key={item.targetId}>
            <img src={item.imageUrl} width="300" alt="" />
            <p>{item.typeTitle}</p>
          </li>
        )}
      </ul>
    </div>
  )
}

export default Home