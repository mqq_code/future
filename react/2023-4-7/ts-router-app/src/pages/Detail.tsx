import React, { useRef } from 'react'
import { RouteComponentProps, useParams } from 'react-router-dom'
import { getSearchResult } from '../services'

interface IParams {
  id: string
}

// RouteComponentProps<params类型, any, state参数类型>
const Detail: React.FC<RouteComponentProps<IParams>> = (props) => {
  // console.log(props.match.params.id)
  const params = useParams<IParams>()
  const inpRef = useRef<HTMLInputElement>(null)
  const search = async () => {
    const res = await getSearchResult(inpRef.current!.value)
    console.log(res.data)
  }
  
  return (
    <div>
      <h2>详情</h2>
      <input type="text" ref={inpRef} />
      <button onClick={search}>搜索</button>
    </div>
  )
}

export default Detail

