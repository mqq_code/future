import React from 'react'
import { RouteComponentProps } from 'react-router-dom'

const query = <T,>(str: string): T => {
  const obj: { [key: string]: string } = {}
  const arr = str.slice(1).split('&')
  arr.forEach(item => {
    const [key, val] = item.split('=')
    obj[key] = val
  })
  return obj as T
}

type IQs = Record<'a' | 'b', string>
interface IRouteState {
  A: string;
  B: string;
}

const Other: React.FC<RouteComponentProps<any, any, IRouteState>> = (props) => {
  const qs = query<IQs>(props.location.search)
  // console.log(qs)

  // console.log((props.location.state as IRouteState).A)
  console.log(props.location.state.A)

  return (
    <div>Other</div>
  )
}

export default Other