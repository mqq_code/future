import axios, { AxiosResponse } from 'axios'
import { IBannerData, SearchData, IBannerItem } from './api'

// CommonJS ==> require module.exports
// es module ===>  import export

export const getBanner = () => {
  return axios.get<any, AxiosResponse<IBannerData>>('http://zyxcl.xyz/banner')
}


export const getSearchResult = (keywords: string) => {
  // AxiosResponse<后端接口返回值类型>: axios包装后的返回值类型
  return axios.get<any, AxiosResponse<SearchData>>('http://zyxcl.xyz/search', {
    params: { keywords }
  })
}

export type {
  IBannerData,
  SearchData,
  IBannerItem
}


// const fn = (): Promise<number> => {
//   return new Promise((resolve, reject) => {
//     resolve(100)
//   })
// }