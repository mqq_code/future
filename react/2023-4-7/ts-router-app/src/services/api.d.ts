
export interface IBannerItem {
  imageUrl: string;
  targetId: number;
  typeTitle: string;
}

export interface IBannerData {
  code: number;
  banners: IBannerItem[]
}

export interface ISearchResult {
  songCount: number;
  hasMore: boolean;
  songs: {
    name: string;
    id: number;
  }[];
  artists: {
    id: number;
    img1v1Url: string;
    name: string;
  }[]
}

export interface SearchData {
  code: number;
  result: ISearchResult
}
