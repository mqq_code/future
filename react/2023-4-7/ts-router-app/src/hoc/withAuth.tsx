import React from 'react'
import { Redirect, RouteComponentProps } from 'react-router-dom'

const withAuth = (Com: React.ComponentType<any>) => {

  const IsAuth = (props: RouteComponentProps) => {
    const token = localStorage.getItem('token')
    if (token) {
      return <Com {...props} />
    }
    return <Redirect to="/login" />
  }

  return IsAuth
}

export default withAuth