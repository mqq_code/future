import { IStore } from './type'

export const SET_BANNER = "SET_BANNER" as const
export const SET_NAME = "SET_NAME" as const

export const setBannerAction = (payload: IStore['list']) => {
  return {
    type: SET_BANNER,
    payload
  }
}

export const setNameAction = (payload: string) => {
  return {
    type: SET_NAME,
    payload
  }
}


export type ActionType = ReturnType<typeof setBannerAction> | ReturnType<typeof setNameAction>
