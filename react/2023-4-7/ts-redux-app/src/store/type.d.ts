import { IBannerItem } from '../services'

export interface IStore {
  list: IBannerItem[];
  name: string;
  age: number;
}