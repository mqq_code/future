import { IStore } from './type'
import { SET_BANNER, SET_NAME, ActionType } from './actions'

const init: IStore = {
  list: [],
  name: '小王',
  age: 20
}

const reducer = (state = init, action: ActionType) => {
  if (action.type === SET_BANNER) {
    return {...state, list: action.payload}
  } else if (action.type === SET_NAME) {
    return {...state, name: action.payload}
  }
  return state
}

export default reducer