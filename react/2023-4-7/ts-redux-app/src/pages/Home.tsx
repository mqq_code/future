import React, { useEffect } from 'react'
import { Link, RouteComponentProps } from 'react-router-dom'
import { connect } from 'react-redux'
import type { Dispatch } from 'redux'
import { IStore } from '../store/type'
import { setBannerAction, setNameAction } from '../store/actions'
import { getBanner } from '../services'


interface IProps extends RouteComponentProps {
  name: string;
  list: IStore['list'];
  dispatch: Dispatch
}

const Home: React.FC<IProps> = (props) => {

  useEffect(() => {
    getBanner().then(res => {
      props.dispatch(setBannerAction(res.data.banners))
    })
  }, [])

  const changeName = (e: React.ChangeEvent<HTMLInputElement>) => {
    props.dispatch(setNameAction(e.target.value))
  }

  return (
    <div>
      <h2>Home</h2>
      <input type="text" value={props.name} onChange={changeName} />
      <Link to="/other">other</Link>
      <ul>
        {props.list.map(item =>
          <li key={item.targetId}>
            <img src={item.imageUrl} width="300" alt="" />
          </li>
        )}
      </ul>
    </div>
  )
}

export default connect((state: IStore) => {
  return {
    name: state.name,
    list: state.list
  }
})(Home)