import axios, { AxiosResponse } from 'axios'
import { IBannerData, SearchData, IBannerItem } from './api'

axios.defaults.baseURL = 'http://zyxcl.xyz'

export const getBanner = () => {
  return axios.get<any, AxiosResponse<IBannerData>>('/banner')
}

export const getSearchResult = (keywords: string) => {
  return axios.get<any, AxiosResponse<SearchData>>('/search', {
    params: { keywords }
  })
}

export type {
  IBannerData,
  SearchData,
  IBannerItem
}
