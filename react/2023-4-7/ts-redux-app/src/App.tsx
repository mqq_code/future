import { Switch, Route, Redirect } from 'react-router-dom'
import Login from './pages/Login'
import Detail from './pages/Detail'
import Home from './pages/Home'
import Other from './pages/Other'
import withAuth from './hoc/withAuth'


const App = () => {
  return (
    <Switch>
      <Route exact path="/login" component={Login} />
      <Route exact path="/detail/:id" component={withAuth(Detail)} />
      <Route exact path="/other" component={Other} />
      <Route exact path="/" component={Home} />
      <Redirect to="/" />
    </Switch>
  )
}

export default App