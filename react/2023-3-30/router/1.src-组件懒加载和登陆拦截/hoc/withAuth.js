import { Redirect, useLocation } from 'react-router-dom'

const withAuth =  (Com) => {
  return props => {
    const location = useLocation()
    const token = localStorage.getItem("token")
    if (token) {
      return <Com {...props} />
    }

    console.log(location.pathname)
    return <Redirect to={`/login?redirectFrom=${encodeURIComponent(location.pathname)}`} />
  }
}
export default withAuth