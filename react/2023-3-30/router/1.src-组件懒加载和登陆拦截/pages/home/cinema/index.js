import React, { lazy, Suspense } from 'react'
// import Info from './Info'
// 异步组件，必须配合 Suspense 组件使用，必须在 Suspense 组件内
const Info = lazy(() => import(/* webpackChunkName: "info" */'./Info'))

const Cinema = () => {
  return (
    /* fallback: 加载内部的异步组件时展示的备选内容 */
    <div>
      <h2>Cinema</h2>
      <Suspense fallback={<div style={{ fontSize: 26 }}>loading...</div>}>
        <Info />
      </Suspense>
    </div>
  )
}

export default Cinema