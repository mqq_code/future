import React from 'react'
import { NavLink } from 'react-router-dom'

const Movie = (props) => {
  return (
    <div className="movie">
      <div className="topBar">
        <NavLink to="/home/movie/hot">正在热映</NavLink>
        <NavLink to="/home/movie/coming">即将上映</NavLink>
      </div>
      <div className="content">
        {props.children}
      </div>
    </div>
  )
}

export default Movie