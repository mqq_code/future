import React from 'react'

const query = str => {
  const obj = {}
  const arr = str.slice(1).split('&')
  arr.forEach(item => {
    const [key, value] = item.split('=')
    obj[key] = decodeURIComponent(value)
  })
  return obj
}

const Login = (props) => {
  const qs = query(props.location.search)
  const submit = () => {
    localStorage.setItem('token', 'token123')
    console.log(qs)
    props.history.replace(qs.redirectFrom || '/')
  }
  return (
    <div>
      <h1>Login</h1>
      <button onClick={submit}>登陆</button>
    </div>
  )
}

export default Login