import React from 'react'
import routes from './router'
import RouterView from './router/RouterView'


const App = () => {
  return (
    <div>
      <RouterView routes={routes} />
    </div>
  )
}

export default App

// const App = () => {
//   return (
//     <Switch>
//       <Route path="/home" render={routeInfo => (
//         <Home {...routeInfo}>
//           <Switch>
//             <Route path="/home/movie" render={routeInfo => (
//               <Movie {...routeInfo}>
//                 <Switch>
//                   <Route path="/home/movie/hot" component={Hot} />
//                   <Route path="/home/movie/coming" component={Coming} />
//                   <Redirect from="/home/movie" to="/home/movie/hot" />
//                 </Switch>
//               </Movie>
//             )} />
//             <Route path="/home/cinema" component={Cinema} />
//             <Route path="/home/mine" component={Mine} />
//             <Redirect from="/home" to="/home/movie" />
//           </Switch>
//         </Home>
//       )} />
//       <Route exact path="/detail/:id/:a" component={Detail} />
//       <Route exact path="/login" component={Login} />
//       <Route path="/404" component={NotFound} />
//       <Redirect exact from="/" to="/home" />
//       <Redirect to="/404" />
//     </Switch>
//   )
// }