import React, { useState, useEffect } from 'react'
import style from './hot.module.scss'
import axios from 'axios'
import Item from './Item'

const Hot = (props) => {
  const [list, setList] = useState([])
  const getList = async () => {
    const res = await axios.get('http://zyxcl.xyz/toplist/detail')
    console.log(res.data.list)
    setList(res.data.list)
  }

  useEffect(() => {
    getList()
  }, [])

  const goDetail = id => {
    // console.log(id)
    // 如果当前页面传给了 Route 组件，可以通过 props 获取路由信息和跳转路由
    console.log(props)
    props.history.push('/detail')
  }

  return (
    <div className={style.wrap}>
      {list.map(item =>
        <Item key={item.id} info={item} />
      )}
    </div>
  )
}

export default Hot