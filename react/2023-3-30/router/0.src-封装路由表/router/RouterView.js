import {
  Route, // 渲染路由视图
  Redirect, // 重定向组件
  Switch, // 只渲染匹配成功的第一个组件
} from 'react-router-dom'

const RouterView = (props) => {
  return (
    <Switch>
      {props.routes.map(item => {
        if (item.component) {
          return <Route key={item.path} exact={item.exact} path={item.path} render={routeInfo => {
            return <item.component {...routeInfo}>
              {item.children && <RouterView routes={item.children} />}
            </item.component>
          }} />
        }
        return <Redirect key={item.to} exact={item.exact} from={item.from} to={item.to} />
      })}
    </Switch>
  )
}
export default RouterView