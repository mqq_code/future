import Home from '../pages/home'
import Detail from '../pages/detail'
import Login from '../pages/login'
import NotFound from '../pages/404'
import Movie from '../pages/home/movie'
import Cinema from '../pages/home/cinema'
import Mine from '../pages/home/mine'
import Hot from '../pages/home/movie/hot'
import Coming from '../pages/home/movie/coming'

const routes = [
  {
    path: '/home',
    component: Home,
    children: [
      {
        path: '/home/movie',
        component: Movie,
        children: [
          {
            path: '/home/movie/hot',
            component: Hot
          },
          {
            path: '/home/movie/coming',
            component: Coming
          },
          {
            from: '/home/movie',
            to: '/home/movie/hot'
          }
        ]
      },
      {
        path: '/home/cinema',
        component: Cinema
      },
      {
        path: '/home/mine',
        component: Mine
      },
      {
        from: '/home',
        to: '/home/movie'
      }
    ]
  },
  {
    path: '/detail/:id/:a',
    component: Detail,
    exact: true
  },
  {
    path: '/login',
    component: Login,
    exact: true
  },
  {
    path: '/404',
    component: NotFound
  },
  {
    exact: true,
    from: '/',
    to: '/home'
  },
  {
    to: '/404'
  }
]

export default routes
