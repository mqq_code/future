import React, { useState, useEffect, useLayoutEffect } from 'react'
import style from '../home.module.scss'
import classNames from 'classnames'
import { NavLink, useLocation, useHistory } from 'react-router-dom'
import tabs from './tab'

const Side = () => {
  const [curIndex, setCurIndex] = useState(0)
  const [title, setTitle] = useState('')
  const location = useLocation()
  const history = useHistory()

  const changeTab = (index) => {
    setCurIndex(index)
    history.push(tabs[index].list[0].path)
  }
  
  useLayoutEffect(() => {
    tabs.forEach((item, index) => {
      item.list.forEach(val => {
        if (val.path === location.pathname) {
          setTitle(val.title)
          setCurIndex(index)
        }
      })
    })
  }, [location.pathname])

  return (
    <div className={style.side}>
      <div className={style.tab_nav}>
        {tabs.map((item, index) =>
          <div
            key={item.title}
            className={classNames(style.nav_item, { [style.active]: curIndex === index })}
            onClick={() => changeTab(index)}
          >
            {item.icon}
            <p>{item.title}</p>
          </div>
        )}
      </div>
      <div className={style.tabpane}>
        <h3>{title}</h3>
        <ul>
          {tabs[curIndex].list.map(item =>
            <li key={item.title}>
              <NavLink activeClassName={style.active} to={item.path}>{item.title}</NavLink>
            </li>
          )}
        </ul>
      </div>
    </div>
  )
}

export default Side