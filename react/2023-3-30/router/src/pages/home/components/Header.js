import React, { useEffect, useState } from 'react'
import style from '../home.module.scss'
import { NavLink, useLocation, useHistory } from 'react-router-dom'
import { list } from './tab'

const Header = () => {
  const [nav, setNav] = useState([])
  const location = useLocation()
  const history = useHistory()

  useEffect(() => {
    if (!nav.find(v => v.path === location.pathname)) {
      const item = list.find(v => v.path === location.pathname)
      setNav([...nav, item])
    }
  }, [location, nav])

  const remove = (e, index, path) => {
    e.preventDefault()
    if (nav.length === 1) {
      setNav([])
      history.push('/login')
      localStorage.setItem('token', '')
      return
    }
    if (path === location.pathname) {
      if (index === nav.length - 1) {
        history.push(nav[index - 1].path)
      } else {
        history.push(nav[index + 1].path)
      }
    }
    setNav(nav.filter(v => v.path !== path))
  }

  return (
    <div className={style.header}>
      <div className={style.info}>
        <button>退出</button>
      </div>
      <div className={style.nav}>
        {nav.map((item, index) =>
          <NavLink activeClassName={style.active} key={item.path} to={item.path}>
            {item.title}
            <span onClick={e => remove(e, index, item.path)}>x</span>
          </NavLink>
        )}
      </div>
    </div>
  )
}

export default Header