import React from 'react'
import style from './home.module.scss'
import Side from './components/Side'
import Header from './components/Header'

const Home = (props) => {
  return (
    <div className={style.home}>
      <Side />
      <div className={style.content}>
        <Header />
        <main>
          {props.children}
        </main>
      </div>
    </div>
  )
}

export default Home