import withAuth from '../hoc/withAuth'
import Home from '../pages/home/Home'
import One from '../pages/home/one/One'
import Two from '../pages/home/two/Two'
import Three from '../pages/home/three/Three'
import Four from '../pages/home/four/Four'
import Five from '../pages/home/five/Five'
import Login from '../pages/login/Login'
import NotFound from '../pages/404'

const routes = [
  {
    path: '/home',
    component: withAuth(Home),
    children: [
      {
        path: '/home/one',
        component: One
      },
      {
        path: '/home/two',
        component: Two
      },
      {
        path: '/home/three',
        component: Three
      },
      {
        path: '/home/four',
        component: Four
      },
      {
        path: '/home/five',
        component: Five
      },
      {
        from: '/home',
        to: '/home/one'
      }
    ]
  },
  {
    path: '/login',
    component: Login,
    exact: true
  },
  {
    path: '/404',
    component: NotFound
  },
  {
    exact: true,
    from: '/',
    to: '/home'
  },
  {
    to: '/404'
  }
]

export default routes
