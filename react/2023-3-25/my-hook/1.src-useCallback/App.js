import React, { useMemo, useState, useCallback } from 'react'

const data = [
  {
    title: '苹果',
    price: 10,
    count: 0
  },
  {
    title: '香蕉',
    price: 11,
    count: 0
  },
  {
    title: '梨',
    price: 10,
    count: 0
  }
]

const arr = []

const App = () => {
  const [title, setTitle] = useState('useCallback')
  const [list, setList] = useState(data)

  // useCallback: 两个参数，第一个参数是要缓存的函数，第二个参数是依赖项数组
  // 作用：返回一个缓存的函数，依赖项改变重新创建函数，没有改变从缓存中读取
  // const changeCount = useCallback((title, n) => {
  //   setList(list.map(item => {
  //     if (item.title === title) {
  //       return {...item, count: item.count + n}
  //     }
  //     return item
  //   }))
  // }, [list])

  // 可以使用useMemo实现useCallback的功能
  const changeCount = useMemo(() => {
    return (title, n) => {
      setList(list.map(item => {
        if (item.title === title) {
          return {...item, count: item.count + n}
        }
        return item
      }))
    }
  }, [list]) 

  if (!arr.includes(changeCount)) {
    arr.push(changeCount)
  }
  console.log(arr)

  const total = useMemo(() => {
    return list.reduce((prev, val) => prev + val.count * val.price, 0)
  }, [list])

  return (
    <div>
      <h1>{title}</h1>
      <input type="text" value={title} onChange={e => setTitle(e.target.value)} />
      <ul>
        {list.map(item =>
          <li key={item.title}>
            <h3>{item.title}</h3>
            <p>价格：¥{item.price}</p>
            <button disabled={item.count <= 0} onClick={() => changeCount(item.title, -1)}>-</button>
            {item.count}
            <button onClick={() => changeCount(item.title, 1)}>+</button>
          </li>
        )}
      </ul>
      <footer>总价：¥{total}</footer> 
    </div>
  )
}

export default App