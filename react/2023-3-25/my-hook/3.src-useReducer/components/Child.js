import React, { useState, memo } from 'react'

const Child = (props) => {
  const [flag, setFlag] = useState(true)
  console.log('child组件更新了')

  return (
    <div>
      <h2 style={flag ? { background: 'red' } : {}}>Child</h2>
      <button onClick={() => setFlag(!flag)}>添加背景颜色</button>
      <p>{props.num}</p>
      <button onClick={() => props.add(2)}>props.num + 2</button>
    </div>
  )
}

// memo: 优化函数组件性能的高阶组件
// 作用: 浅比较组件的props是否改变，props更新组件渲染，没有改变组件不渲染
export default memo(Child)

// 如果需要深层数据比较，可以传入第二个参数，是一个函数，返回true不更新组件，返回false更新组件
// export default memo(Child, (prevProps, nextProps) => {
//   // console.log('prevProps', prevProps)
//   // console.log('nextProps', nextProps)
//   // console.log(prevProps.add === nextProps.add)
//   if (prevProps.num !== nextProps.num) {
//     return false
//   }
//   return true
// })





// import React, { Component, PureComponent } from 'react'
// export default class Child extends PureComponent {
//   state = {
//     flag: true
//   }
//   // shouldComponentUpdate(nextProps, nextState) {
//   //   if (this.props.num !== nextProps.num || this.state.flag !== nextState.flag) {
//   //     return true
//   //   }
//   //   return false
//   // }
//   render() {
//     const { flag } = this.state
//     console.log('child组件更新了')
//     return (
//       <div>
//         <h2 style={flag ? { background: 'red' } : {}}>Child</h2>
//         <button onClick={() => this.setState({ flag: !flag })}>添加背景颜色</button>
//         <p>{this.props.num}</p>
//       </div>
//     )
//   }
// }
