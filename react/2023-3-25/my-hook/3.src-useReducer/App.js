import React, { useMemo, useState, useReducer } from 'react'
import Child from './components/Child'

const reducer = (state, action) => {
  console.log(state, action)
  if (action.type === 'changeName') {
    return {...state, name: action.payload}
  } else if (action.type === 'addAge') {
    return {...state, age: state.age + action.payload}
  } else if (action.type === 'subAge') {
    return {...state, age: state.age - action.payload}
  } else if (action.type === 'changeSex') {
    return {...state, sex: action.payload}
  }
  return state
}
const init = {
  name: '小明',
  age: 20,
  sex: '男'
}
const App = () => {
  // 当 state 逻辑较复杂且包含多个子值，或者下一个 state 依赖于之前的 state 可以使用 useReducer 代替 useState
  // const [user, setUser] = useState({
  //   name: '小明',
  //   age: 20,
  //   sex: '男'
  // })

  // const changeName = () => {
  //   setUser({
  //     ...user,
  //     name: '小黑'
  //   })
  // }
  // const changeAge = () => {
  //   setUser({
  //     ...user,
  //     age: 200
  //   })
  // }
  // const changeSex = () => {
  //   setUser({
  //     ...user,
  //     sex: '女'
  //   })
  // }

  
  const [user, dispatch] = useReducer(reducer, init)

  return (
    <div>
      <h1>useReducer</h1>
      <p>{JSON.stringify(user)}</p>
      <div>
        <button onClick={() => dispatch({ type: 'changeName', payload: 'AAA' })}>修改姓名</button>
        <button onClick={() => dispatch({ type: 'addAge', payload: 1 })}>年龄+1</button>
        <button onClick={() => dispatch({ type: 'subAge', payload: 10 })}>年龄-10</button>
        <button onClick={() => dispatch({ type: 'changeSex', payload: '女' })}>修改性别</button>
      </div>
    </div>
  )
}

export default App
