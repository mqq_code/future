import React, { useMemo, useState, useCallback } from 'react'
import Child from './components/Child'

const App = () => {
  const [title, setTitle] = useState('useCallback')
  const [num, setNum] = useState(0)

  // useCallback配合memo高阶组件优化子组件性能
  const add = useCallback(n => {
    setNum(num => num + n)
  }, [])

  console.log('app更新了')

  return (
    <div>
      <h1>{title}</h1>
      <input type="text" value={title} onChange={e => setTitle(e.target.value)} />
      <div>
        <button onClick={() => add(1)}>num+</button>{num}
      </div>
      <Child num={num} add={add} />
    </div>
  )
}

export default App
