import React, { useEffect, useState, useReducer, useLayoutEffect } from 'react'


const App = () => {
 
  const [str, setStr] = useState('0')

  // 浏览器绘制完成执行，如果在useEffect中改变数据可能会造成页面闪烁
  // useEffect(() => {
    // console.log('useEffect 页面渲染完成', document.querySelector('p').innerHTML)
    // if (str === '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!') {
    //   setStr('AAAAAAA')
    // }
  // }, [str])

  // dom更新后，浏览器绘制之前执行
  useLayoutEffect(() => {
    console.log('useLayoutEffect 页面渲染完成', document.querySelector('p').innerHTML)
    if (str === '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!') {
      setStr('AAAAAAA')
    }
  }, [str])

  return (
    <div>
      <h1>useLayoutEffect</h1>
      <p>{str}</p>
      <button onClick={() => setStr('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')}>修改字符串</button>
    </div>
  )
}

export default App
