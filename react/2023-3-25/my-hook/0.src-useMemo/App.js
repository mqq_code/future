import React, { useMemo, useState } from 'react'

const data = [
  {
    title: '苹果',
    price: 10,
    count: 0
  },
  {
    title: '香蕉',
    price: 11,
    count: 0
  },
  {
    title: '梨',
    price: 10,
    count: 0
  }
]
const App = () => {
  const [title, setTitle] = useState('useMemo')
  const [list, setList] = useState(data)
  const changeCount = (title, n) => {
    setList(list.map(item => {
      if (item.title === title) {
        return {...item, count: item.count + n}
      }
      return item
    }))
  }
  // const getTotal = () => {
  //   return list.reduce((prev, val) => {
  //     console.log(val)
  //     return prev + val.count * val.price
  //   }, 0)
  // }
  // useMemo: 两个参数，第一个是要是执行的函数需要有一个返回值，第二个是依赖项数组
  // 作用：返回一个缓存的值，依赖项发生改变自动执行函数重新计算结果，依赖项没有发生改变就从缓存中读取数据
  const total = useMemo(() => {
    return list.reduce((prev, val) => {
      console.log(val)
      return prev + val.count * val.price
    }, 0)
  }, [list])

  return (
    <div>
      <h1>{title}</h1>
      <input type="text" value={title} onChange={e => setTitle(e.target.value)} />
      <ul>
        {list.map(item =>
          <li key={item.title}>
            <h3>{item.title}</h3>
            <p>价格：¥{item.price}</p>
            <button disabled={item.count <= 0} onClick={() => changeCount(item.title, -1)}>-</button>
            {item.count}
            <button onClick={() => changeCount(item.title, 1)}>+</button>
          </li>
        )}
      </ul>
      {/* <footer>总价：¥{getTotal()}</footer> */}
      <footer>总价：¥{total}</footer> 
    </div>
  )
}

export default App