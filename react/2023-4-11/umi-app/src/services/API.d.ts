
declare module API {
  export interface IBannerRes {
    code: number;
    banners: {
      imageUrl: string;
      targetId: number;
    }[];
  }
  
  export interface ISearchRes {
    code: number;
    result: {
      hasMore: boolean;
      songCount: number;
      songs: {
        id: number;
        name: string;
      }[];
    }
  }
}

