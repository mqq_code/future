import { request } from 'umi'

export const getBanner = () => {
  return request<API.IBannerRes>('https://zyxcl.xyz/banner', {
    method: 'post',
    data: {
      a: 100,
      b: 200
    }
  })
}

export const getSearchApi = (keywords: string) => {
  return request<API.ISearchRes>('https://zyxcl.xyz/search', {
    method: 'post',
    data: {
      keywords
    }
  })
}
