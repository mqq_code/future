import { RequestConfig, IRoute } from 'umi';
import { RequestOptionsInit } from 'umi-request'

const request1 = (url: string, options: RequestOptionsInit) => {
  // 请求拦截，在所有接口发送请求前执行，可以添加公共参数
  return {
    url: `${url}`,
    options: { ...options },
  };
}


export const request: RequestConfig = {
  timeout: 1000,
  errorConfig: {},
  middlewares: [],
  requestInterceptors: [request1], // 请求拦截器，所有通过useRequest发送的请求都会先执行此数组中的函数
  responseInterceptors: [(response, options) => {
    // 添加响应拦截器，可以处理公共错误(例如：返回401跳转登陆，403跳转403页面)
    console.log('响应拦截器', response)
    console.log('options', options)
    return response;
  }],
};


export function patchRoutes({ routes }: { routes: IRoute }) {
  console.log(routes)
  routes.unshift({
    path: '/foo',
    exact: true,
    component: require('@/pages/foo').default,
  });
}

export function render(oldRender: any) {
  // 可以在渲染之前调用接口
  oldRender()
  // fetch('/api/auth').then(auth => {
  //   if (auth.isLogin) { oldRender() }
  //   else { 
  //     history.push('/login'); 
  //     oldRender()
  //   }
  // });
}
