import React from 'react'
import { Button, Input, Spin } from 'antd'
import { getSearchApi } from '@/services'
import { useRequest } from 'umi'

const city = () => {

  const { data, loading, run, cancel } = useRequest(getSearchApi, {
    manual: true, // 让接口手动执行
    loadingDelay: 300
  })

  console.log(data)


  return (
    <div>
      <Input type="text" placeholder='歌手/歌名' />
      <Button type="primary" onClick={() => {
        run('刘德华')
      }}>搜索</Button>
      <Button onClick={cancel}>取消搜索</Button>
      <hr />
      <Spin spinning={loading}>
        {data?.result.songs.map(item =>
          <div key={item.id}>{item.name}</div>
        )}
      </Spin>
    </div>
  )
}

export default city