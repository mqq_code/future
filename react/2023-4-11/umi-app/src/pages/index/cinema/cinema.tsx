import React, { useEffect, useState } from 'react'
import { Spin } from 'antd'
import { useRequest } from 'umi'
import { getBanner } from '@/services'

const cinema: React.FC = () => {

  const { data, loading } = useRequest(getBanner)
 
  return (
    <Spin spinning={loading}>
      <div style={{ width: '100%', height: '100%' }}>
        <h2>cinema</h2>
        <ul>
          {data?.banners.map(item =>
            <li key={item.targetId}>
              <img src={item.imageUrl} width="300" alt="" />
            </li>
          )}
        </ul>
      </div>
    </Spin>
  )
}

export default cinema