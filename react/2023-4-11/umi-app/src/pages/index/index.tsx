import React, { useDebugValue, useEffect } from 'react'
import style from './index.less'
import { NavLink, request } from 'umi'

const IndexPage: React.FC = (props) => {
  return (
    <div className={style.home}>
      <main>{props.children}</main>
      <footer>
        <NavLink exact activeClassName={style.active} to="/">电影</NavLink>
        <NavLink exact activeClassName={style.active} to="/cinema">影院</NavLink>
        <NavLink exact activeClassName={style.active} to="/mine">我的</NavLink>
      </footer>
    </div>
  )
}

export default IndexPage