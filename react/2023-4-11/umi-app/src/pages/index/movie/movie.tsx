import React from 'react'
import { RouteComponentProps } from 'react-router-dom'
import { history, useHistory, useLocation } from 'umi'
import Test from './components/Test'

const movie: React.FC<RouteComponentProps> = (props) => {
  const goDetail = () => {
    history.push('/detail/100')
  }
  return (
    <div>
      <h2>movie</h2>
      <button onClick={goDetail}>跳转到详情</button>
      <Test />
    </div>
  )
}

export default movie