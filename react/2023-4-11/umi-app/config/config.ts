import { defineConfig } from 'umi';
import routes from './routes';
export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  favicon: 'https://m.maizuo.com/favicon.ico',
  history: { type: 'hash' }, // 切换路由模式
  routes,
  fastRefresh: {},
  request: {
    dataField: '',
  },
});
