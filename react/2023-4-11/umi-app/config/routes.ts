export default [
  { exact: true, path: '/login', component: '@/pages/login/login', title: '登陆' },
  { exact: true, path: '/detail/:id', component: '@/pages/detail/detail', title: '详情', wrappers: ['@/wrappers/auth'] },
  { exact: true, path: '/city', component: '@/pages/city/city', title: '城市' },
  { exact: true, path: '/404', component: '@/pages/404', title: '404' },
  {
    path: '/',
    component: '@/pages/index/index',
    routes: [
      { exact: true, path: '/', component: '@/pages/index/movie/movie', title: '电影' },
      { exact: true, path: '/cinema', component: '@/pages/index/cinema/cinema', title: '影院' },
      { exact: true, path: '/mine', component: '@/pages/index/mine/mine', title: '我的' },
      { redirect: '/404' }
    ],
    wrappers: ['@/wrappers/auth']
  }
]