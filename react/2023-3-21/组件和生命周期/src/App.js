import React, { Component } from 'react'
import './app.scss'

class Child extends Component {

  state = {
    count: this.props.num
  }

  // 当state的值始终依赖于props时使用
  static getDerivedStateFromProps(nextProps) {
    console.log('最新的props', nextProps)
    // 返回的对象会合并到state中
    return {
      count: nextProps.num
    }
  }

  render() {
    return (
      <div className='bg'>
        <h3>Child组件</h3>
        <p>count: {this.state.count}</p>
      </div>
    )
  }
}



export default class App extends Component {
  state = {
    num: 15,
    list: [
      {
        id: 1,
        text: '11111'
      },
      {
        id: 2,
        text: '222222'
      },
      {
        id: 3,
        text: '33333'
      }
    ]
  }
  listRef = React.createRef()
  keyDown = e => {
    if (e.keyCode === 13) {
      this.setState({
        list: [...this.state.list, {
          id: Date.now(),
          text: e.target.value
        }]
      })
      e.target.value = ''
    }
  }

  getSnapshotBeforeUpdate () {
    const { scrollTop, offsetHeight, scrollHeight } = this.listRef.current
    return {
      isFloor: scrollTop + offsetHeight >= scrollHeight,
      scrollTop
    }
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    // 判断更新之前列表是否显示最后一条数据
    if (snapshot.isFloor) {
      const last = this.listRef.current.children.length - 1
      this.listRef.current.scrollTop += this.listRef.current.children[last].offsetHeight
    }
  }


  render() {
    const { list } = this.state
    return (
      <div>
        <h1>App</h1>
        <button onClick={() => this.setState({ num: this.state.num + 1 })}>{this.state.num }</button>
        <Child num={this.state.num} />
        <hr />
        <input type="text" placeholder="add item..." onKeyDown={this.keyDown} />
        <ul className="list" ref={this.listRef}>
          {list.map(item => <li key={item.id}>{item.text}</li>)}
        </ul>
      </div>
    )
  }
}
