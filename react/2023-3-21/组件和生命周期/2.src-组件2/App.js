import React, { Component, createRef } from 'react'
import './app.scss'
import Goods from './components/Goods'

class App extends Component {

  state = {
    obj: {
      name: '苹果',
      price: 10,
      desc: '特价处理'
    }
  }
  // 实例属性改变页面不会触发更新，存和组件更新无关的数据
  count = 100
  addCount = () => {
    this.count++ 
    console.log(this.count)
    this.forceUpdate() // 强制更新组件，尽量少用
  }

  goodsRef = createRef()
  add = () => {
    // 使用ref获取子组件实例对象，可以调用子组件的数据和方法
    console.log(this.goodsRef.current)
    this.goodsRef.current.changeCount(1)
  }

  render() {
    return (
      <div>
        <h1 onClick={this.addCount}>组件传值 - {this.count}</h1>
        <button onClick={this.add}>+</button>
        {/* 把对象的所有属性依次传给子组件 */}
        <Goods {...this.state.obj} ref={this.goodsRef} />
        {/* <Goods
          name={this.state.obj.name}
          price={this.state.obj.price}
          desc={this.state.obj.desc}
          ref={this.goodsRef}
        /> */}
      </div>
    )
  }
}

export default App
