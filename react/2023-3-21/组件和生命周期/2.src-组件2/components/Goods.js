import React, { Component } from 'react'

export default class Goods extends Component {
  state = {
    num: 0
  }
  changeCount = (n) => {
    this.setState({
      num: this.state.num + n
    })
  }
  render() {
    console.log(this.props)
    return (
      <div className="goods">
        <h2>goods组件</h2>
        <h3>{this.props.name}</h3>
        <p>价格：${this.props.price}</p>
        <p>{this.props.desc}</p>
        <button onClick={() => this.changeCount(-1)}>-</button>
        {this.state.num}
        <button onClick={() => this.changeCount(1)}>+</button>
      </div>
    )
  }
}
