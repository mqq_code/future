import React, { Component, createRef } from 'react'


class Count extends Component {
  state = {
    n: 0
  }
  timer = null
  componentDidMount() {
    this.timer = setInterval(() => {
      this.setState({
        n: this.state.n + 1
      })
      console.log('定时器', this.state.n)
    }, 1000)
  }
  // 组件销毁之前，可以清除异步任务：定时器、原生事件、请求接口
  componentWillUnmount() {
    console.log('组件销毁之前')
    clearInterval(this.timer)
  }
  render() {
    console.log('count组件在更新')
    return <div>定时器: {this.state.n}</div>
  }
}

class Child extends Component {
  state = {
    n: this.props.num
  }
  // props更新时执行，可以获取到最新的props
  componentWillReceiveProps(nextProps) {
    console.log('%c最新的props', 'font-size: 40px; color: red', nextProps)
    this.setState({
      n: nextProps.num
    })
  }
  render() {
    return <div>
      <h2>child组件 - {this.state.n}</h2>
    </div>
  }
}

class App extends Component {
  // 初始化 props 和 state
  constructor(props) {
    super(props)
    this.state = {
      num: 0,
      title: 'AAA'
    }
    console.log('初始化阶段 constructor',  this.appRef)
  }
  appRef = createRef()
  // 挂载之前
  componentWillMount() {
    console.log('componentWillMount 组件挂载之前 不可以操作dom', this.appRef)
  }
  // 挂在之后，可以操作dom、调接口
  componentDidMount() {
    console.log('componentDidMount', this.appRef.current)
  }

  // props 或者 state 改变会触发更新阶段
  // 判断组件是否需要更新，可以阻止组件不必要的更新，优化组件性能
  shouldComponentUpdate(nextProps, nextState) {
    console.log('shouldComponentUpdate', nextProps, nextState)
    // true：更新， false：不更新
    // if (this.state.num !== nextState.num) {
    //   return true
    // }
    return true
  }
  // 更新前
  componentWillUpdate() {
    console.log('componentWillUpdate' , this.state.num, document.querySelector('button').innerHTML)
  }
  // 更新后
  componentDidUpdate() {
    console.log('componentDidUpdate' , this.state.num, document.querySelector('button').innerHTML)
  }

  // 挂载阶段/更新阶段：开始渲染
  render() {
    console.log('render')
    return (
      <div className='app' ref={this.appRef}>
        <h2>{this.state.title}</h2>
        <input type="text" value={this.state.title} onChange={e => {
          this.setState({
            title: e.target.value
          })
        }} />
        <button onClick={() => {
          this.setState({
            num: this.state.num + 1
          })
        }}>{this.state.num}</button>
        <hr />
        {this.state.num > 50 && <Count />}
        <hr />
        <Child num={this.state.num} />
      </div>
    )
  }
}

export default App

