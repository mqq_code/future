import React, { Component } from 'react'
// 使用 css module 让样式只在组件内生效
import style from './content.module.scss'

export default class Content extends Component {
  render() {
    const { title, songs } = this.props
    return (
      <div className={style.content}>
        <h3>歌手: {title}</h3>
        <ul className='bg'>
          {songs.map(item =>
            <li key={item.name}>{item.name} -- {item.playCount}</li>
          )}
        </ul>
      </div>
    )
  }
}
