import React, { Component } from 'react'
import classNames from 'classnames'
import style from './nav.module.scss'

export default class Nav extends Component {
  render() {
    const { list, curIndex, change } = this.props
    return (
      <nav className={style.nav}>
        {list.map((item, index) =>
          // className={['nav-item', curIndex === index ? 'active' : ''].join(' ')}
          <span
            className={classNames(style.nav_item, { [style.active]: curIndex === index })}
            key={item.title}
            onClick={() => change(index)}
          >{item.title}</span>
        )}
      </nav>
    )
  }
}
