import React, { Component } from 'react'
import './app.scss'
import Nav from './components/nav/Nav'
import Content from './components/content/Content'

class App extends Component {

  state = {
    list: [
      {
        title: '许嵩',
        songs: [
          {
            name: '有何不可',
            playCount: 999
          },
          {
            name: '断桥残雪',
            playCount: 123
          },
          {
            name: '千百度',
            playCount: 456
          },
          {
            name: '多余的解释',
            playCount: 1234
          }
        ]
      },
      {
        title: '徐良',
        songs: [
          {
            name: '和平分手',
            playCount: 984
          },
          {
            name: '犯贱',
            playCount: 3214
          },
          {
            name: '红妆',
            playCount: 88
          },
          {
            name: '客官不可以',
            playCount: 222
          }
        ]
      },
      {
        title: '汪苏泷',
        songs: [
          {
            name: '小星星',
            playCount: 777
          },
          {
            name: '不分手的恋爱',
            playCount: 888
          },
          {
            name: '三国杀',
            playCount: 666
          },
          {
            name: '有点甜',
            playCount: 345
          }
        ]
      }
    ],
    curIndex: 0
  }
  change = index => {
    this.setState({
      curIndex: index
    })
  }
  render() {
    const { list, curIndex } = this.state
    return (
      <div>
        <Nav list={list} curIndex={curIndex} change={this.change} />
        <Content title={list[curIndex].title} songs={list[curIndex].songs} />
        <hr />
        <div className="content">
          <ul>
            <li>111111</li>
            <li>222222</li>
          </ul>
        </div>
      </div>
    )
  }
}

export default App
