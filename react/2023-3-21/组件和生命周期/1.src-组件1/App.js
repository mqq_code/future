import React, { Component } from 'react'
import Dialog from './components/Dialog'
import './app.scss'


class App extends Component {

  state = {
    visible: false
  }

  hide = () => {
    this.setState({
      visible: false
    })
  }

  handleClick = (type) => {
    console.log('点击按钮', type)
  }

  render() {
    return (
      <div>
        <h1>组件传值</h1>
        <button onClick={() => this.setState({ visible: true })}>显示弹窗</button>
        {this.state.visible &&
          <Dialog
            header={<div>头部</div>}
            footer={<footer>底部</footer>}
            title="弹窗标题"
            onClose={this.hide}
            onClick={this.handleClick}
          >
            {/* <ul>
              <li>1</li>
              <li>2</li>
            </ul> */}
            <img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9Ii0xMS41IC0xMC4yMzE3NCAyMyAyMC40NjM0OCI+CiAgPHRpdGxlPlJlYWN0IExvZ288L3RpdGxlPgogIDxjaXJjbGUgY3g9IjAiIGN5PSIwIiByPSIyLjA1IiBmaWxsPSIjNjFkYWZiIi8+CiAgPGcgc3Ryb2tlPSIjNjFkYWZiIiBzdHJva2Utd2lkdGg9IjEiIGZpbGw9Im5vbmUiPgogICAgPGVsbGlwc2Ugcng9IjExIiByeT0iNC4yIi8+CiAgICA8ZWxsaXBzZSByeD0iMTEiIHJ5PSI0LjIiIHRyYW5zZm9ybT0icm90YXRlKDYwKSIvPgogICAgPGVsbGlwc2Ugcng9IjExIiByeT0iNC4yIiB0cmFuc2Zvcm09InJvdGF0ZSgxMjApIi8+CiAgPC9nPgo8L3N2Zz4K" height="40" />
          </Dialog>
        }
      </div>
    )
  }
}

export default App
