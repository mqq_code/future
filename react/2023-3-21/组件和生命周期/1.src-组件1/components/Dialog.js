import React, { Component, Children } from 'react'

console.log(Children)

export default class Dialog extends Component {

  // props默认值
  static defaultProps = {
    showClose: true
  }

  render() {
    const { header, footer, title, showClose, onClose, onClick } = this.props
    // this.props.children 类型不确定：undefined、对象、数组
    // console.log(this.props.children)
    console.log(Children.count(this.props.children))
    Children.forEach(this.props.children, (item) => {
      console.log(item)
    })
    console.log(Children.toArray(this.props.children))
    console.log(Children.only(this.props.children)) // 限制组件只能有一个子元素
    return (
      <div className="dialog" onClick={onClose}>
        <div className="dialog-content" onClick={e => e.stopPropagation()}>
          <div className="dialog-header">
            {header ? header : <b>{title}</b>}
            {showClose && <span onClick={onClose}>x</span>}
          </div>
          <div className="dialog-center">
            {Children.map(this.props.children, item => <div className="red">{item}</div>)}
          </div>
          <div className="dialog-footer">
            {footer ? footer : 
              <>
                <button onClick={() => onClick('cancel')}>取消</button>
                <button onClick={() => onClick('ok')}>确定</button>
              </>
            }
          </div>
        </div>
      </div>
    )
  }
}
