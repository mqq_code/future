import { defineConfig } from 'umi';

export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  favicon: 'https://m.maizuo.com/favicon.ico',
  history: { type: 'hash' }, // 切换路由模式
  fastRefresh: {},
  // mock: false,
  antd: {
    dark: false,
    compact: true
  },
  dva: {
    immer: true, // 可以直接修改 reducer 中的 state
    hmr: false, // 热更新
  },
  theme: {
    '@primary-color': '#52c41a',
    // #52c41a
  }
});
