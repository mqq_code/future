import React from 'react'
import style from '../index.less'
import { NavLink, IRouteComponentProps, Redirect } from 'umi'

const HomeWrap: React.FC<IRouteComponentProps> = (props) => {
  return (
    <div className={style.home}>
      <main>{props.children}</main>
      <footer>
        <NavLink exact activeClassName={style.active} to="/">电影</NavLink>
        <NavLink exact activeClassName={style.active} to="/cinema/123">影院</NavLink>
        <NavLink exact activeClassName={style.active} to="/mine">我的</NavLink>
      </footer>
    </div>
  )
}

export default HomeWrap