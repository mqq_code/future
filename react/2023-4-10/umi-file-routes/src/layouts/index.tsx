import React from 'react'
import { IRouteComponentProps } from 'umi'
import HomeWrap from './compoents/HomeWrap'

const homeChild = /^\/((cinema\/\w+)|mine)?$/

const index: React.FC<IRouteComponentProps> = (props) => {

  // 添加重定向
  // if (props.location.pathname === '/') {
  //   return <Redirect to="/movie" />
  // }
  
  if (homeChild.test(props.location.pathname!)) {
    return (
      <HomeWrap {...props}>{props.children}</HomeWrap>
    )
  }
  return <>{props.children}</>
}

export default index