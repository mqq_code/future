import axios from 'axios'


export const getListApi = () => {
  return axios.get('/list')
}