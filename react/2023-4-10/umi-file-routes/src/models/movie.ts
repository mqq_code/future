import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import { getListApi } from '@/services'

export interface MovieState {
  list: string[];
}

export interface MovieModel {
  namespace: 'movie';
  state: MovieState;
  reducers: {
    setList: ImmerReducer<MovieState>
  };
  effects: {
    getList: Effect;
  };
}

const IndexModel: MovieModel = {
  namespace: 'movie',

  state: {
    list: []
  },

  reducers: {
    setList(state, action) {
      state.list = action.payload
    }
  },
  // 处理异步
  effects: {
    *getList(action, { call, put }) {
      const res = yield call(getListApi)
      yield put({
        type: 'setList',
        payload: res.data.values
      })
    }
  },

};

export default IndexModel;