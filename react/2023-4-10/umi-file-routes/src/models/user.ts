import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';

export interface UserState {
  num: number;
  name: string;
}

export interface UserModel {
  namespace: 'user';
  state: UserState;
  reducers: {
    addNum: ImmerReducer<UserState>;
  };
}

const IndexModel: UserModel = {
  namespace: 'user',

  state: {
    num: 0,
    name: '小明',
  },

  reducers: {
    addNum(state, action) {
      state.num += action.payload
    },
  }
};

export default IndexModel;