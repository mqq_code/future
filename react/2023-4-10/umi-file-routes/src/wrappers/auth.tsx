import React from 'react'
import { Redirect } from 'umi'

const auth: React.FC = (props) => {
  const token = localStorage.getItem('token')
  if (token) {
    return <>{props.children}</>
  }
  return <Redirect to="/login" />
}

export default auth