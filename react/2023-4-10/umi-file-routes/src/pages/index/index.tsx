import React, { useEffect } from 'react'
import { Button, Spin } from 'antd'
import { connect, UserState, MovieState, Loading, IRouteComponentProps, ConnectProps } from 'umi'

interface Iprops {
  num: number;
  list: MovieState['list'];
  loading: boolean;
}

const Movie: React.FC<Iprops & IRouteComponentProps & ConnectProps> = (props) => {

  useEffect(() => {
    props.dispatch!({
      type: 'movie/getList'
    })
  }, [])
  if (props.loading) {
    return <Spin />
  }

  return (
    <div>
      <Button type="primary" onClick={() => {
        props.dispatch!({
          type: 'user/addNum',
          payload: 2
        })
      }}>+2</Button>{props.num}
      <br />
      {props.list}
    </div>
  )
}

const mapState = (state: { user: UserState, movie: MovieState, loading: Loading }) => {
  console.log(state.loading)
  return {
    num: state.user.num,
    list: state.movie.list,
    loading: state.loading.models.movie
  }
}
export default connect(mapState)(Movie)