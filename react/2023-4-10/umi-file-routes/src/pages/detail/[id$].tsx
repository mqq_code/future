import React from 'react'
import { RouteComponentProps } from 'react-router-dom'

interface IParams {
  id: string;
}

const Detail: React.FC<RouteComponentProps<IParams>> & { wrappers: string[] } = (props) => {
  console.log(props.match.params.id)
  return (
    <div>Detail</div>
  )
}

Detail.wrappers = ['@/wrappers/auth']

export default Detail