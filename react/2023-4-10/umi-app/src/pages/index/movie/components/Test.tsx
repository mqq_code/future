import React from 'react'
import { withRouter, Prompt } from 'umi'

const Test: React.FC = (props) => {
  console.log(props)
  return (
    <div>
      <h2>Test</h2>
      {/* 用户离开页面时提示一个选择 */}
      {/* <Prompt message="你确定要离开么？" /> */}
    </div>
  )
}

export default withRouter(Test)