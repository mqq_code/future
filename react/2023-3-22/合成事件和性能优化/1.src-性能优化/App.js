import React, { Component } from 'react'
import './App.css'
import Child from './components/Child'

class App extends Component {

  state = {
    num: 0,
    title: '标题'
  }

  changeTitle = e => {
    this.setState({
      title: e.target.value
    })
  }

  changeCount = n => {
    this.setState({
      num: this.state.num + n
    })
  }

  render() {
    const { num, title } = this.state
    console.log('app组件更新了')
    return (
      <div>
        <h1>{title}</h1>
        <input type="text" value={title} onChange={this.changeTitle} />
        <div className="count">
          <button onClick={() => this.changeCount(-1)}>-</button>
          {num}
          <button onClick={() => this.changeCount(1)}>+</button>
        </div>
        <Child num={num} title={title} />
      </div>
    )
  }
}


export default App