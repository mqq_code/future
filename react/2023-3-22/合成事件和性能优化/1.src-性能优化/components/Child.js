import React, { Component, PureComponent } from 'react'

// PureComponent: 内部使用 shouldComponentUpdate 在组件更新时对所有的 state 和 props 进行浅比较，无需在组件中手动定义 shouldComponentUpdate优化性能
export default class Child extends PureComponent {

  state = {
    show: true,
    obj: {
      a: 1,
      b: {
        c: 2
      }
    }
  }

  // shouldComponentUpdate(nextProps, nextState) {
  //   console.log(nextProps, nextState)
  //   console.log(this.props, this.state)
  //   if (this.state.show !== nextState.show || this.props.num !== nextProps.num) {
  //     return true
  //   }
  //   return false
  // }

  changeObjA = () => {
    this.setState({
      obj: {...this.state.obj, a: 22222}
    })
  }

  changeObjC = () => {
    // const obj = {...this.state.obj}
    // obj.b.c = 'CCCC'
    const obj = JSON.parse(JSON.stringify(this.state.obj))
    obj.b.c = 'CCCC'
    this.setState({ obj })
  }

  render() {
    const { show } = this.state
    console.log('child组件更新了')
    return (
      <div className="child">
        <button onClick={() => this.setState({ show: !show })}>显示隐藏标题</button>
        {show && <h2>Child</h2>}
        <hr />
        <p>父组件传过来的数据: {this.props.num}</p>
        <hr />
        <button onClick={this.changeObjA}>修改obj.a</button>
        <button onClick={this.changeObjC}>修改obj.b.c</button>
        {JSON.stringify(this.state.obj)}
      </div>
    )
  }
}


// const obj = {
//   a: 1,
//   b: {
//     c: 2
//   }
// }

// 浅拷贝：只拷贝第一层数据，引用类型的属性还是只向同一个地址
// const obj1 = {...obj}
// const obj1 = Object.assign({}, obj)

// 深拷贝：数据结构一致，不管层级多深，只向地址都不一样
// JSON.parse(JSON.stringify(obj)) 或者 递归
// const obj1 = JSON.parse(JSON.stringify(obj))

// obj1.a = 'AAAA'
// obj1.b.c = 'CCCCCC'

// console.log(obj1, obj)








