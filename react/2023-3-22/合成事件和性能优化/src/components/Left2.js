import React, { Component } from 'react'
import Left3 from './Left3'

export default class Left2 extends Component {
  render() {
    return (
      <div className="box">
        <h3>left2</h3>
        <Left3 num={this.props.num} add={this.props.add} />
      </div>
    )
  }
}
