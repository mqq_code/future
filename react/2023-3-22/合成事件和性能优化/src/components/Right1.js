import React, { Component } from 'react'
import Right2 from './Right2'
import { Provider } from '../context/color'

export default class Right1 extends Component {
  state = {
    color: '#333333'
  }
  changeColor = e => {
    this.setState({
      color: e.target.value
    })
  }

  render() {
    const { color } = this.state
    return (
      <Provider value={{
        color,
        changeColor: this.changeColor
      }}>
        <div className="box">
          <h2>right1</h2>
          <p>颜色: {color}</p>
            <input type="color" value={color} onChange={this.changeColor} />
          <Right2 />
        </div>
      </Provider>
    )
  }
}
