import React, { Component } from 'react'
import Left2 from './Left2'

export default class Left1 extends Component {
  render() {
    return (
      <div className="box">
        <h2>left1</h2>
        <Left2 num={this.props.num} add={this.props.add} />
      </div>
    )
  }
}
