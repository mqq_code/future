import React, { Component } from 'react'
import { Consumer } from '../context/num'

export default class Left3 extends Component {
  render() {
    return <Consumer>
      {value => {
        // value: Provider 传过来的数据
        return (
          <div className="box">
            <h4>left3</h4>
            <p>点击次数: {value.num}</p>
            <button onClick={() => value.add(2)}>App按钮点击次数加2</button>
          </div>
        )
      }}
    </Consumer>
    
  }
}
