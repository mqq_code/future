import React, { Component } from 'react'
import Right3 from './Right3'

export default class Right2 extends Component {
  render() {
    return (
      <div className="box">
        <h3>right2</h3>
        <Right3 />
      </div>
    )
  }
}
