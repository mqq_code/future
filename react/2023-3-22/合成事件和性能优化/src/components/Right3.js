import React, { Component } from 'react'
import { Consumer } from '../context/num'
import { Consumer as ColorConsumer } from '../context/color'

export default class Right3 extends Component {

  renderColor = ({ color }) => {
    return (
      <Consumer>
        {value => (
          <div className="box">
            <h4>right3</h4>
            <p>num: {value.num}</p>
            <p style={{ background: color }}>color: {color}</p>
          </div>
        )}
      </Consumer>
    )
  }

  // 无法阻止context值的更新
  shouldComponentUpdate() {
    return false
  }

  render() {
    console.log('right3组件更新了')
    return (
      <ColorConsumer>
        {this.renderColor}
      </ColorConsumer>
    )
    
  }
}
