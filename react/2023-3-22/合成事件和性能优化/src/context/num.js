import React from 'react'

const numCtx = React.createContext()

export const Provider = numCtx.Provider
export const Consumer = numCtx.Consumer
export default numCtx
