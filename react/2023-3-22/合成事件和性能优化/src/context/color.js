import React from 'react'

const colorCtx = React.createContext()

export const Provider = colorCtx.Provider
export const Consumer = colorCtx.Consumer
export default colorCtx
