import React, { Component } from 'react'
import './App.scss'
import Left1 from './components/Left1'
import Right1 from './components/Right1'
import numCtx from './context/num'

class App extends Component {

  state = {
    num: 0
  }
  add = (n) => {
    this.setState({
      num: this.state.num + n
    })
  }

  render() {
    const { num } = this.state
    return (
      <numCtx.Provider value={{
        num,
        add: this.add
      }}>
        <div className="app">
          <h1>Context</h1>
          <button onClick={() => this.add(1)}>按钮点击次数{num}</button>
          <main>
            <Left1 num={num} add={this.add} />
            <Right1 />
          </main>
        </div>
      </numCtx.Provider>
    )
  }
}


export default App