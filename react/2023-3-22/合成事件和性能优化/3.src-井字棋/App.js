import React, { Component } from 'react'
import './App.scss'
import Board from './components/Board'
import Step, { isWinner } from './components/Step'

class App extends Component {

  state = {
    history: [ // 下棋记录
      {
        id: 0,
        list: new Array(9).fill(null)
      }
    ],
    step: 0, // 第几步
    xIsNext: true // 下一个棋手
  }

  clickBoard = (index) => {
    const { history, step, xIsNext } = this.state
    const copyHistory = history.slice(0, step + 1) // 从当前的步数截取历史记录
    const curBoard = [...copyHistory[step].list] // 拷贝当前展示的棋盘
    if (curBoard[index] || isWinner(curBoard)) return // 如果当前位置有棋子或者有获胜者停止执行
    curBoard[index] = xIsNext ? 'x' : 'o' // 落子
    copyHistory.push({ // 添加到历史记录
      id: Date.now(),
      list: curBoard
    })
    this.setState({
      history: copyHistory,
      step: copyHistory.length - 1,
      xIsNext: !xIsNext
    })
  }

  changeStep = step => {
    this.setState({
      step,
      xIsNext: step % 2 === 0
    })
  }

  render() {
    const { history, step, xIsNext } = this.state
    // console.log(history)
    return (
      <div className="app">
        <Board list={history[step].list} onClick={this.clickBoard} />
        <Step history={history} step={step} xIsNext={xIsNext} onClick={this.changeStep} />
      </div>
    )
  }
}


export default App
