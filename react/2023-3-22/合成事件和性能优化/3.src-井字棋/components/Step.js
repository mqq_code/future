import React, { Component } from 'react'

const winList = [
  [0, 1, 2],
  [3, 4, 5],
  [6, 7, 8],
  [0, 3, 6],
  [1, 4, 7],
  [2, 5, 8],
  [0, 4, 8],
  [2, 4, 6]
]
export const isWinner = (list) => {
  for (let val of winList) {
    const [a, b, c] = val
    if (list[a] && list[a] === list[b] && list[a] === list[c]) {
      return list[a]
    }
  }
  return null
}

export default class Step extends Component {

  btnText = index => {
    return index === 0 ? '开始游戏' : `第${index}步`
  }

  render() {
    const { history, xIsNext, step, onClick } = this.props
    const winner = isWinner(history[step].list)
    return (
      <div>
        {winner ?
          <div>获胜者: {winner}</div>
        :
          <div>下一步：{xIsNext ? 'x' : 'o'}</div>
        }
        <ul>
          {history.map((item, index) =>
            <li key={item.id}>
              {index + 1}. <button onClick={() => onClick(index)}>{this.btnText(index)}</button>
            </li>
          )}
        </ul>
      </div>
    )
  }
}
