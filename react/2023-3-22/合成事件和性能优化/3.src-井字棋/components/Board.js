import React, { Component } from 'react'

export default class Board extends Component {
  render() {
    const { list, onClick } = this.props
    return (
      <div className="board">
        <ul>
          {list.map((item, index) =>
            <li key={index} onClick={() => onClick(index)}>{item}</li>
          )}
        </ul>
      </div>
    )
  }
}
