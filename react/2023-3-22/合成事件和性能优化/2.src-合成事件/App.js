import React, { Component } from 'react'
import './App.css'

class App extends Component {

  constructor(props) {
    super(props)
    this.sub = this.sub.bind(this)
  }

  state = {
    num: 0,
    title: '标题'
  }

  changeTitle = e => {
    this.setState({
      title: e.target.value
    })
  }

  sub() {
    this.setState({
      num: this.state.num - 1
    })
  }

  add = () => {
    this.setState({
      num: this.state.num + 1
    })
  }

  submit(e) {
    // react合成事件
    // react中的事件并没有直接绑定在 dom 元素上，利用事件冒泡的原理把所有元素的事件都绑定在 root 元素上(v18.0之前是绑定在document)，在内部实现了一套事件机制来模拟事件冒泡和捕获操作
    // e: react为了兼容所有浏览器处理后的合成事件对象
    // e.nativeEvent: 获取原生的事件对象
    console.log('冒泡提交', e)
  }
  submitCapture(e) {
    console.log('捕获提交', e)
  }

  render() {
    const { num, title } = this.state
    const button = React.createElement('button', { onClick: this.submit, onClickCapture: this.submitCapture } , ['提交'])

    return (
      <div>
        <h1>{title}</h1>
        <input type="text" value={title} onChange={this.changeTitle} />
        {button}
        <div className="count">
          {/* <button onClick={() => this.sub()}>-</button> */}
          <button onClick={this.sub}>-</button>
          {num}
          <button onClick={this.add}>+</button>
        </div>
      </div>
    )
  }
}


export default App


// const obj = {
//   num: 0,
//   fn() {
//     console.log(this)
//   }
// }
// let a = obj.fn
// 严格模式下全局的 this 指向 undefined， 普通模式下全局的 this 指向 Window 对象
// a()
document.querySelector('#root').addEventListener('click', (e) => {
  console.log(e)
})


