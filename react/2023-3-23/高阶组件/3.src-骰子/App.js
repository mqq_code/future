import React, { Component } from 'react'
import User from './components/user'
import Dice from './components/dice'
import Chip from './components/chip'
import Footer from './components/footer'

class App extends Component {

  state = {
    dices: [4, 5, 6],
    chipMoney: 0,
    userMoney: 1000,
    diff: 0
  }
  loading = false
  changeChip = (chipMoney) => {
    if (chipMoney > this.state.userMoney) chipMoney = this.state.userMoney
    this.setState({
      chipMoney
    })
  }
  // 摇骰子
  changeDices = () => {
    this.loading = true
    return new Promise((resolve, reject) => {
      let ms = 0
      const timer = setInterval(() => {
        ms += 100
        const dices = this.state.dices.map(() => Math.ceil(Math.random() * 6))
        this.setState({ dices })
        if (ms >= 3000) {
          clearInterval(timer)
          resolve(dices)
          this.loading = false
        }
      }, 100)
    })
  }
  // 计算骰子结果
  getDicesRes = (arr) => {
    if (new Set(arr).size === 1) {
      return '豹子'
    }
    const total = arr.reduce((prev, val) => prev + val)
    if (total >= 10) {
      return '大'
    }
    return '小'
  }
  // 开始摇骰子
  start = async (btnInfo) => {
    if (this.loading) return
    if (this.state.chipMoney === 0) {
      alert('请投币')
      return
    }
    // 开始摇骰子
    const dices = await this.changeDices()
    if (btnInfo.text === this.getDicesRes(dices)) {
      console.log('赢了')
      this.setState({
        userMoney: this.state.userMoney + this.state.chipMoney * btnInfo.scale,
        chipMoney: 0,
        diff: this.state.chipMoney * btnInfo.scale
      })
    } else {
      console.log('输了')
      this.setState({
        userMoney: this.state.userMoney - this.state.chipMoney,
        chipMoney: 0,
        diff: this.state.chipMoney * -1
      })
    }
  }
  // 清空diff
  clearDiff = () => {
    this.setState({ diff: 0 })
  }
  render() {
    const { dices, chipMoney, userMoney, diff } = this.state
    return (
      <div className="wrap">
        <User userMoney={userMoney} diff={diff} onClear={this.clearDiff} />
        <Dice dices={dices} />
        <Chip chipMoney={chipMoney} onChange={this.changeChip} />
        <Footer onClick={this.start} />
      </div>
    )
  }
}

export default App
