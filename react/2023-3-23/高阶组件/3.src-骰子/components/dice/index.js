import React, { Component } from 'react'
import style from './index.module.scss'

export default class Dice extends Component {
  render() {
    const { dices } = this.props
    return (
      <div className={style.wrap}>
        {dices.map((item, index) =>
          <p key={index} className={style['dice' + item]}>
            {new Array(item).fill(0).map((v, i) => <span key={i}></span>)}
          </p>
        )}
      </div>
    )
  }
}
