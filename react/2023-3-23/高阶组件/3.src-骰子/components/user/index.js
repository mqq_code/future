import React, { Component } from 'react'
import style from './index.module.scss'

export default class User extends Component {
  render() {
    const { userMoney, diff, onClear } = this.props
    return (
      <div className={style.user}>
        <div className={style.name}>姓名</div>
        <div className={style.money}>
          {diff !== 0 &&
            <span
              className={diff < 0 ? style.red : ''}
              onAnimationEnd={onClear}
            >{diff > 0 && '+'}{diff}</span>
          }
          {userMoney}
        </div>
      </div>
    )
  }
}
