import React, { Component } from 'react'
import style from './index.module.scss'

export default class Footer extends Component {
  btns = [
    {
      text: '大',
      scale: 1
    },
    {
      text: '豹子',
      scale: 24
    },
    {
      text: '小',
      scale: 1
    }
  ]
  render() {
    const { onClick } = this.props
    return (
      <div className={style.footer}>
        {this.btns.map(item =>
          <button key={item.text} onClick={() => onClick(item)}>
            <p>{item.text}</p>
            <p>1 : {item.scale}</p>
          </button>
        )}
      </div>
    )
  }
}
