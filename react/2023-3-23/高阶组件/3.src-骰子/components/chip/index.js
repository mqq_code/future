import React, { Component } from 'react'
import style from './index.module.scss'
import img1 from '../../assets/chip-100.png'

export default class Chip extends Component {
  chiplist = [
    {
      money: 50,
      img: require('../../assets/chip-50.png')
    },
    {
      money: 100,
      img: require('../../assets/chip-100.png')
    },
    {
      money: 200,
      img: require('../../assets/chip-200.png')
    },
    {
      money: 500,
      img: require('../../assets/chip-500.png')
    }
  ]
  render() {
    const { chipMoney, onChange } = this.props
    return (
      <div className={style.chip_wrap}>
        <div className={style.left}>
          {this.chiplist.map(item =>
            <img key={item.money} src={item.img} onClick={() => onChange(chipMoney + item.money)} />
          )}
        </div>
        <div className={style.right} onClick={() => onChange(0)}>
          {chipMoney > 0 ? <b>{chipMoney}</b> : '请下注'}
        </div>
      </div>
    )
  }
}
