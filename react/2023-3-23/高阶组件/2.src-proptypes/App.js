import React, { Component } from 'react'
import Child1 from './components/Child1'
import Child2 from './components/Child2'

class App extends Component {


  render() {
    return (
      <div>
        <h2>类型检查</h2>
        <Child1 title="child1标题" price={100} node={{ a: 100 }} />
        <Child2 />
      </div>
    )
  }
}

export default App
