import React, { Component } from 'react'

class Child2 extends Component {
 

  render() {
    return (
      <div className="box">
        <h2>Child2</h2>
        <table border="1" >
          <thead>
            <tr>
              <th>表头1</th>
              <th>表头2</th>
              <th>表头3</th>
              <th>表头4</th>
              <th>表头5</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>表格1</td>
              <td>表格2</td>
              <td>表格3</td>
              <td>表格4</td>
              <td>表格5</td>
            </tr>
          </tbody>
        </table>
      </div>
    )
  }
}

export default Child2
