import React, { Component } from 'react'
import PropTypes from 'prop-types';

class Child1 extends Component {
  render() {
    console.log(this.props.node)
    return (
      <div className="box">
        <h2>{this.props.title}</h2>
        <p>价格: ${this.props.price.toFixed(2)}</p>
        <ul>
          <li>1</li>
          <li>2</li>
          <li>3</li>
          <li>4</li>
          <li>5</li>
          <li>6</li>
          <li>7</li>
          <li>8</li>
          <li>9</li>
          <li>10</li>
        </ul>
      </div>
    )
  }
}

// 校验组件参数类型
Child1.propTypes = {
  title: PropTypes.string,
  price: PropTypes.number,
  node: PropTypes.node
}

// 使用高阶组件给Child1添加功能
export default Child1
