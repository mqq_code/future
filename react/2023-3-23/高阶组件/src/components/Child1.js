import React, { Component } from 'react'
import withPos from '../hoc/withPos'

class Child1 extends Component {
  render() {
    // 通过props获取高阶组件传过来的数据
    const { x, y } = this.props
    return (
      <div className="box">
        <h2>{this.props.title}</h2>
        <p>鼠标位置 x:{x}, y:{y}</p>
        <ul>
          <li>1</li>
          <li>2</li>
          <li>3</li>
          <li>4</li>
          <li>5</li>
          <li>6</li>
          <li>7</li>
          <li>8</li>
          <li>9</li>
          <li>10</li>
        </ul>
      </div>
    )
  }
}

// 使用高阶组件给Child1添加功能
export default withPos(Child1)
