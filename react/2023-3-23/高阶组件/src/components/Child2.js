import React, { Component, createRef } from 'react'
import withPos from '../hoc/withPos'

class Child2 extends Component {
  boxRef = createRef()
  rectInfo = {
    left: 0,
    top: 0
  }

  getPos = () => {
    let x = this.props.x - this.rectInfo.left
    let y = this.props.y - this.rectInfo.top
    if (x < 0) x = 0
    if (x > this.rectInfo.width - 300) x = this.rectInfo.width - 300
    if (y < 0) y = 0
    if (y > this.rectInfo.height - 100) y = this.rectInfo.height - 100
    return { x, y }
  }
  componentDidMount() {
    this.rectInfo = this.boxRef.current.getBoundingClientRect()
  }

  render() {
    const { x, y } = this.getPos()
    return (
      <div className="box" ref={this.boxRef}>
        <h2>Child2</h2>
        <table border="1" style={{ top: y, left: x }}>
          <thead>
            <tr>
              <th>表头1</th>
              <th>表头2</th>
              <th>表头3</th>
              <th>表头4</th>
              <th>表头5</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>表格1</td>
              <td>表格2</td>
              <td>表格3</td>
              <td>表格4</td>
              <td>表格5</td>
            </tr>
          </tbody>
        </table>
      </div>
    )
  }
}

export default withPos(Child2)
