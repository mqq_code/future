import React, { Component, createRef } from 'react'
import Child1 from './components/Child1'
import Child2 from './components/Child2'
import withPos from './hoc/withPos'

class App extends Component {

  child1Ref = createRef()

  componentDidMount() {
    console.log(this.child1Ref)
  }

  render() {
    return (
      <div>
        <h2>高阶组件 - {JSON.stringify(this.props)}</h2>
        <Child1 ref={this.child1Ref} title="child1标题" a="1000" />
        <Child2 />
      </div>
    )
  }
}

export default withPos(App)
