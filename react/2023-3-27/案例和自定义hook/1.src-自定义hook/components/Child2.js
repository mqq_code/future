import React, { useEffect, useState, useMemo } from 'react'
import { getBanner } from '../services'
import useRequest from '../hooks/useRequest'


const Child2 = () => {

  const [res, getList, loading] = useRequest(getBanner)

  const list = useMemo(() => {
    return res.banners || []
  }, [res])


  return (
    <div className='box'>
      <h2>chidl2</h2>
      <ul>
        {list.map(item =>
          <li key={item.targetId}>
            <img src={item.imageUrl} width="100" alt="" />
          </li>
        )}
      </ul>
      <button onClick={getList}>重试</button>
      {loading && <div className="loading">加载中...</div>}
    </div>
  )
}

export default Child2