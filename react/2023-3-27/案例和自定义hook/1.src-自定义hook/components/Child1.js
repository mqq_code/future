import React, { useEffect, useState } from 'react'
import { getListApi } from '../services'
import useRequest from '../hooks/useRequest'

const Child1 = () => {

  const [res, getList, loading] = useRequest(getListApi)


  return (
    <div className='box'>
      <ul>
        {res.values && res.values.map(item => <li key={item.id}>{item.keyTrigger}</li>)}
      </ul>
      <button onClick={getList}>重试</button>
      {loading && <div className="loading">加载中...</div>}
    </div>
  )
}

export default Child1