import React, { useEffect, useState, useMemo } from 'react'
import useRequest from '../hooks/useRequest'
import { searchMusic } from '../services'

const Child3 = () => {
  const [res, searchApi, loading] = useRequest(searchMusic, {}, false)

  

  const [text, setText] = useState('')
  const songs = useMemo(() => {
    return res.result && res.result.songs ? res.result.songs : []
  }, [res])

  return (
    <div className='box'>
      <h2>chidl3</h2>
      <input type="text" value={text} onChange={e => setText(e.target.value)} />
      <button onClick={() => searchApi(text)}>搜索</button>
      <ul>
        {songs.map(item => <li key={item.id}>{item.name}</li>)}
      </ul>
      {loading && <div className="loading">加载中...</div>}
    </div>
  )
}

export default Child3