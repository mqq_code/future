import React, { useState } from 'react'
import './App.scss'
import Child1 from './components/Child1'
import Child2 from './components/Child2'
import Child3 from './components/Child3'

// import withPos from './hoc/withPos' // 高阶组件处理公共逻辑让组件变得更加复杂且难以维护
import usePos from './hooks/usePos'
// 自定义hook处理公用逻辑，不改变原本组件嵌套结构
// 自定义 hook 是一个函数，名字必须以 use 开头，函数内部可以使用其他 hook
// 普通函数内不能使用 hook 函数



const App = () => {
  // const { pos, text } = usePos()

  return (
    <div>
      <h1>自定义hook</h1>
      {/* <h1>自定义hook - {JSON.stringify(pos)}</h1>
      <p>{text}</p> */}
      <main>
        <Child1 />
        <Child2 />
        <Child3 />
      </main>
    </div>
  )
}

export default App