import { useEffect, useState } from 'react'


const useRequest = (fn, params, immediate = true) => {

  const [res, setRes] = useState({})
  const [loading, setLoading] = useState(false)

  const getData = async (options) => {
    setLoading(true)
    const res = await fn(options)
    setRes(res.data)
    setLoading(false)
  }

  useEffect(() => {
    if (immediate) {
      getData(params)
    }
  }, [])

  return [res, getData, loading]
}

export default useRequest
