import { useState, useEffect, useMemo } from 'react'

const usePos = () => {
  const [pos, setPos] = useState({ x: null, y: null })
  const text = useMemo(() => {
    return `鼠标现在的位置: x => ${pos.x}, y => ${pos.y}`
  }, [pos])

  useEffect(() => {
    console.log('x改变了', pos.x)
  }, [pos.x])

  useEffect(() => {
    const mousemove = e => {
      setPos({
        x: e.pageX,
        y: e.pageY
      })
    }
    document.addEventListener('mousemove', mousemove)
    return () => {
      document.removeEventListener('mousemove', mousemove)
    }
  }, [])

  return { pos, text }
}

export default usePos