import React, { Component, forwardRef } from 'react'

// 高阶组件：参数为组件返回值为新组件的函数
function withPos(Com) {

  class PosCom extends Component {

    state = {
      x: null,
      y: null
    }
  
    mousemove = e => {
      this.setState({
        x: e.pageX,
        y: e.pageY
      })
    }
    componentDidMount() {
      document.addEventListener('mousemove', this.mousemove)
    }
    componentWillUnmount() {
      document.removeEventListener('mousemove', this.mousemove)
    }

    render() {
      const { x, y } = this.state
      const { posRef, ...otherProps } = this.props
      const newProps = {
        ...otherProps,
        x,
        y
      }
      return <Com {...newProps} ref={posRef} />
    }
  }

  // 通过 forwardRef 获取父组件传过来的ref对象
  return forwardRef((props, ref) => {
    return <PosCom {...props} posRef={ref} />
  })
}

export default withPos