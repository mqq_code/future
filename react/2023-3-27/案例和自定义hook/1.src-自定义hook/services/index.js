import axios from 'axios'

export const getListApi = () => {
  return axios.get('/api/list')
}

export const getBanner = () => {
  return axios.get('http://zyxcl.xyz/banner')
}

export const searchMusic = (keywords) => {
  return axios.get('http://zyxcl.xyz/search', {
    params: {
      keywords
    }
  })
}
