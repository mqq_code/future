import React, { useEffect, useState, useCallback } from 'react'
import style from './index.module.scss'
import classNames from 'classnames'

const Keyboard = (props) => {
  const [curId, setCurId] = useState('')

  const mouseDown = (item) => {
    props.changeMusic(item)
    setCurId(item.id)
  }
  const mouseUp = () => {
    setCurId('')
  }

  useEffect(() => {
    const keyDown = (e) => {
      const item = props.list.find(v => v.keyCode === e.keyCode)
      if (item) {
        mouseDown(item)
      }
    }
    document.addEventListener('keydown', keyDown)
    document.addEventListener('keyup', mouseUp)
    return () => {
      document.removeEventListener('keydown', keyDown)
      document.removeEventListener('keyup', mouseUp)
    }
  }, [props.list, props.changeMusic])


  return (
    <div className={style.wrap}>
      <ul>
        {props.list.map(item =>
          <li
            key={item.id}
            onMouseDown={() => mouseDown(item)}
            onMouseUp={mouseUp}
            className={classNames({
              [style.mousedown]: curId === item.id,
              [style.active]: props.power && curId === item.id
            })}
          >{item.keyTrigger}</li>
        )}
      </ul>
    </div>
  )
}

export default Keyboard