import React, { useEffect, useState, useRef } from 'react'
import style from './index.module.scss'
import Btn from './Btn'

const Control = (props) => {
  const [posX, setPosX] = useState(0)
  const isDown = useRef(false)
  const volume = useRef()
  useEffect(() => {
    const { left, width } = volume.current.getBoundingClientRect()
    const max = width - 10
    setPosX(max / 2)
    const mousemove = (e) => {
      if (isDown.current) {
        let x = e.pageX - left - 5
        if (x <= 0) x = 0
        if (x >= max) x = max
        props.changeVolume((x / max).toFixed(2))
        setPosX(x)
      }
    }
    const mouseup = () => {
      isDown.current = false
    }
    document.addEventListener('mousemove', mousemove)
    document.addEventListener('mouseup', mouseup)
    
    return () => {
      document.removeEventListener('mousemove', mousemove)
      document.removeEventListener('mouseup', mouseup)
    }
  }, [])

  const mouseDown = e => {
    isDown.current = true
  }

  return (
    <div className={style.wrap}>
      <Btn status={props.power} changeStatus={() => props.changePower(!props.power)}>power</Btn>
      <div className={style.title}>{props.curMusic.id}</div>
      <div className={style.volume} ref={volume}>
        <i onMouseDown={mouseDown} style={{ transform: `translateX(${posX}px)` }}></i>
      </div>
      <Btn status={props.bank} changeStatus={() => props.changeBank(!props.bank)}>bank</Btn>
    </div>
  )
}

export default Control