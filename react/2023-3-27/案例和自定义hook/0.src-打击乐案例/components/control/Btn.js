import React from 'react'
import style from './index.module.scss'

const Btn = (props) => {
  return (
    <div className={style.btn}>
      <b>{props.children}</b>
      <p className={props.status ? style.on : ''} onClick={props.changeStatus}></p>
    </div>
  )
}

export default Btn