import React, { useState, useEffect, useRef } from 'react'
import { getListApi } from './services'
import Keyboard from './components/keyboard'
import Control from './components/control'
import './App.scss'

const App = () => {
  const [list, setList] = useState([])
  const [curMusic, setCurMusic] = useState({})
  const [power, setPower] = useState(true)
  const [bank, setBank] = useState(false)
  const audioRef = useRef()

  const changeMusic = (item) => {
    if (power) {
      setCurMusic(item)
      audioRef.current.load()
      setTimeout(() => {
        audioRef.current.play()
      })
    }
  }

  const getMusic = url => {
    console.log(url)
    const video = new Audio()
    video.src = url
    video.addEventListener('canplay', () => {
      console.log('加载成功', url)
    })
  }

  const getList = async () => {
    const res = await getListApi()
    if (res.data.code === 0) {
      console.log(res.data.values)
      setList(res.data.values)
      res.data.values.forEach(item => {
        getMusic(item.url)
        getMusic(item.bankUrl)
      })
    }
  }

  useEffect(() => {
    if (!power) {
      setCurMusic({})
    }
  }, [power])

  const changeVolume = (volume) => {
    console.log(volume)
    audioRef.current.volume = volume
  }

  useEffect(() => {
    getList()
    changeVolume(0.5)
  }, [])

  
  return (
    <div className='App'>
      <Keyboard list={list} power={power} changeMusic={changeMusic} />
      <Control
        curMusic={curMusic}
        power={power}
        bank={bank}
        changePower={setPower}
        changeBank={setBank}
        changeVolume={changeVolume}
      />
      <audio ref={audioRef} src={bank ? curMusic.bankUrl : curMusic.url} />
    </div>
  )
}

export default App