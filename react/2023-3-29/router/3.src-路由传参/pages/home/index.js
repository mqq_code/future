import React from 'react'
import Movie from './movie'
import Cinema from './cinema'
import Mine from './mine'
import { NavLink, Switch, Route, Redirect } from 'react-router-dom'

const Home = (props) => {
  return (
    <div className="home">
      <main>
        <Switch>
          <Route path="/home/movie" component={Movie} />
          <Route path="/home/cinema" component={Cinema} />
          <Route path="/home/mine" component={Mine} />
          <Redirect from="/home" to="/home/movie" />
        </Switch>
      </main>
      <footer>
        <NavLink to="/home/movie">电影</NavLink>
        <NavLink to="/home/cinema">影院</NavLink>
        <NavLink to="/home/mine">我的</NavLink>
      </footer>
    </div>
  )
}

export default Home