import React from 'react'
import style from './hot.module.scss'
import { withRouter, useHistory } from 'react-router-dom'

const Item = (props) => {
  const history = useHistory()

  const goDetail = id => {
    // search 传参数
    // props.history.push(`/detail?id=${id}&a=100`)
    // props.history.push({
    //   pathname: '/detail',
    //   search: `id=${id}&a=100`
    // })

    // 动态路由｜params 传参数，必须先配置动态路由
    // props.history.push(`/detail/${id}/aaaa`)

    // state 传参数(刷新页面参数会消失)
    history.push({
      pathname: `/detail/${id}/aaaa`,
      state: props
    })
  }

  return (
    <div className={style.item} onClick={() => goDetail(props.info.id)}>
      <h3>{props.info.name}</h3>
      <p>{props.info.description}</p>
      <img src={props.info.coverImgUrl} alt="" />
    </div>
  )
}

// withRouter: 高阶组件，把当前路由信息传给组件的props
export default Item//withRouter(Item)