import React from 'react'
import Hot from './hot'
import Coming from './coming'
import { NavLink, Switch, Route, Redirect } from 'react-router-dom'

const Movie = (props) => {
  return (
    <div className="movie">
      <div className="topBar">
        <NavLink to="/home/movie/hot">正在热映</NavLink>
        <NavLink to="/home/movie/coming">即将上映</NavLink>
      </div>
      <div className="content">
        <Switch>
          <Route path="/home/movie/hot" component={Hot} />
          <Route path="/home/movie/coming" component={Coming} />
          <Redirect from="/home/movie" to="/home/movie/hot" />
        </Switch>
      </div>
    </div>
  )
}

export default Movie