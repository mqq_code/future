import React, { useState, useEffect } from 'react'
import axios from 'axios'
import { useHistory, useLocation, useParams } from 'react-router-dom'

const query = str => {
  const obj = {}
  const arr = str.slice(1).split('&')
  arr.forEach(item => {
    const [key, value] = item.split('=')
    obj[key] = value
  })
  return obj
}

const Detail = (props) => {
  const history = useHistory() // history === props.history
  const location = useLocation() // location === props.location
  const params = useParams() // params = props.match.params

  console.log(props)

  // props.location.search：获取 ？ 后的参数
  // const queryObj = query(props.location.search)
  // console.log(queryObj)

  // 获取动态路由参数
  // console.log(props.match.params.id)

  // 获取state参数
  // console.log(props.location.state)

  // /playlist/detail?id=24381616

  const [info, setInfo] = useState({})

  const getInfo = async () => {
    const res = await axios.get('https://zyxcl-music-api.vercel.app/playlist/detail', {
      params: {
        id: props.match.params.id
      }
    })
    // console.log(res.data)
    setInfo(res.data.playlist)
  }

  useEffect(() => {
    getInfo()
  }, [])

  return (
    <div>
      {JSON.stringify(info)}
    </div>
  )
}

export default Detail