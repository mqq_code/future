import React from 'react'
import Home from './pages/home'
import Detail from './pages/detail'
import Login from './pages/login'
import NotFound from './pages/404'
import {
  Route, // 渲染路由视图
  Redirect, // 重定向组件
  Switch, // 只渲染匹配成功的第一个组件
} from 'react-router-dom'


const App = () => {
  return (
    <Switch>
      <Route path="/home" component={Home} />
      <Route exact path="/detail/:id/:a" render={(routeInfo) => {
        // routeInfo: 当前路由信息
        return <Detail {...routeInfo} />
      }} />
      <Route exact path="/login" component={Login} />
      <Route path="/404" component={NotFound} />
      <Redirect exact from="/" to="/home" />
      <Redirect to="/404" />
    </Switch>
  )
}

export default App