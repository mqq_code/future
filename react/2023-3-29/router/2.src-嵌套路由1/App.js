import React from 'react'
import Home from './pages/home'
import Detail from './pages/detail'
import Login from './pages/login'
import NotFound from './pages/404'
import Movie from './pages/home/movie'
import Cinema from './pages/home/cinema'
import Mine from './pages/home/mine'
import Hot from './pages/home/movie/hot'
import Coming from './pages/home/movie/coming'
import {
  Route, // 渲染路由视图
  Redirect, // 重定向组件
  Switch, // 只渲染匹配成功的第一个组件
} from 'react-router-dom'


const App = () => {
  return (
    <Switch>
      <Route path="/home" render={() => (
        <Home>
          <Switch>
            <Route path="/home/movie" render={() => (
              <Movie>
                <Switch>
                  <Route path="/home/movie/hot" component={Hot} />
                  <Route path="/home/movie/coming" component={Coming} />
                  <Redirect from="/home/movie" to="/home/movie/hot" />
                </Switch>
              </Movie>
            )} />
            <Route path="/home/cinema" component={Cinema} />
            <Route path="/home/mine" component={Mine} />
            <Redirect from="/home" to="/home/movie" />
          </Switch>
        </Home>
      )} />
      <Route exact path="/detail" component={Detail} />
      <Route exact path="/login" component={Login} />
      <Route path="/404" component={NotFound} />
      <Redirect exact from="/" to="/home" />
      <Redirect to="/404" />
    </Switch>
  )
}

export default App