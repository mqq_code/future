import React from 'react'
import { NavLink } from 'react-router-dom'

const Home = (props) => {
  return (
    <div className="home">
      <main>
        {props.children}
      </main>
      <footer>
        <NavLink to="/home/movie">电影</NavLink>
        <NavLink to="/home/cinema">影院</NavLink>
        <NavLink to="/home/mine">我的</NavLink>
      </footer>
    </div>
  )
}

export default Home