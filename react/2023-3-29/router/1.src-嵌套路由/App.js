import React from 'react'
import Home from './pages/home'
import Detail from './pages/detail'
import Login from './pages/login'
import NotFound from './pages/404'
import {
  Route, // 渲染路由视图
  Redirect, // 重定向组件
  Switch, // 只渲染匹配成功的第一个组件
} from 'react-router-dom'


const App = () => {
  return (
    <Switch>
      {/* exact: 精准匹配 */}
      {/* 地址栏匹配到 path 时渲染 component 中的组件 */}
      <Route path="/home" component={Home} />
      {/* 地址栏匹配到 path 时渲染 render 返回值 */}
      <Route exact path="/detail" render={() => {
        return <Detail />
      }} />
      <Route exact path="/login" component={Login} />
      <Route path="/404" component={NotFound} />
      <Redirect exact from="/" to="/home" />
      <Redirect to="/404" />
      {/* <Route component={NotFound} /> */}
    </Switch>
  )
}

export default App