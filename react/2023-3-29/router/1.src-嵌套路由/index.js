import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import { HashRouter, BrowserRouter } from 'react-router-dom'
// HashRouter、BrowserRouter： 路由根组件，一个项目中只能有一个根组件，路由相关的所有内容必须在根组件内使用
// HashRouter: hash 模式路由，地址栏有 #，原理：通过 hashchange 事件监听 # 后的内容改变展示对应的组件
// BrowserRouter: history 模式，地址栏没有 # ，利用 h5 提供的 pushState 实现

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <HashRouter>
    <App />
  </HashRouter>
);
