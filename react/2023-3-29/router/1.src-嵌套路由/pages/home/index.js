import React from 'react'
import Movie from './movie'
import Cinema from './cinema'
import Mine from './mine'

import {
  Route, // 渲染路由视图
  Redirect, // 重定向组件
  Switch, // 只渲染匹配成功的第一个组件
  NavLink // 有高亮类名，默认高亮是 active，可以通过 activeClassName 属性修改，
} from 'react-router-dom'
const Home = (props) => {
  return (
    <div className="home">
      <main>
        <Switch>
          <Route path="/home/movie" component={Movie} />
          <Route path="/home/cinema" component={Cinema} />
          <Route path="/home/mine" component={Mine} />
          <Redirect from="/home" to="/home/movie" />
        </Switch>
      </main>
      <footer>
        <NavLink to="/home/movie">电影</NavLink>
        <NavLink to="/home/cinema">影院</NavLink>
        <NavLink to="/home/mine">我的</NavLink>
      </footer>
    </div>
  )
}

export default Home