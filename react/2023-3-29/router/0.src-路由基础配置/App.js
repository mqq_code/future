import React from 'react'
import Home from './pages/home'
import Detail from './pages/detail'
import Login from './pages/login'
import NotFound from './pages/404'
import {
  Route, // 渲染路由视图
  Redirect, // 重定向组件
  Switch, // 只渲染匹配成功的第一个组件
  Link, // 跳转页面，没有高亮类名
  NavLink // 有高亮类名，默认高亮是 active，可以通过 activeClassName 属性修改，
} from 'react-router-dom'


const App = () => {

  return (
    <div>
      <nav>
        {/* <Link to="/home">首页</Link>
        <Link to="/detail">详情</Link>
        <Link to="/login">登陆</Link> */}
        <NavLink to="/home" activeClassName="aaa" activeStyle={{ fontSize: 20 }}>首页</NavLink>
        <NavLink to="/detail" activeClassName="aaa">详情</NavLink>
        <NavLink to="/login" activeClassName="aaa">登陆</NavLink>
      </nav>
      <Switch>
        {/* exact: 精准匹配 */}
        <Route exact path="/home" component={Home} />
        <Route exact path="/detail" component={Detail} />
        <Route exact path="/login" component={Login} />
        <Route path="/404" component={NotFound} />
        <Redirect exact from="/" to="/home" />
        <Redirect to="/404" />
        {/* <Route component={NotFound} /> */}
      </Switch>
    </div>
  )
}

export default App