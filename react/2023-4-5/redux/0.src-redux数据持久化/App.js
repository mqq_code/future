import React from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
import Home from './pages/home/Home'
import Movie from './pages/movie/Movie'
import MovieDetail from './pages/movie_detail/MovieDetail'
import MemoHome from './pages/memoHome/MemoHome'
import Detail from './pages/detail/Detail'
import Memo from './pages/memoHome/memo/Memo'
import Create from './pages/memoHome/create/Create'
import Mine from './pages/memoHome/mine/Mine'

const App = () => {
  return (
    <Switch>
      <Route exact path="/detail/:id" component={Detail} />
      <Route path="/memo_home" render={routeInfo => {
        return (
          <MemoHome {...routeInfo}>
            <Switch>
              <Route exact path="/memo_home" component={Memo} />
              <Route exact path="/memo_home/create" component={Create} />
              <Route exact path="/memo_home/mine" component={Mine} />
            </Switch>
          </MemoHome>
        )
      }} />
      <Route exact path="/movie" component={Movie} />
      <Route exact path="/movie/detail/:id" component={MovieDetail} />
      <Route exact path="/" component={Home} />
      <Redirect to="/" />
    </Switch>
  )
}

export default App