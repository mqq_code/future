import React from 'react'
import { Link } from 'react-router-dom'
const Home = () => {
  return (
    <div className="home">
      <h2>首页</h2>
      <p>
        <Link to="/movie">去电影</Link>
        <Link to="/memo_home">备忘录</Link>
      </p>
    </div>
  )
}

export default Home