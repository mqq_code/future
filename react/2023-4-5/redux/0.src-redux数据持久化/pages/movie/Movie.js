import React, { useEffect } from 'react'
import axios from 'axios'
import { useDispatch, useSelector } from 'react-redux'

const Movie = () => {
  const dispatch = useDispatch()
  const movieList = useSelector(state => state.movieList)

  useEffect(() => {
    axios.get('https://zyxcl.xyz/toplist/detail').then(res => {
      dispatch({
        type: 'SET_MOVIE',
        payload: res.data.list
      })
    })
  }, [])

  console.log(movieList)

  return (
    <div>
      <ul>
        {movieList.map(item =>
          <li key={item.id}>
            <img src={item.coverImgUrl} width="120" alt="" />
          </li>
        )}
      </ul>
    </div>
  )
}

export default Movie