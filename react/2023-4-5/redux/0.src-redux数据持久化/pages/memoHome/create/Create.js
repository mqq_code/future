import React from 'react'
import style from './create.module.scss'
import {
  Form,
  Input,
  Button,
  TextArea,
  Radio,
  Space,
  Stepper,
  Checkbox,
  Grid,
  Toast
} from 'antd-mobile'
import { useDispatch } from 'react-redux'

const Create = (props) => {
  const dispatch = useDispatch()
  const onFinish = values => {
    dispatch({
      type: 'CREATE_MEMO',
      payload: values
    })
    Toast.show('提交成功')
    props.history.push('/')
  }
  return (
    <div className={style.create}>
      <Form
        layout='horizontal'
        onFinish={onFinish}
        footer={
          <Button block type='submit' color='primary' size='large'>提交</Button>
        }
      >
        <Form.Item name='pt' label='平台' rules={[{ required: true, message: '姓名不能为空' }]}>
          <Radio.Group>
            <Space direction="horizontal">
              <Radio value='tb'>淘宝</Radio>
              <Radio value='jd'>京东</Radio>
              <Radio value='pdd'>拼多多</Radio>
            </Space>
          </Radio.Group>
        </Form.Item>
        <Form.Item name='name' label='任务' rules={[{ required: true, message: '姓名不能为空' }]}>
          <Input placeholder='请输入任务名称' />
        </Form.Item>
        <Form.Item label='本金'>
          <Grid columns={2} gap={10} className={style.row}>
            <Grid.Item>
              <Form.Item name="bj" rules={[{ required: true, message:'本金必须大于0' }]}>
                <Stepper min={0} />
              </Form.Item>
            </Grid.Item>
            <Grid.Item>
              <Form.Item name="bj_back">
                <Checkbox>本金已返</Checkbox>
              </Form.Item>
            </Grid.Item>
          </Grid>
        </Form.Item>
        <Form.Item label='佣金'>
          <Grid columns={2} gap={10} className={style.row}>
            <Grid.Item>
              <Form.Item name="yj" rules={[{ required: true, message:'佣金必须大于0' }]}>
                <Stepper min={0} />
              </Form.Item>
            </Grid.Item>
            <Grid.Item>
              <Form.Item name="yj_back">
                <Checkbox>本金已返</Checkbox>
              </Form.Item>
            </Grid.Item>
          </Grid>
        </Form.Item>
        <Form.Item name='remark' label='备注'>
          <TextArea placeholder='请输入备注，没有可以不写' />
        </Form.Item>
      </Form>
    </div>
  )
}

export default Create