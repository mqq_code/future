import React, { useRef } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { setUserAction } from '../../store/actions'

const User = () => {
  const dispatch = useDispatch()
  const userInfo = useSelector(s => s.user)
  const inp = useRef()
  return (
    <div>
      <h2>User</h2>
      <input type="text" ref={inp} />
      <button onClick={() => dispatch(setUserAction(inp.current.value))}>触发 set_user</button>
      <p>{JSON.stringify(userInfo)}</p>
    </div>
  )
}

export default User