import React, { useEffect } from 'react'
import axios from 'axios'
import { useDispatch, useSelector } from 'react-redux'
import { setMovieAction } from '../../store/actions'

const Movie = (props) => {
  const dispatch = useDispatch()
  const movieList = useSelector(state => state.movie.movieList)

  useEffect(() => {
    axios.get('https://zyxcl.xyz/toplist/detail').then(res => {
      dispatch(setMovieAction(res.data.list))
    })
  }, [])


  const goDetail = (id) => {
    props.history.push(`/movie/detail/${id}`)
  }
  return (
    <div>
      <ul>
        {movieList.map(item =>
          <li key={item.id} onClick={() => goDetail(item.id)}>
            <img src={item.coverImgUrl} width="120" alt="" />
          </li>
        )}
      </ul>
    </div>
  )
}

export default Movie