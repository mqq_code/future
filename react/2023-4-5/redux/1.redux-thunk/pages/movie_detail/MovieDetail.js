import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { getMovieListAction } from '../../store/actions'


const MovieDetail = (props) => {
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(getMovieListAction).then(res => {
      console.log(res.data)
    })
  }, [])

  const info = useSelector(state => {
    return state.movie.movieList.find(v => v.id === Number(props.match.params.id)) || {}
  })
  return (
    <div>
      <h2>{info.name}</h2>
      <img src={info.coverImgUrl} width="120" alt="" />
      <p>{info.description}</p>
      <p>播放量:{info.playCount}</p>
      <ul>
        {info.tracks?.map(item =>
          <li key={item.first}>{item.first} - {item.second}</li>
        )}
      </ul>
    </div>
  )
}

export default MovieDetail