import React from 'react'
import { useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import style from './memo.module.scss'
import moment from 'moment'

const Memo = (props) => {
  const state = useSelector(s => s.memo.memoList)
  const goDetail = id => {
    props.history.push(`/detail/${id}`)
  }

  return (
    <div className={style.memo}>
      {state.length === 0 ?
        <div className={style.empty}>
          <p>备忘录空空如也，快去创建一个吧～</p>
          <Link to="/create">新建</Link>
        </div>
      :
        <div className={style.list}>
          {state.map(item =>
            <div key={item.id} className={style.item} onClick={() => goDetail(item.id)}>
              <h3>{item.name}</h3>
              <p>本金{item.bj}，佣金{item.yj}</p>
              <p>{moment(item.date).format('YYYY-MM-DD kk:mm:ss')}</p>
              <div className={style.btns}>
                <button disabled={!item.bj_back}>本金</button>
                <button disabled={!item.yj_back}>佣金</button>
              </div>
            </div>
          )}
        </div>
      }
    </div>
  )
}

export default Memo