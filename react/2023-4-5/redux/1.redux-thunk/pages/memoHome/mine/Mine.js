import React from 'react'
import { useSelector } from 'react-redux'


const keys = [{
  key: 'bj_back',
  title: '本金已返'
},{
  key: 'bj_no_back',
  title: '本金未返'
},{
  key: 'yj_back',
  title: '佣金已返'
},{
  key: 'yj_no_back',
  title: '佣金未返'
}]
const Mine = () => {
  const total = useSelector(state => {
    return state.memo.memoList.reduce((prev, val) => {
      if (val.bj_back) {
        prev.bj_back += val.bj
      } else {
        prev.bj_no_back += val.bj
      }
      if (val.yj_back) {
        prev.yj_back += val.yj
      } else {
        prev.yj_no_back += val.yj
      }
      return {...prev}
    }, {
      bj_back: 0,
      bj_no_back: 0,
      yj_back: 0,
      yj_no_back: 0,
    })
  })

  return (
    <div>
      {keys.map(item =>
        <div key={item.key}>
          <b>{total[item.key]}</b>
          <p>{item.title}</p>
        </div>
      )}
    </div>
  )
}

export default Mine