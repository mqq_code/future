import { legacy_createStore as createStore, applyMiddleware } from 'redux'
import logger from 'redux-logger'
import thunk from 'redux-thunk'
import reducers from './reducers'
 

// redux-thunk: 中间件，让 dispatch 可以接收函数作为参数
const store = createStore(reducers, applyMiddleware(thunk, logger))

export default store


