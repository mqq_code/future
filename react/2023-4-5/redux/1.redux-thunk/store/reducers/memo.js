import { CREATE_MEMO, SAVE, REMOVE } from '../actions'

const initState = {
  memoList: []
}

const reducer = (state = initState, action) => {
  if (action.type === CREATE_MEMO) {
    return {
      ...state,
      memoList: [{
        ...action.payload,
        id: Date.now(),
        date: Date.now(),
      }, ...state.memoList]
    }
  } else if (action.type === SAVE) {
    const newState = JSON.parse(JSON.stringify(state))
    newState.memoList.forEach(item => {
      if (item.id === action.payload.id) {
        item.bj_back = action.payload.bj_back
        item.yj_back = action.payload.yj_back
      }
    })
    return newState
  } else if (action.type === REMOVE) {
    return {
      ...state,
      memoList: state.memoList.filter(v => v.id !== action.payload)
    }
  }
  console.log('memo reducer')
  return state
}

export default reducer