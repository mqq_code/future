import { SET_USER } from '../actions'

const initState = {
  user: '小明',
  age: 20,
  sex: '男'
}

const reducer = (state = initState, action) => {
  console.log('user reducer')
  if (action.type === SET_USER) {
    return {
      ...state,
      user: action.payload
    }
  }
  return state
}

export default reducer