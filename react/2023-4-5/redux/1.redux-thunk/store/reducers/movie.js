import { SET_MOVIE } from '../actions'

const initState = {
  movieList: []
}

// 纯函数
const reducer = (state = initState, action) => {
  if (action.type === SET_MOVIE) {
    return {
      ...state,
      movieList: action.payload
    }
  }
  return state
}

export default reducer