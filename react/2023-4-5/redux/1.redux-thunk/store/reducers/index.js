import { combineReducers } from 'redux'
import memo from './memo'
import movie from './movie'
import user from './user'

// 合并多个reducer
export default combineReducers({
  memo,
  movie,
  user
})
