import axios from 'axios'

export const SET_MOVIE = "SET_MOVIE"
export const CREATE_MEMO = "CREATE_MEMO"
export const SAVE = "SAVE"
export const REMOVE = "REMOVE"
export const SET_USER = "SET_USER"

// 修改用户名
export const setUserAction = payload => {
  return {
    type: SET_USER,
    payload
  }
}

// 修改仓库电影列表
export const setMovieAction = payload => {
  return {
    type: SET_MOVIE,
    payload
  }
}

// 异步获取电影列表
export const getMovieListAction = async (dispatch) => {
  const res = await axios.get('https://zyxcl.xyz/toplist/detail')
  dispatch(setMovieAction(res.data.list))
  return res
}

// 创建备忘录
export const createMemoAction = payload => {
  return {
    type: CREATE_MEMO,
    payload
  }
}
