const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

// CommonJS 规范
// 引入 require
// 抛出 module.exports
module.exports = {
  mode: 'development', // 打包模式, development、 production
  entry: './src/main.js',
  output: {
    path: path.join(__dirname, 'build'),
    filename: 'index.js',
    clean: true
  },
  module: {
    // 配置loader，让js可以解析其他类型的文件
    rules: [
      {
        test: /\.(css|sass|scss)$/i,
        use: ['style-loader', 'css-loader', 'sass-loader']
      }
    ]
  },
  // 插件，扩展webpack功能
  plugins: [
    // 自动把打包后的 js 文件引入到此页面
    new HtmlWebpackPlugin({
      template: './src/index.html',
      filename: 'index.html'
    })
  ],
  // 开发服务器
  devServer: {
    port: 9000,
    open: true,
    proxy: {
      '/api': {
        target: 'http://192.168.1.101:3000',
        pathRewrite: { '^/api': '' },
      }
    }
  }
}