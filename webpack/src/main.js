import { a, b } from './utils/a'
import sum from './utils/b'
import './scss/style.scss'
import axios from 'axios'

// let AA = 1
// class Person {
//   constructor(name) {
//     this.name = name
//   }
// }
// let xm = new Person('晓明')
// console.log(AA + sum(a, b))
// let title = document.querySelector('h1')
// title.style.color = 'red'

// 同源策略：浏览器的安全机制，页面发起的 xhr 请求时请求地址的协议、端口、域名必须和当前页面一致
// 跨域：页面发起的 xhr 请求时协议、端口、域名其中一个和当前页面不一致就会报跨域错误，js、css、图片没有跨域限制
// 解决跨域三种方案
// 1. jsonp
// 缺陷：只能处理get请求的跨域，无法处理post请求，需要后端配合修改代码
// 原理：
  //  利用 script 标签不会发生跨域的特性，动态创建 script 标签发起 js 请求
  //  后端返回一段 js 代码，调用前端指定的函数，把数据传入制定的函数参数中
// 2. 后端配置允许跨域访问的响应头 Access-Control-Allow-Origin
// 3. 前端代理（本地开发使用 webpack 中的 proxy 属性配置，线上使用 nginx 代理）
  // 原理：跨域是受浏览器的安全限制，服务器与服务器之间通信没有跨域
  // 前端页面请求本地的服务器应用，本地服务器应用发送请求，获取到数据后返回给页面


// jsonp：原理
// window.getListRes1 = function (res) {
//   console.log('res1', res)
// }
// window.getListRes2 = function (res) {
//   console.log('res2', res)
// }
// window.getListRes3 = function (res) {
//   console.log('res3', res)
// }
// function jsonp(url) {
//   let script = document.createElement('script')
//   script.src = url
//   document.body.appendChild(script)
//   script.onload = () => {
//     document.body.removeChild(script)
//   }
// }
// jsonp('http://192.168.1.101:3000/getlist?page=1&pagesize=5&callback=getListRes1')
// jsonp('http://192.168.1.101:3000/getlist?page=1&pagesize=10&callback=getListRes2')
// jsonp('http://192.168.1.101:3000/getlist?page=1&pagesize=20&callback=getListRes3')


axios.post('/api/getlist', {
  page: 1,
  pagesize: 10
})
.then(res => {
  console.log(res.data)
})
