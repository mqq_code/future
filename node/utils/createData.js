const fs = require('fs')
const path = require('path')

const firstName = ['安', '张', '王', '李', '马', '赵', '左', '欧阳', '司马', '诸葛', '上官', '西门']
const lastName = ['铁柱', '八一', '钢弹', '红旗', '爱国', '翠花', '晓明']

const data = new Array(1000).fill(0).map((v, i) => {
  return {
    id: i + 1,
    firstName: firstName[Math.floor(Math.random() * firstName.length)],
    lastName: lastName[Math.floor(Math.random() * lastName.length)],
    age: Math.floor(Math.random() * 22 + 18),
    sex: Math.random() > 0.5 ? '男' : '女'
  }
})

fs.writeFileSync(path.join(__dirname, './data.json'), JSON.stringify(data))

