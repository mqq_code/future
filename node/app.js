const express = require('express')
const path = require('path')
const data = require('./utils/data.json')

// 创建服务器应用
const app = express()

app.use(express.urlencoded()) // 允许post接收 urlencoded 类型的参数
app.use(express.json()) // 允许post接收 json 类型的参数

// 设置允许访问的文件夹
app.use(express.static(path.join(__dirname, './public')))

// 定义接口
app.get('/getlist', (req, res) => {
  // get请求接受参数
  console.log(req.query)
  const { page, pagesize, callback } = req.query
  // 1  10  data.slice(0, 10)
  // 2  10  data.slice(10, 20)
  // 3  10  data.slice(20, 30)
  // res.send({
  //   total: data.length,
  //   data: data.slice(page * pagesize - pagesize, page * pagesize),
  //   page: Number(page),
  //   pagesize: Number(pagesize)
  // })

  let data1 = {
    total: data.length,
    data: data.slice(page * pagesize - pagesize, page * pagesize),
    page: Number(page),
    pagesize: Number(pagesize)
  }

  res.send(`${callback}(${JSON.stringify(data1)})`)
})


// 后端配置允许跨域访问的响应头
app.use((req, res, next) => {
  res.set({
    'Access-Control-Allow-Credentials': true,
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers': 'X-Requested-With,Content-Type',
    'Access-Control-Allow-Methods': 'PUT,POST,GET,DELETE,OPTIONS',
  })
  next()
})

app.post('/getlist', (req, res) => {

  // post请求接受 urlencoded 参数
  console.log('req.body', req.body)
  const { page, pagesize } = req.body
  // 1  10  data.slice(0, 10)
  // 2  10  data.slice(10, 20)
  // 3  10  data.slice(20, 30)
  res.send({
    total: data.length,
    data: data.slice(page * pagesize - pagesize, page * pagesize),
    page: Number(page),
    pagesize: Number(pagesize)
  })
})


app.listen(3000, () => {
  console.log('服务启动成功 http://192.168.1.124:3000')
})


// const http = require('http')
// const path = require('path')
// const fs = require('fs')
// const app = http.createServer((req, res) => {
//   res.end(fs.readFileSync(path.join(__dirname, './public/index.html')))
// })
// app.listen(3000, () => {
//   console.log('服务启动成功 http://192.168.1.124:3000')
// })
