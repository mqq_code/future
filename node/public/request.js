function axios({ url, method = 'GET', data = {}, headers = {} }) {
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest()
    method = method.toUpperCase()
    if (method === 'GET') {
      let [url1, qs = ''] =  url.split('?')
      const { params } = data
      if (params) {
        if (qs) qs += '&'
        for (let key in params) {
          qs += `${key}=${params[key]}&`
        }
        qs = '?' + qs.slice(0, -1)
      }
      xhr.open(method, url1 + qs)
      xhr.send()
    } else {
      xhr.open(method, url)
      for (let key in headers) {
        xhr.setRequestHeader(key, headers[key])
      }
      if (headers['content-type']) {
        xhr.send(data)
      } else {
        xhr.setRequestHeader('content-type', 'application/json')
        xhr.send(JSON.stringify(data))
      }
    }
    xhr.onreadystatechange = () => {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          const data = JSON.parse(xhr.responseText)
          resolve(data)
        } else {
          reject()
        }
      }
    }
  })
}

axios.get = function(url, data = {}) {
  return axios({ url, method: 'get', data })
}
axios.post = function(url, data = {}, headers = {}) {
  return axios({ url, method: 'post', data, headers })
}

// axios({
//   url: '/getlist?a=100&b=200',
//   data: {
//     params: {
//       page: 1,
//       pagesize: 3
//     }
//   }
// })
// .then(res => {
//   console.log(res)
// })

// axios.get('/getlist', {
//   params: {
//     page: 1,
//     pagesize: 5
//   }
// })
// .then(res => {
//   console.log(res.data)
// })

// axios({
//   url: '/getlist',
//   method: 'post',
//   data: 'page=1&pagesize=5',
//   headers: {
//     'content-type': 'application/x-www-form-urlencoded'
//   }
// })
// .then(res => {
//   console.log(res)
// })

// axios.post('/getlist', {
//   page: 1,
//   pagesize: 10
// })
// .then(res => {
//   console.log(res)
// })