// components/tabpane/tabpane.js
Component({
  relations: {
    '../tabs/tabs': {
      type: 'parent', // 关联的目标节点应为父节点
      linked: function(target) {
        // 每次被插入到 custom-ul 时执行，target是 custom-ul 节点实例对象，触发在 attached 生命周期之后
      },
      linkChanged: function(target) {
        // 每次被移动后执行，target是 custom-ul 节点实例对象，触发在 moved 生命周期之后
      },
      unlinked: function(target) {
        // 每次被移除时执行，target是 custom-ul 节点实例对象，触发在 detached 生命周期之后
      }
    }
  },
  /**
   * 组件的属性列表
   */
  properties: {
    title: String
  },

  /**
   * 组件的初始数据
   */
  data: {
    show: false
  },

  /**
   * 组件的方法列表
   */
  methods: {

  }
})
