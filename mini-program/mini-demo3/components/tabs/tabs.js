// components/tabs/tabs.js
Component({
  relations: {
    '../tabpane/tabpane': {
      type: 'child', // 关联的目标节点应为子节点
      linked: function(target) {
        // 每次有 tabpane 被插入时执行，target是该节点实例对象，触发在该节点 attached 生命周期之后
      },
      linkChanged: function(target) {
        // 每次有 custom-li 被移动后执行，target是该节点实例对象，触发在该节点 moved 生命周期之后
      },
      unlinked: function(target) {
        // 每次有 custom-li 被移除时执行，target是该节点实例对象，触发在该节点 detached 生命周期之后
      }
    }
  },
  methods: {
    _getAllLi: function(){
      // 使用 getRelationNodes 可以获得 nodes 数组，包含所有已关联的 tabpane，且是有序的
      this.nodes = this.getRelationNodes('../tabpane/tabpane')
      this.setData({
        navlist: this.nodes.map(item => item.data.title),
      })
      this.showTabPane(0)
    },
    changetab (e) {
      const { index } = e.currentTarget.dataset
      this.showTabPane(index)
      this.setData({
        curIndex: index
      })
    },
    showTabPane (index) {
      this.nodes[this.data.curIndex].setData({
        show: false
      })
      this.nodes[index].setData({
        show: true
      })
    }
  },
  ready: function(){
    this._getAllLi()
  },

  /**
   * 组件的初始数据
   */
  data: {
    navlist: [], // 导航数据
    curIndex: 0 // 高亮下标
  },
})
