// components/goods/goods.js
Component({
  /**
   * 组件的属性列表, 接收父组件传过来的数据
   */
  properties: { // 通过 this.data 调用数据
    title: String,
    price: Number,
    count: Number,
    goodsId: Number
  },

  /**
   * 组件的初始数据
   */
  data: {
    num: 100
  },

  /**
   * 组件的方法列表
   */
  methods: {
    addNum () {
      this.setData({
        num: this.data.num + 1
      })
    },
    add (e) {
      const { num } = e.currentTarget.dataset
      // 调用父组件的自定义事件，并且传参数
      this.triggerEvent('changeCount', { num, id: this.data.goodsId })
    }
  }
})
