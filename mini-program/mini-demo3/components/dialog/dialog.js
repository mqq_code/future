// components/dialog/dialog.js
import common from '../../behaviors/common'

Component({
  behaviors: [common],
  // 组件的生命周期
  lifetimes: {
    attached: function() {
      // 在组件实例进入页面节点树时执行
      console.log('组件创建成功');
    },
    detached: function() {
      // 在组件实例被从页面节点树移除时执行
      console.log('组件销毁');
    },
  },
  // 页面的生命周期
  pageLifetimes: {
    show: function() {
      // 页面被展示
      console.log('页面展示');
    },
    hide: function() {
      // 页面被隐藏
      console.log('页面隐藏');
    },
    resize: function(size) {
      // 页面尺寸变化
      console.log('页面尺寸改变');
    }
  },
  /**
   * 组件的属性列表
   */
  properties: {
    title: {
      type: String,
      value: '默认标题'
    }
  },
  options: {
    multipleSlots: true, // 启用多 slot 支持
    // styleIsolation: 'apply-shared'
    pureDataPattern: /^_/ // 指定所有 _ 开头的数据字段为纯数据字段
  },

  /**
   * 组件的初始数据
   */
  data: {
    num: 0,
    _num: 100 // 纯数据字段，不可以在页面中渲染
  },
  
  // 监听变量改变,类似vue中watch
  observers: {
    'num': function(num1) {
      // 在 num 被设置时，执行这个函数
      console.log('num改变了', this.data.num);
    }
  },

  /**
   * 组件的方法列表
   */
  methods: {
    add () {
      this.setData({
        num: this.data.num + 1
      })
    }
  }
})
