// pages/index/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    arr: [{
      id: 1,
      title: '苹果',
      price: 12,
      count: 1
    },{
      id: 2,
      title: '香蕉',
      price: 20,
      count: 3
    },{
      id: 3,
      title: '橘子',
      price: 16,
      count: 2
    }]
  },
  getChild () {
    // 获取子组件实例对象
    const child = this.selectComponent('.goods');
    // 调用子组件的方法
    // child.addNum()
    child.setData({
      num: child.data.num + 1
    })
  },
  changeCount(e) {
    // e.detail: 接收子组件传过来的数据
    const { num, id } = e.detail
    const index = this.data.arr.findIndex(v => v.id === id)
    const key = `arr[${index}].count`
    if (this.data.arr[index].count + num <= 0) return
    this.setData({
      [key]: this.data.arr[index].count + num
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})