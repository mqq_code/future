// pages/home/home.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    arr: [1,2,3,4,5,6,7,8],
    scrollTop: 0,
    scrollIntoView: 'item0',
    count: 0
  },

  handleClick () {
    this.setData({
      // scrollTop: 50,
      scrollIntoView: 'item1'
    })
  },

  scrollFn (e) {
    console.log(e.detail);
  },
  goDetail () {
    wx.navigateTo({
      url: '/pages/detail/detail?a=100&b=200'
    })
  },

  getElement () {
    // 返回一个选择器
    const query = wx.createSelectorQuery()
    // 选择页面元素
    const box = query.select('.box')
    // 查询元素的位置信息
    box.boundingClientRect(function(rect){
      console.log('位置信息', rect);
    })
    box.scrollOffset((res) => {
      console.log(res);
    })
    // 开始执行查询请求
    query.exec()
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log('%conLoad', 'color: red;font-size: 20px');
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    console.log('%conReady',  'color: green;font-size: 20px');
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    console.log('%conShow',  'color: yellow;font-size: 20px');
    this.timer = setInterval(() => {
      this.setData({
        count: this.data.count + 1
      })
      // console.log(this.data.count);
    }, 1000)
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    console.log('%c onHide',  'color: blue;font-size: 20px');
    clearInterval(this.timer)
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    console.log('%c onUnload',  'color: blue;font-size: 20px');
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})