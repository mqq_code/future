// pages/list/list.js

const request = ({ url, method = 'get', data = {} }) => {
  return new Promise((resolve, reject) => {
    wx.request({
      url,
      method,
      data,
      success (res) {
        resolve(res.data)
      },
      fail (err) {
        reject(err)
      }
    })
  })
}


Page({

  /**
   * 页面的初始数据
   */
  data: {
    banners: []
  },

  shareImage () {
    wx.downloadFile({
      url: 'http://p1.music.126.net/e-ZJo2eULNaoJs0SGZV1pg==/109951168449859992.jpg',
      success: (res) => {
        wx.showShareImageMenu({
          path: res.tempFilePath
        })
      }
    })
  },
  getuserinfo (e) {
    console.log(e);
  },
  chooseavatar (e) {
    console.log(e);
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: async function (options) {
    // 显示分享按钮
    wx.showShareMenu({
      withShareTicket: true,
      menus: ['shareAppMessage', 'shareTimeline']
    })
    // 隐藏分享按钮
    // wx.hideShareMenu({
    //   menus: ['shareAppMessage', 'shareTimeline']
    // })
    wx.getStorage({
      key: 'banners',
      success: res => {
        this.setData({
          banners: res.data
        })
      }
    })
    // wx.showLoading({
    //   title: '加载中',
    // })
    // const res = await request({
    //   url: 'http://121.89.213.194:3000/banner',
    //   data: {
    //     x: 'xxxx',
    //     y: 'yyyyyyy'
    //   }
    // })
    // console.log(res.banners);
    // this.setData({
    //   banners: res.banners
    // })
    // wx.setStorage({
    //   key: 'banners',
    //   data: res.banners
    // })
    // setTimeout(() => {
    //   wx.hideLoading()
    // }, 2000)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (obj) {
    // 配置分享内容
    return {
      title: '快来测试一下',
      path: '/pages/list/list',
      imageUrl: 'http://p1.music.126.net/e-ZJo2eULNaoJs0SGZV1pg==/109951168449859992.jpg'
    }
  }
})