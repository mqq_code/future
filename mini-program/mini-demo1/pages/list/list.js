// pages/list/list.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    name: '小明',
    obj: {
      name: '小明',
      age: 10,
      sex: '男',
      info: {
        a: {
          b: {
            c: 'ccccccc'
          }
        }
      }
    }
  },

  inputchange (e) {
    console.log(e.detail.value)
    // this.setData({
    //   obj: {
    //     ...this.data.obj,
    //     name: e.detail.value
    //   }
    // })
    // 修改对象的属性
    this.setData({
      'obj.info.a.b.c': e.detail.value
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})