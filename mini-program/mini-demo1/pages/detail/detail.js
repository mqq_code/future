// pages/detail/detail.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    arr: [1,2,3,4,5],
    name: 'aaa',
    list: [
      {
        name: '苹果',
        price: 10,
        count: 1
      },
      {
        name: '香蕉',
        price: 20,
        count: 3
      },
      {
        name: '梨',
        price: 3,
        count: 2
      }
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})