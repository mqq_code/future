import request from './request.js'

// 排行榜
export const getToplist = () => {
  return request({
    url: `/toplist`,
  })
}

// 排行榜详情
export const getToplistDetail = () => {
  return request({
    url: `/toplist/detail`,
  })
}

// 歌单详情
export const getPlaylistDetail = (id) => {
  return request({
    url: `/playlist/detail`,
    data: { id }
  })
}
// 获取歌单所有歌曲
export const getPlaylistTrack = (id, limit, offset) => {
  return request({
    url: `/playlist/track/all`,
    data: { id, limit, offset }
  })
}
// 歌曲详情
export const getSongDetail = (ids) => {
  return request({
    url: `/song/detail`,
    data: { ids }
  })
}
// 音乐URL
export const getSongUrl = (id) => {
  return request({
    url: `/song/url`,
    data: { id }
  })
}
// 歌词
// /lyric?id=33894312
// /lyric/new?id=33894312
export const getLyric = (id) => {
  return request({
    url: `/lyric`,
    data: { id }
  })
}
// 评论
export const getComment = (id, limit = 10, offset = 0) => {
  return request({
    url: `/comment/music`,
    data: { id, limit, offset }
  })
}

// 热搜榜
export const getSearchHot = () => {
  return request({
    url: `/search/hot/detail`
  })
}
// 搜索
export const getSearchResult = (keywords) => {
  return request({
    url: `/search`,
    data: { keywords }
  })
}
// 搜索建议
export const getSearchSuggest = (keywords, type = 'mobile') => {
  return request({
    url: `/search/suggest`,
    data: {
      keywords,
      type
    }
  })
}