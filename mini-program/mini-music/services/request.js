// const BaseURL = 'http://121.89.213.194:3000'
const BaseURL = 'https://zyxcl.xyz'

const request = ({
  url,
  method = 'GET',
  data = {},
  header = {}
}) => {
  return new Promise((resolve, reject) => {
    wx.request({
      url: BaseURL + url,
      method,
      data,
      header,
      success: res => {
        resolve(res.data)
      },
      fail: err => {
        reject(err)
      }
    })
  })
}

export default request