// app.js
import {
  getSongUrl
} from './services/index'
App({
  onLaunch() {
    const res = wx.getMenuButtonBoundingClientRect()
    this.globalData.menuButton = res

    // 监听播放完成，开始下一首
    this.globalData.audioCtx.onEnded(() => {
      console.log('播放完成');
      if (this.globalData.currentIndex + 1 > this.globalData.playlist.length) {
        this.globalData.currentIndex = 0
      } else {
        this.globalData.currentIndex ++
      }
      this.play()
    })
  },
  async play () {
    console.log('播放音乐');
    const id = this.globalData.playlist[this.globalData.currentIndex]
    if (this.globalData.audioCtx.src === id) {
      // 当前播放歌曲和要播放的歌曲一样时直接播放
      this.globalData.audioCtx.play()
    } else {
      const res = await getSongUrl(id)
      this.globalData.audioCtx.src = res.data[0].url
      this.globalData.audioCtx.autoplay = true
      this.globalData.audioCtx.playbackRate = 2
    }
  },
  addSong (id) {
    const index = this.globalData.playlist.indexOf(id)
    if (index > -1) {
      // 添加的歌曲在列表中存在，从原位置删除
      this.globalData.playlist.splice(index, 1)
    }
    // 把添加的歌曲插入到现在播放的位置
    this.globalData.playlist.splice(this.globalData.currentIndex, 0, id)
  },
  globalData: {
    // 创建全局音乐实例
    audioCtx: wx.createInnerAudioContext(),
    playlist: [], // 当前播放列表
    currentIndex: 0, // 当前播放的歌曲下标
    menuButton: {}
  }
})
