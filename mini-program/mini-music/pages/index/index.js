// pages/index/index.js
import {
  getToplist,
  getPlaylistDetail,
  getToplistDetail
} from '../../services/index'
import menuButton from '../../behaviors/menuButton'

Page({
  behaviors: [menuButton],
  /**
   * 页面的初始数据
   */
  data: {
    list: [],
    loading: true
  },

  goSearch () {
    wx.navigateTo({
      url: '/pages/search/search',
    })

  },
  goList (e) {
    const { id } = e.currentTarget.dataset
    wx.navigateTo({
      url: '/pages/list/list?id=' + id
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: async function (options) {
    const res = await getToplistDetail()
    console.log(res.list.slice(0, 4));
    this.setData({
      list: res.list.slice(0, 4),
      loading: false
    })
    // // 调用排行榜接口
    // const res = await getToplist()
    // // 截取排行榜前四个
    // const list = res.list.slice(0, 4)
    // // 遍历排行榜获取每个排行榜前三首歌
    // const requestArr = list.map(item => {
    //   return getPlaylistDetail(item.id)
    // })
    // const playlistRes = await Promise.all(requestArr)
    // // 赋值更新页面
    // this.setData({
    //   list: playlistRes.map(item => {
    //     return {
    //       ...item.playlist,
    //       tracks: item.playlist.tracks.slice(0, 3)
    //     }
    //   })
    // })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})