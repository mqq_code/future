// pages/player/player.js
import {
  getSongDetail,
  getLyric,
  getComment
} from '../../services/index'
import menuButton from '../../behaviors/menuButton'

const formatTime = (str) => {
  const [m, s] = str.split(':')
  return m * 60 + Number(s)
}
// 获取全局数据
var app = getApp()

Page({
  behaviors: [menuButton],
  /**
   * 页面的初始数据
   */
  data: {
    paused: true, // 是否暂停
    songInfo: {}, // 歌曲信息
    lyric: [], // 歌词
    currentIndex: 0, // 当前歌词位置
    commonts: [] // 评论
  },
  songId: '',
  innerAudioContext: null,
  play() {
    if (app.globalData.audioCtx.paused) {
      app.globalData.audioCtx.play()
      this.setData({
        paused: false
      })
    } else {
      app.globalData.audioCtx.pause()
      this.setData({
        paused: true
      })
    }
  },
  async getDetail() {
    const res = await getSongDetail(this.songId)
    this.setData({
      songInfo: res.songs[0]
    })
  },
  async getLyric() {
    const res = await getLyric(this.songId)
    const lyric = res.lrc.lyric.split(/\n/).filter(Boolean).map(item => {
      let [time, word] = item.split(']')
      return [formatTime(time.slice(1)), word]
    })
    this.setData({
      lyric
    })
  },
  async getComment() {
    const res = await getComment(this.songId, 20)
    console.log('评论', res.comments);
    this.setData({
      commonts: res.comments
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // options = {id: "297748"}
    this.songId = Number(options.id)
    app.addSong(this.songId) // 把当前id存到全局
    this.getDetail()
    this.getLyric()
    this.getComment()
  },
  onReady () {
    app.play() // 获取音乐url，播放音乐
  },
  onEnded () {
    // 页面的播放结束
    this.setData({
      currentIndex: 0,
      paused: true
    })
    console.log('player内播放结束');
    // 当前歌曲播放结束调用下一首歌曲的信息
    this.songId = app.globalData.playlist[app.globalData.currentIndex]
    this.getDetail()
    this.getLyric()
    this.getComment()
  },
  onTimeUpdate () {
    console.log('监听歌曲正在播放');
    if (this.data.paused) {
      this.setData({ paused: false })
    }
    if (this.data.currentIndex + 1 >= this.data.lyric.length) return
    const currentTime = app.globalData.audioCtx.currentTime
    const nextTime = this.data.lyric[this.data.currentIndex + 1][0]
    if (currentTime >= nextTime) {
      this.setData({
        currentIndex: this.data.currentIndex + 1
      })
    }
  },
  onPlay () {
    this.setData({ paused: false })
    console.log('开始播放');
  },
  // 取消监听
  offListener () {
    // 取消监听
    app.globalData.audioCtx.offEnded(this.onEnded)
    app.globalData.audioCtx.offTimeUpdate(this.onTimeUpdate)
    app.globalData.audioCtx.offPlay(this.onPlay)
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // 监听播放结束
    app.globalData.audioCtx.onEnded(this.onEnded)
    // 监听音频播放进度更新
    app.globalData.audioCtx.onTimeUpdate(this.onTimeUpdate)
    // 监听开始播放
    app.globalData.audioCtx.onPlay(this.onPlay)
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    this.offListener()
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    this.offListener()
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: this.data.songInfo.name,
      imageUrl: this.data.songInfo.al.picUrl
    }
  }
})