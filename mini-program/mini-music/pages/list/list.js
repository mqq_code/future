// pages/list/list.js
import { getPlaylistDetail, getPlaylistTrack } from '../../services/index'
import menuButton from '../../behaviors/menuButton'
const app = getApp()

Page({
  behaviors: [menuButton],

  /**
   * 页面的初始数据
   */
  data: {
    playlistInfo: {},
    tracks: [],
    loading: true,
    skeletonLoading: true
  },

  playlistId: '',
  offset: 0,

  playMusic (e) {
    const { id } = e.currentTarget.dataset
    wx.navigateTo({
      url: `/pages/player/player?id=${id}`,
    })
  },

  playAll () {
    // 存当前歌单所有id
    app.globalData.playlist = this.data.playlistInfo.trackIds.map(v => v.id)
    app.play()
  },
  async getInfo () {
    // 获取歌单详情
    const res = await getPlaylistDetail(this.playlistId)
    this.setData({
      playlistInfo: res.playlist
    })
  },

  async getTracks () {
    const res = await getPlaylistTrack(this.playlistId, 10, this.offset)
    this.setData({
      tracks: [...this.data.tracks, ...res.songs],
      loading: false
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: async function (options) {
    // options = { id: 19723756 }
    this.playlistId = options.id
    // 获取歌单和歌曲信息
    await Promise.all([this.getInfo(), this.getTracks()])
    // 等待两个接口都成功后隐藏骨架屏
    this.setData({
      skeletonLoading: false
    })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if (this.data.tracks.length >= this.data.playlistInfo.trackCount || this.data.loading) return
    this.setData({
      loading: true
    })
    this.offset += 10
    this.getTracks()
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: '分享歌单:' + this.data.playlistInfo.name,
      imageUrl: this.data.playlistInfo.coverImgUrl
    }
  }
})