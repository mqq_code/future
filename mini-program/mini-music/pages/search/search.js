// pages/search/search.js
import menuButton from '../../behaviors/menuButton'
import {
  getSearchHot,
  getSearchResult,
  getSearchSuggest
} from '../../services/index'

Page({
  behaviors: [menuButton],
  /**
   * 页面的初始数据
   */
  data: {
    hotList: [], // 热搜
    serchText: '',
    suggestList: [],
    resultList: [],
    searchloading: false,
    showSuggest: false,
    history: wx.getStorageSync('history') || []
  },

  clearHistory () {
    this.setData({
      history: []
    })
    wx.setStorage({
      key: 'history',
      data: []
    })
  },
  clearSearch () {
    this.setData({
      serchText: '',
      suggestList: [],
      resultList: [],
      showSuggest: false
    })
  },

  goPlayer (e) {
    const { id } = e.currentTarget.dataset
    wx.navigateTo({
      url: '/pages/player/player?id=' + id,
    })
  },

  async startSearch (e) {
    const { keyword } = e.currentTarget.dataset
    const history = [...this.data.history]
    const index = history.indexOf(keyword)
    if (index > -1) {
      history.splice(index, 1)
    }
    history.unshift(keyword)
    wx.setStorage({
      key: 'history',
      data: history
    })
    this.setData({
      searchloading: true,
      showSuggest: false,
      serchText: keyword,
      history
    })
    const res = await getSearchResult(keyword)
    this.setData({
      resultList: res.result.songs.map(item => {
        const info = item.artists.map(v => v.name).join('/') + '-' + item.album.name
        return {
          ...item,
          info
        }
      }),
      searchloading: false
    })
  },
  timer: null,
  async getSuggest () {
    this.setData({
      showSuggest: true,
      resultList: []
    })
    const res = await getSearchSuggest(this.data.serchText)
    this.setData({
      suggestList: res.result.allMatch
    })
  },
  changeInput () {
    if (this.data.serchText) {
      // 添加防抖：连续调用同一个函数只执行最后一次
      if (this.timer) clearTimeout(this.timer)
      this.timer = setTimeout(() => {
        this.getSuggest()
        this.timer = null
      }, 1000)
    } else {
      this.setData({
        showSuggest: false,
        resultList: []
      })
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: async function (options) {
    const res = await getSearchHot()
    this.setData({
      hotList: res.data
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})