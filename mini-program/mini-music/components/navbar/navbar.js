// components/navbar/navbar.js
import menuButton from '../../behaviors/menuButton'

const app = getApp()

Component({
  behaviors: [menuButton],
  /**
   * 组件的属性列表
   */
  properties: {
    title: {
      type: String,
      value: '音乐app'
    },
    showActions: {
      type: Boolean,
      value: false
    },
    light: {
      type: Boolean,
      value: true
    },
    bgUrl: {
      type: String
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
  },

  /**
   * 组件的方法列表
   */
  methods: {

  }
})
