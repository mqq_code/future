const app = getApp()

module.exports = Behavior({
  data: {
    menuButtonBottom: app.globalData.menuButton.bottom,
    menuButtonTop: app.globalData.menuButton.top,
    menuButtonLeft: app.globalData.menuButton.left,
    menuButtonRight: app.globalData.menuButton.right,
    menuButtonWidth: app.globalData.menuButton.width,
    menuButtonHeight: app.globalData.menuButton.height
  }
})